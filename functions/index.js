const functions = require('firebase-functions');
const admin = require('firebase-admin');
const nodemailer = require('nodemailer');
const cors = require('cors')({origin: true});
admin.initializeApp();

/**
* Here we're using Gmail to send 
*/
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'capitalfake2020@gmail.com',
        pass: 'capital00'
    }
});

exports.sendMail = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        // getting dest email by query string
        const dest = req.body.dest;

        const mailOptions = {
            from: 'Constructora capital <constructora.noreply@gmail.com>', // Something like: Jane Doe <janedoe@gmail.com>
            to: dest,
            subject: 'Contrato Constructora'
            , attachments: [{
                filename: 'contract.pdf',
                content: req.body.file,
                encoding: 'base64'
            }],
            text: 'Adjunto se encuentra el contrato'
        };
  
        // returning result
        return transporter.sendMail(mailOptions, (erro, info) => {
            if(erro){
                return res.send(erro.toString());
            }
            return res.send('Sended');
        });
    });    
});