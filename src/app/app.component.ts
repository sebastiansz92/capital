import {
  Component,
  ViewChild,
  HostListener,
  SimpleChanges,
} from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { LocationStrategy } from "@angular/common";
import { ProjectsService } from "./projects/project.service";
import { ConnectionService } from "ng-connection-service";
import { MDBModalRef, MDBModalService } from "ng-uikit-pro-standard";
import { OfflineModalComponent } from "./shared/offline-modal/offline-modal.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  modalRef: MDBModalRef;
  isConnected = true;
  modoOffline = false;
  title = "app";
  shown = false;
  icon = "angle-right";
  audits = [];
  ownerAudits = [];

  constructor(
    private projectsService: ProjectsService,
    private router: Router,
    private connectionService: ConnectionService,
    private modalService: MDBModalService
  ) {}

  ngOnInit(): void {
    localStorage.setItem("status", "online");
    this.audits = JSON.parse(localStorage.getItem("audits"));
    this.ownerAudits = JSON.parse(localStorage.getItem("ownerAudits"));
    this.validateConnectionInternet();
  }

  validateConnectionInternet() {
    this.connectionService.monitor().subscribe((isConnected) => {
      if (isConnected) {
        localStorage.setItem("status", "online");
        this.modoOffline = false;
        this.openOfflineModal(isConnected);
      } else {
        localStorage.setItem("status", "offline");
        this.modoOffline = true;
        this.openOfflineModal(isConnected);
      }
    });
  }

  openOfflineModal(isConnected) {
    let getLastDateUpdate = JSON.parse(
      localStorage.getItem("fechaActualizacion")
    ).split("T")[0];
    let projectName = JSON.parse(localStorage.getItem("project")).name;
    let auditByUpdate =
      JSON.parse(localStorage.getItem("audits"))?.length === undefined
        ? 0
        : JSON.parse(localStorage.getItem("audits"))?.length;
    let ownerAudit =
      JSON.parse(localStorage.getItem("ownerAudits"))?.length === undefined
        ? 0
        : JSON.parse(localStorage.getItem("ownerAudits"))?.length;
    let descriptionOnline = `Hay ${auditByUpdate} auditoria(s) pendiente(s) por sincronizar y ${ownerAudit} entrega(s) pendiente(s) por sincronizar`;
    let descriptionOffline = `Última actualización el ${getLastDateUpdate} para el proyecto ${projectName}`;
    this.modalRef = this.modalService.show(OfflineModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: "feature-modal",
      containerClass: "",
      animated: true,
      data: {
        title: isConnected
          ? "¡Conexión establecida!"
          : "Sin conexión a internet",
        description: isConnected ? descriptionOnline : descriptionOffline,
        question: isConnected
          ? "¿Deseas iniciar la sincronización?"
          : "¿Deseas iniciar el modo offline?",
      },
    });
    this.modalRef.content.action.subscribe((result: any) => {
      if (result) {
        let status = localStorage.getItem("status");
        if (status === "online") {
          this.syncData();
          this.syncDataEntrega();
        }
      }
    });
    this.modalRef.hide();
  }

  ngOnChanges(changes: SimpleChanges): void {}

  @ViewChild("sidenav") public el: any;

  getName() {
    return localStorage.getItem("username") || "Sin conectar";
  }

  @HostListener("click", ["$event.target"])
  onClick(elm) {
    const element = document.querySelector("#sidenav-overlay");
    if (elm == element) {
      this.close();
    }
  }

  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    localStorage.removeItem("isLoggedIn");
    this.router.navigate(["/login"]);
  }

  close() {
    this.el.hide();
    this.shown = false;
    this.icon = "angle-right";
  }

  isLoggedIn() {
    return localStorage.getItem("isLoggedIn") == "true";
  }

  checkRoles(roles) {
    if (roles.find((x) => x == localStorage.getItem("role")) != undefined) {
      return true;
    } else {
      return false;
    }
  }

  getState() {
    return this.shown ? "opened" : "closed";
  }

  toggle() {
    this.el.toggle();
    this.shown = !this.shown;
    this.shown ? (this.icon = "angle-left") : (this.icon = "angle-right");
  }

  setOwner(flag) {
    this.projectsService.owner = flag;
  }

  syncData() {
    if (this.audits != null && this.audits.length > 0) {
      this.audits.forEach((request, index) => {
        if (request.auditsByReforms !== undefined) {
          let withReforms = {
            generalObservation: request.generalObservation,
            auditsByReforms: request.auditsByFeatures,
            nomenclatureState: request.nomenclatureState,
            auditor: "CONSTRUCTORA",
          };
          this.projectsService
            .addAudit(request.id, {
              ...withReforms,
              auditsByReforms: request.auditsByReforms,
            })
            .subscribe(
              (res) => {
                console.log("Actualizo bien");
              },
              (error) => {
                alert(error.error.data || error.error.message);
              }
            );
        } else {
          let withOutReforms = {
            generalObservation: request.generalObservation,
            auditsByFeatures: request.auditsByFeatures,
            nomenclatureState: request.nomenclatureState,
            auditor: "CONSTRUCTORA",
          };
          this.projectsService.addAudit(request.id, withOutReforms).subscribe(
            (res) => {
              console.log("Actualizo bien");
            },
            (error) => {
              alert(error.error.data || error.error.message);
            }
          );
        }
      });
      localStorage.removeItem("audits");
      this.router.navigate(["/offline-projects"]);
    }
  }

  syncDataEntrega() {
    if (this.ownerAudits != null && this.ownerAudits.length > 0) {
      this.ownerAudits.forEach((request) => {
        debugger;
        let data = {
          auditsByFeatures: request.auditsByFeatures,
          auditsByReforms: request.auditsByReforms,
          contract: request.contract,
        };
        this.projectsService
          .addOwnerAudit(request.nomenclatureId, data)
          .subscribe(
            (res) => {
              console.log(res);
            },
            (error) => {
              if (error.status == 400) {
                alert(error.error.message);
              } else {
                alert(error.error.data);
              }
            }
          );
      });
      localStorage.removeItem("ownerAudits");
    }
  }
}
