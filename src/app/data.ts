export const data = {
    projects : [
        {
            name: 'Proyecto 1',
            img_url: 'https://informativojuridico.com/wp-content/uploads/2015/04/proyecto-construcci%C3%B3n.jpg',
            towers: 2,
            aptos: 40
        },
        {
            name: 'Proyecto 2',
            img_url: 'https://www.portafolio.co/files/article_multimedia/uploads/2018/09/02/5b8c8f78a67e9.jpeg',
            towers: 1,
            aptos: 18
        },
        {
            name: 'Proyecto 3',
            img_url: 'https://www.portafolio.co/files/article_multimedia/uploads/2018/09/02/5b8c8f78a67e9.jpeg',
            towers: 3,
            aptos: 102
        }
    ],

    types: [
        {
            name: 'Apartamento',
            spaces: [
                {
                    name: 'Alcoba',
                    features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                },
                {
                    name: 'Comedor',
                    features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                },
                {
                    name: 'Cocina',
                    features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                }
            ]
        },
        {
            name: 'Aparta-estudio',
            spaces: [
                {
                    name: 'Alcoba',
                    features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                },
                {
                    name: 'Cocina',
                    features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                }
            ]
        },
        {
            name: 'Casa',
            spaces: [
                {
                    name: 'Alcoba',
                    features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                },
                {
                    name: 'Comedor',
                    features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                },
                {
                    name: 'Cocina',
                    features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                },
                {
                    name: 'Patio',
                    features: ['Jardin', 'Juegos']
                },
                {
                    name: 'Sala',
                    features: ['Ventanas', 'Muebles', 'Enchufes', 'Luces']
                },
                {
                    name: 'Baño',
                    features: ['Inodoro', 'Ducha', 'Tina', 'Espejos', 'Cajones']
                }
            ]
        }
    ]
};