import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../shared/services/project.service';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email;
  password;
  loginForm : FormGroup

  constructor(private loginService: LoginService, private router: Router,
    private formBuilder: FormBuilder ) { }

  ngOnInit() {
    if(localStorage.getItem('isLoggedIn') == 'true') {
      this.router.navigate(['/projects']);
    }

    this.loginForm = this.formBuilder.group ({
      email: ['', []],
      password: ['', []]
    })
  }

  login() {
    this.loginService.login(this.loginForm.get('email').value, this.loginForm.get('password').value).subscribe(res => {
      localStorage.setItem('token', res.headers.get('authorization'));
      let token = localStorage.getItem('token');
      let decoded = jwt_decode(token);
      localStorage.setItem('role', decoded['Authority']);
      localStorage.setItem('isLoggedIn', 'true');
      localStorage.setItem('username', this.loginForm.get('email').value);
      this.router.navigate(['projects']);
    }, error => {
      if(error.error) {
        alert(error.error.data || error.error.message || 'Datos de autenticación erroneos');
      } else {
        alert ('Datos de autenticación erroneos');
      }
      this.loginForm.get('email').setValue('');
      this.loginForm.get('password').setValue('');
      console.log('Datos de autenticación erroneos');
    });
  }

}
