import { Injectable } from '@angular/core';
import { HttpBaseService } from '../shared/services/http/http-base.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends HttpBaseService{

  constructor(http: HttpClient) {
    super(http);
  }

  public login(email, password) {
    return this.post('login', {password, email});
  }

  public signup(newUser) {
    return this.post('user/signup', newUser);
  }

  
}
