import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { SCHEMA } from './excel-schema';
import * as XLSX from 'xlsx';
import { ReformsService } from '../reforms.service';
import { ProjectsService } from 'src/app/projects/project.service';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { ConfirmationModalComponent } from 'src/app/shared/confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'app-apto-reforms',
  templateUrl: './apto-reforms.component.html',
  styleUrls: ['./apto-reforms.component.css']
})
export class AptoReformsComponent implements OnInit {

  project;
  exist = false;
  loaded = false;
  tower;
  audit;
  apto;
  type;
  schema = SCHEMA;
  arrayBuffer: any;
  filelist:any;
  reformas;
  headElements = ['Descripción', 'Cantidad', 'Valor Unitario', 'Valor Total'];
  /////////////
  nomenclature;
  reforms;
  modalRef: MDBModalRef;

  constructor(private projectService: ProjectService, private route: ActivatedRoute, private router: Router
    , private formBuilder: FormBuilder, private reformsService: ReformsService,
    private modalService: MDBModalService, private projectsService: ProjectsService) { }

  ngOnInit() {
    /* this.project = this.projectService.data.projects[this.route.snapshot.params.id];
    if(this.project == undefined) {
      this.router.navigate(['/reform']);
    }
    this.tower = this.route.snapshot.params.tower;
    this.audit = this.route.snapshot.params.audit;
    this.apto = this.route.snapshot.params.apto;
    let aptoObj = this.project.noms.filter(tower => tower.tower == this.tower).find(apto => apto.nom == this.apto);
    this.type = this.project.types.find(x => x.name == aptoObj.type);
    this.reformas = this.projectService.getReforms(this.route.snapshot.params.id, this.tower, this.apto); */
    this.tower = this.route.snapshot.params.tower;
    this.project = this.projectsService.project;
    this.nomenclature = this.reformsService.nomenclature;
    this.nomenclature.reforms = this.nomenclature.reforms.sort((a,b) => {
      if(a.filename < b.filename){
        return -1;
      } else if(a.filename > b.filename) {
        return 1;
      } else {
        return 0;
      }
    });
    let reformsByFile = this.nomenclature.reforms.reduce((accu, actual, index) => {
      return {...accu, [actual.filename]: accu[actual.filename]? [...accu[actual.filename], actual] : [actual]}
    }, {});
    this.reforms = Object.entries(reformsByFile);
    if (this.nomenclature == null || this.project == null) {
      this.router.navigate(['/reform']);
    }
  }

  openConfirmationModal(filename) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: "feature-modal",
      containerClass: "",
      animated: true,
      data: {
        question:
          "¿Está seguro que desea eliminar estas reformas (" +
          filename +
          ")?",
      },
    });
    this.modalRef.content.action.subscribe((result: any) => {
      if (result) {
        this.projectsService.deleteReform(this.nomenclature.id, filename)
        .subscribe(res => {
          console.log(res);
          this.nomenclature.reforms = this.nomenclature.reforms.filter(reform => reform.filename !== filename);
          let reformsByFile = this.nomenclature.reforms.reduce((accu, actual, index) => {
            return {...accu, [actual.filename]: accu[actual.filename]? [...accu[actual.filename], actual] : [actual]}
          }, {});
          this.reforms = Object.entries(reformsByFile);
        })
      }
    });
    this.modalRef.hide();
  }

  readFile(event) {
  const file = event.target.files[0];
  const fileReader = new FileReader();
  fileReader.readAsArrayBuffer(file);
  fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i != data.length; ++i){
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join("");
      const workbook = XLSX.read(bstr, {type:"binary"});
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      // convertir el archivo de excel a json
      // range: 1 indica que los nombres de los atributos se encuentran en la segunda fila del archivo
      const arraylist = XLSX.utils.sheet_to_json(worksheet, {raw: true, range: 1});
      this.filelist = arraylist;
      // mapear (cambiar) nombres de los atributos del excel
      const keys = Object.keys(this.filelist[0]);
      this.filelist = this.filelist.filter((cell, index) => cell[keys[0]]&& cell[keys[1]] && cell[keys[2]] && cell[keys[3]])
      .map(reform => {
          return {
            name: reform[keys[0]],
            quantity: reform[keys[2]],
            unitaryPrice: reform[keys[1]],
            totalPrice: reform[keys[3]],
            filename: file.name
          };
      });

      let json = {
        nomenclatureId: this.nomenclature.id,
        reforms: this.filelist
      };
      console.log(json);
      this.reformsService.addReform(json.nomenclatureId, {reforms: json.reforms}).subscribe(res => {
        json.reforms.forEach(reform => {
          this.nomenclature.reforms.push(reform);
          let reformsByFile = this.nomenclature.reforms.reduce((accu, actual, index) => {
            return {...accu, [actual.filename]: accu[actual.filename]? [...accu[actual.filename], actual] : [actual]}
          }, {});
          this.reforms = Object.entries(reformsByFile);
        });
        console.log(event.target);
        event.target.value = '';
      }, error => {
        alert(error.error.data || 'Ha ocurrido un error al guardar las reformas');
        console.log(error);
      })
      //this.projectService.addReforms(this.route.snapshot.params.id, this.tower, this.apto, this.filelist);
  }
  //this.reformas = this.projectService.getReforms(this.route.snapshot.params.id, this.tower, this.apto);
}
getTotal(reforms) {
  return reforms.reduce((total, currentReform) => { return total + currentReform.totalPrice}, 0);
}

}
