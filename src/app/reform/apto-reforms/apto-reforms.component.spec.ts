import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AptoReformsComponent } from './apto-reforms.component';

describe('AptoReformsComponent', () => {
  let component: AptoReformsComponent;
  let fixture: ComponentFixture<AptoReformsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AptoReformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AptoReformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
