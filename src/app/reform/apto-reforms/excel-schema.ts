export const SCHEMA = {
    'DESCRIPTION': {
      prop: 'Descripcion de la reforma',
      type: String,
      required: true
      // Excel stores dates as integers.
      // E.g. '24/03/2018' === 43183.
      // Such dates are parsed to UTC+0 timezone with time 12:00 .
    },
    'QUANTITY': {
      prop: 'Cantidad solicitada',
      type: Number,
      required: true
    },
    // 'COURSE' is not a real Excel file column name,
    // it can be any string — it's just for code readability.
    'x_value': {
      prop: 'Valor X',
      type: Number,
      required: true
    },
    'y_value': {
        prop: 'Valor X',
        type: Number,
        required: true
    }
  }