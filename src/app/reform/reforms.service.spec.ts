import { TestBed } from '@angular/core/testing';

import { ReformsService } from './reforms.service';

describe('ReformsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReformsService = TestBed.get(ReformsService);
    expect(service).toBeTruthy();
  });
});
