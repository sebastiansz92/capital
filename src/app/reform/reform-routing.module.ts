import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReformComponent } from './reform/reform.component';
import { ReformProjectSelectionComponent } from './reform-project-selection/reform-project-selection.component';
import { ReformAptoListComponent } from './reform-apto-list/reform-apto-list.component';
import { AptoReformsComponent } from './apto-reforms/apto-reforms.component';
import { AdminGuard } from '.././admin/admin.guard';

const routes: Routes = [
  {
    path: '', component: ReformComponent,
    children: [{path: '', component: ReformProjectSelectionComponent}],
    canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA']}
  },
  {
    path: 'reform', component: ReformComponent,
    children: [
      {path: '', component: ReformProjectSelectionComponent},
      {path: 'project_selection', component: ReformProjectSelectionComponent},
      {path: 'project/:id', component: ReformAptoListComponent},
      {path: 'project/:id/tower/:tower/apto/:apto', component: AptoReformsComponent}
    ],
    canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA']}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReformRoutingModule { }
