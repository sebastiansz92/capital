import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpBaseService } from '../shared/services/http/http-base.service';

@Injectable({
  providedIn: 'root'
})
export class ReformsService extends HttpBaseService{

  constructor(http: HttpClient) {
    super(http);
  }

  nomenclature = null;

  addReform(nomenclature, reforms) {
    return this.post(`nomenclature/${nomenclature}/reform`, reforms)
  }

  addAudit(audits, id) {
    return this.post(`audit/byReforms?nomenclature=${id}`, audits);
  }


  setCurrentNomenclature(nomenclature) {
    this.nomenclature = nomenclature;
  }
}
