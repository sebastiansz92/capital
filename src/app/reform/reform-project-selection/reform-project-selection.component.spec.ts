import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReformProjectSelectionComponent } from './reform-project-selection.component';

describe('ReformProjectSelectionComponent', () => {
  let component: ReformProjectSelectionComponent;
  let fixture: ComponentFixture<ReformProjectSelectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReformProjectSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReformProjectSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
