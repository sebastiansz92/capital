import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { Router } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';

@Component({
  selector: 'app-reform-project-selection',
  templateUrl: './reform-project-selection.component.html',
  styleUrls: ['./reform-project-selection.component.css']
})
export class ReformProjectSelectionComponent implements OnInit {
  projects = [];

  constructor(private projectService: ProjectService, private router: Router, private projectsService: ProjectsService) { }

  ngOnInit() {
    //this.projects = this.projectService.data.projects.map((project, index) => ({label: project.name, value: index}));
    this.projectsService.getProjects().subscribe(res => {
      let projs = res.body['data'];
      this.projects = projs.map(proj => ({label: proj.name, value: proj.projectId}));
    });
  }

  goToProjectReforms(event) {
    this.router.navigate(['/reform', 'project', event.value] );
  }

}
