import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReformRoutingModule } from './reform-routing.module';
import { ReformComponent } from './reform/reform.component';
import { ReformProjectSelectionComponent } from './reform-project-selection/reform-project-selection.component';
import { ReformAptoListComponent } from './reform-apto-list/reform-apto-list.component';
import { MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AptoReformsComponent } from './apto-reforms/apto-reforms.component';


@NgModule({
  declarations: [ReformComponent, ReformProjectSelectionComponent, ReformAptoListComponent, AptoReformsComponent],
  imports: [
    CommonModule,
    ReformRoutingModule,
    MDBBootstrapModulesPro.forRoot(),
    ToastModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ReformModule { }
