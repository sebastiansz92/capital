import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReformAptoListComponent } from './reform-apto-list.component';

describe('ReformAptoListComponent', () => {
  let component: ReformAptoListComponent;
  let fixture: ComponentFixture<ReformAptoListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReformAptoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReformAptoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
