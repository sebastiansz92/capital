import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';
import { ReformsService } from '../reforms.service';

@Component({
  selector: 'app-reform-apto-list',
  templateUrl: './reform-apto-list.component.html',
  styleUrls: ['./reform-apto-list.component.css']
})
export class ReformAptoListComponent implements OnInit {

  project;
  noms = [];
  date;
  loaded = false;

  constructor(private projectService: ProjectService, private route: ActivatedRoute, private router: Router,
    private projectsService: ProjectsService, private reformService: ReformsService) { }

  ngOnInit() {
    //this.project = this.projectService.data.projects[this.route.snapshot.params.id];
    this.projectsService.getProjectById(this.route.snapshot.params.id).subscribe(res => {
      this.project = res.body['data'];
      this.projectsService.setCurrentProject(res.body['data']);
      console.log(this.project);
      this.noms = this.project.towers.map(tower => {

        return {tower: tower.name, noms: tower.nomenclaturesPage.pageContent};
      });
      this.date = new Date();
      this.loaded = true;
    });
    
  }

  goToAptoReforms(tower, apto) {
    this.reformService.setCurrentNomenclature(apto);
    this.router.navigate(['/reform', 'project', this.route.snapshot.params.id, 'tower', tower, 'apto', apto.number]);
  }

}
