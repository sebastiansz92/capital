import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  role = null;
  options = [
    { value: 'ADMIN', label: 'Administrador' },
    { value: 'RESIDENTE_OBRA', label: 'Residente de Obra' },
    { value: 'RESIDENTE_ENTREGA', label: 'Residente de Entregas' },
  ];

  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group ({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    })
  }


  optionSelected(event) {
    this.role = event.value;
  }

  signup() {
    if( this.signupForm.valid && this.role != null) {
      let user = {
        fullname: this.signupForm.get('name').value,
        email: this.signupForm.get('email').value,
        password: this.signupForm.get('password').value,
        rol: this.role
      }
      this.loginService.signup(user).subscribe(res => {
        alert('El usuario ha sido registrado correctamente');
        this.signupForm.get('name').setValue('');
        this.signupForm.get('email').setValue('');
        this.signupForm.get('password').setValue('');
      }, error => {
        alert(error.error.data || error.error.message);
        this.signupForm.get('name').setValue('');
        this.signupForm.get('email').setValue('');
        this.signupForm.get('password').setValue('');
      })
    } else {
      alert('El formulario debe estár completamente diligenciado');
    }
  }

}
