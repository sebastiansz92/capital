import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HistoryProjectSelectionComponent } from './history-project-selection.component';

describe('HistoryProjectSelectionComponent', () => {
  let component: HistoryProjectSelectionComponent;
  let fixture: ComponentFixture<HistoryProjectSelectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryProjectSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryProjectSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
