import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';

@Component({
  selector: 'app-history-project-selection',
  templateUrl: './history-project-selection.component.html',
  styleUrls: ['./history-project-selection.component.css']
})
export class HistoryProjectSelectionComponent implements OnInit {
  projects = [];

  constructor(private projectService: ProjectService, private router: Router, private route: ActivatedRoute,
    private projectsService: ProjectsService) { }

  ngOnInit() {
    //this.projects = this.projectService.data.projects.map((project, index) => ({label: project.name, value: index}));
    this.projectsService.getProjects().subscribe(res => {
      let projs = res.body['data'];
      this.projects = projs.map(proj => ({label: proj.name, value: proj.projectId}));
    });
  }

  goToProjectHistory(event) {
    console.log(event);
    if (this.route.snapshot.params.owner !== undefined) {
      this.router.navigate(['/historic', 'owner', true, 'project', event.value] );
    } else {
      this.router.navigate(['/historic', 'project', event.value] );
    }
  }

}
