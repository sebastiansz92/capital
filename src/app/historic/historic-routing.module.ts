import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryProjectSelectionComponent } from './history-project-selection/history-project-selection.component';
import { HistoricComponent } from './historic/historic.component';
import { HistoricDetailsComponent } from './historic-details/historic-details.component';
import { HistoricListComponent } from './historic-list/historic-list.component';
import { AuditHistoriesComponent } from './audit-histories/audit-histories.component';
import { AptoHistoricComponent } from './apto-historic/apto-historic.component';
import { AdminGuard } from '../admin/admin.guard';


const routes: Routes = [
  {
    path: '', component: HistoricComponent,
    children: [{path: '', component: HistoryProjectSelectionComponent}],
    canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA']}
  },
  {
    path: 'historic', component: HistoricComponent,
    children: [
      {path: '', component: HistoryProjectSelectionComponent},
      {path: 'project_selection', component: HistoryProjectSelectionComponent},
      {path: 'project/:id', component: HistoricListComponent},
      {path: 'project/:id/tower/:tower/apto/:apto', component: AptoHistoricComponent},
      {path: 'project/:id/tower/:tower/apto/:apto/audit/:audit', component: AuditHistoriesComponent},
      {path: 'owner/:owner/project_selection', component: HistoryProjectSelectionComponent},
      {path: 'owner/:owner/project/:id', component: HistoricListComponent},
      {path: 'owner/:owner/project/:id/tower/:tower/apto/:apto', component: AptoHistoricComponent},
      {path: 'owner/:owner/project/:id/tower/:tower/apto/:apto/audit/:audit', component: AuditHistoriesComponent}
    ],
    canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA']}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HistoricRoutingModule { }
