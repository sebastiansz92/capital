import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.css']
})
export class HistoricComponent implements OnInit {

  historyType;
  constructor(private route: ActivatedRoute, private router: Router, private projectsService: ProjectsService) { }

  ngOnInit() {
    this.getOwner();
    this.router.events.subscribe(event => {
      this.getOwner();
    })
  }

  getOwner() {
    if(this.projectsService.owner) {
      this.historyType = this.projectsService.owner;
    } else {
      this.historyType = this.router.url.includes('/owner/')? 'Entregas' : 'Proyectos';
    }
  }

}
