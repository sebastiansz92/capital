import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AuditHistoriesComponent } from './audit-histories.component';

describe('AuditHistoriesComponent', () => {
  let component: AuditHistoriesComponent;
  let fixture: ComponentFixture<AuditHistoriesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditHistoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditHistoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
