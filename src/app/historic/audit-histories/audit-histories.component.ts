import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { ProjectsService } from 'src/app/projects/project.service';
import * as cloneDeep from 'lodash/cloneDeep';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-audit-histories',
  templateUrl: './audit-histories.component.html',
  styleUrls: ['./audit-histories.component.css'],
})
export class AuditHistoriesComponent implements OnInit {
  project;
  spaceForm: FormGroup;
  exist = false;
  loaded = false;
  tower;
  audit;
  apto;
  type;
  owner = false;
  audits;
  audSpaces;
  @ViewChild('thereAre') content: ElementRef;
  attachments;
  generalAttachments = [];
  multimedia = 'http://68.183.97.247:8091/';
  ready = false;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private projectsService: ProjectsService
  ) {}

  ngOnInit() {
    this.tower = this.route.snapshot.params.tower;
    this.tower = this.route.snapshot.params.tower;
    this.apto = this.route.snapshot.params.apto;
    this.audit = this.route.snapshot.params.audit;
    let nomenclature = this.projectsService.nomenclature;
    if (this.route.snapshot.params.owner !== undefined) {
      this.owner = true;
    }
    let space;
    if(nomenclature?.reforms?.length > 0) {
      space = {
        name: 'Reformas',
        features: []
      }
      nomenclature.reforms.forEach((ref, index) => {
        space.features.push({
          name: ref.name
        })
      })
    }
    this.project = this.projectsService.project;
    if (!this.project){
      this.router.navigate(['/historic']);
    }
    let projectType = this.project?.towers.find(tower => tower.name === this.tower).nomenclaturesPage.pageContent.find(nom => nom.number.toString() === this.apto).projectTypeId;
    let aptoSpaces = this.project['projectTypes'].find(type => type.id == projectType).spaces;
    this.projectsService.getAuditDetail(this.audit)
          .subscribe(resp => {
            let audits = resp.body['data'];
            console.log(audits);
            let noreformAudits = audits.filter(x => !x.reform);
            let reformAudits = audits.filter(x => x.reform);
            this.audSpaces = this.projectsService.mapAuditsToSpaces(aptoSpaces, audits, "online");
            this.attachments = cloneDeep(this.audSpaces);;
            /* this.attachments['audits'] = this.attachments['audits'].filter(space => {
              space.features = space.features.filter(feat => feat.aud.attachments.length > 0);
              return space.features.some(feature => feature.aud.attachments.length > 0);
            }) */
            /* if(reformAudits.length > 0) {
              reformAudits.forEach(reformAudit => {
                space.features.find(feat => feat.name === reformAudit.reform.name)['aud'] = {
                  observation: reformAudit.observation,
                  approved: reformAudit.approved,
                  featureId: -1,
                  visitNote: reformAudit.visitNote,
                  quantity: reformAudit.reform.quantity,
                  totalReformPrice: reformAudit.reform.totalPrice,
                  attachments: reformAudit.attachments
                };
              })
            } */
            console.log(this.audSpaces);
            if(space && reformAudits.length > 0) this.audSpaces['audits'].push(space);
            this.createSpaceForm(this.audSpaces, true);
            console.log('oe');
            this.ready = true;
            this.spaceForm.disable();
          })
  }

  getAttachs(audits) {
    audits.forEach((space, index) => {
      this.attachments.push({space: space.name, attachs: []});
      space.features.forEach(feature => {
        this.attachments[index].attachs.push({name: feature.name, attachs: []})
        this.attachments[index].attachs[this.attachments[index].attachs.length -1].attachs = this.attachments[index].attachs[this.attachments[index].attachs.length -1].attachs.concat(feature.aud.attachments.map(attach => (this.getProjectImage(attach.resource_url))));
        if (this.attachments[index].attachs[this.attachments[index].attachs.length -1].attachs.length <= 0) {
          this.attachments[index].attachs.pop();
        }
      })
    })
    this.attachments = this.attachments.filter(x => x.attachs.length > 0);
    this.generalAttachments = this.audits.filter(aud => aud.featureId == undefined)[0].attachments.map(attach => (this.getProjectImage(attach.resource_url)));
  }

  getProjectImage(image_url){
    let url = image_url.substr(10);
    return this.multimedia + image_url;
  }

  checkForAuditChanges() {
    let audits;
    if (this.route.snapshot.params.owner !== undefined) {
      this.owner = true;
      audits = this.project.ownerAudits;
    } else {
      audits = this.project.audits;
    }
    const apto = this.route.snapshot.params.apto;
    if (audits[this.tower]) {
      if (audits[this.tower][apto]) {
        if (audits[this.tower][apto].length > 1) {
          this.spaceForm = audits[this.tower][apto][
            this.audit
          ].form;
          this.exist = true;
          this.loaded = true;
        } else {
          this.spaceForm = audits[this.tower][apto][
            this.audit
          ].form;
          this.exist = true;
          this.loaded = true;
        }
      } else {
        this.exist = false;
        this.loaded = false;
      }
    }
  }

  get spaces(): FormArray {
    if (this.spaceForm != undefined) {
      return this.spaceForm.get('spaces') as FormArray;
    }
  }

  checkValues(featureForm, i, j) {
    /* const obsGenerales =  */
    const obsVisita = featureForm.get('obsVisita').value;
    const obsPendientes = featureForm.get('obsPendientes').value;
    const check = obsVisita != '' || obsPendientes != '';
    if (check) {
      return this.checkChanges(featureForm, i, j);
    } else {
      return check;
    }
  }

  checkItem(spaceForm, i) {
    const array = spaceForm.controls.features as FormArray;
    let check = false;
    let j = 0;
    for (let feature of array.controls) {
      let check = this.checkValues(feature, i, j);
      if (check) {
        return true;
      }
      j = j + 1;
    }
    return false;
  }

  checkChanges(featureForm, i, j) {
    let audits;
    if (this.route.snapshot.params.owner !== undefined) {
      audits = this.project.ownerAudits;
    } else {
      audits = this.project.audits;
    }
    const apto = this.route.snapshot.params.apto;
    if (audits[this.tower][apto].length > 1 && this.audit >= 1) {
      const obsVisitaAnterior = audits[this.tower][apto][
        this.audit - 1
      ].form.controls.spaces.controls[i].controls.features.controls[j].controls
        .obsVisita.value;
      const obsPendientesAnterior = audits[this.tower][apto][
        this.audit - 1
      ].form.controls.spaces.controls[i].controls.features.controls[j].controls
        .obsPendientes.value;
      const obsVisita = featureForm.controls.obsVisita.value;
      const obsPendientes = featureForm.controls.obsPendientes.value;
      if (
        obsVisitaAnterior != obsVisita ||
        obsPendientesAnterior != obsPendientes
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  createSpaceForm(data, copy?) {
    if (!copy) {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: ['', []],
        spaces: this.formBuilder.array([]),
      });

      data.spaces.forEach((space, index) => {
        this.addSpace(space);
      });
    } else {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: [this.projectsService.audit?.generalObservation || '', []],
        spaces: this.formBuilder.array([])
      });
      /* const form = this.project.audits[this.tower][this.apto.nom][this.project.audits[this.tower][this.apto.nom].length - 1].form;
      this.spaceForm.patchValue(form.controls.spaces);
 */
      data.audits.forEach((space, index) => {
        this.addSpace(space, true);
      });
    }
  }

  getfeatures(index): FormArray {
    return this.spaceForm.get('spaces').get(index.toString()) as FormArray;
  }

  createFeatureGroup(feature, copy?) {
    console.log(feature);
    if (copy) {
      let form = this.formBuilder.group({
        name: [feature.feature, []],
        obsVisita: [this.checkNotNull(feature.visitNote), []],
        obsPendientes: [this.checkNotNull(feature.observation), []],
        aproved: [feature.approved, []],
        id: [feature.featureId, []],
        /* quantity: [feature.aud['totalReformPrice'] || '', []],
        totalReformPrice: [feature.aud['totalReformPrice'] || '', []] */
      });
      return form;
    } else {
      return this.formBuilder.group({
        name: [feature, []],
        obsVisita: ['', []],
        obsPendientes: ['', []],
        aproved: [false, []],
        quantity: ['', []],
        totalReformPrice: ['', []]
      });
    }
  }

  checkNotNull(value) {
    if(value == '.') {
      return '';
    } else {
      return value;
    }
  }

  createSpaceGroup(spaceobj, features, copy?) {
    if (copy) {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([])
      });

      let feats: FormArray = space.get('features') as FormArray;
      features.forEach((feature, index) => {
        feats.push(this.createFeatureGroup(feature, true));
      });
      return space;
    } else {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([]),
      });

      let feats: FormArray = space.get('features') as FormArray;
      features.forEach((feature) => {
        feats.push(this.createFeatureGroup(feature));
      });
      return space;
    }
  }

  addSpace(space, data?) {
    console.log(space);
    if (data) {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup(space, features, true));
    } else {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup(space, features));
    }
  }

  addFeature(i, name) {
    this.getfeatures(i).push(this.createFeatureGroup(name));
  }

  exportToPDF() {}

  getDocumentDefinition() {
    const data = {
      content: [
        {
          text: 'Titulo',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20],
        },
        {
          columns: [
            [
              {
                text: 'texto1',
                style: 'name',
              },
              {
                text: 'texto2',
              },
              {
                text: 'Email : ' + 'email',
              },
              {
                text: 'Contant No : ' + 'number',
              },
              {
                text: 'GitHub: ' + 'socialProfile',
                link: 'enlace',
                color: 'blue',
              },
            ],
            [
              // Document definition for Profile pic
            ],
          ],
        },
        {
          text: 'titulo2',
          bold: true,
          fontSize: 24,
          alignment: 'center',
          margin: [20, 0, 0, 0],
        },
      ],
      styles: {
        name: {
          fontSize: 16,
          bold: true,
        },
      },
    };

    let datos = [
      'uno', 'dos', 'tres', 'cuatro', 'cinco'
    ];

    return {
      content: [
        {table: {
          width: ['*', '*', '*', '*', '*'],
          body: [[{
            text: 'header 1'
          },
          {
            text: 'header 2'
          },
          {
            text: 'header 3'
          },
          {
            text: 'header 4'
          },
          {
            text: 'header 5'
          }],
          datos.map(x => {
            return [{text: x}, {text: x}, {text: x}, {text: x}];
          })
        ]
        }}
      ]
    }
  }

  generatePdf(action = 'open') {
    const documentDefinition = this.getDocumentDefinition();
    switch (action) {
      case 'open':
        pdfMake.createPdf(documentDefinition).open();
        break;
      case 'print':
        pdfMake.createPdf(documentDefinition).print();
        break;
      case 'download':
        pdfMake.createPdf(documentDefinition).download();
        break;
      default:
        pdfMake.createPdf(documentDefinition).open();
        break;
    }
  }

  //owner audits
  createSpaceForm2(spaces, copy?) {
    if (!copy) {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: ['', []],
        spaces: this.formBuilder.array([])
      });

      spaces.forEach((space, index) => {
        this.addSpace2(space);
      });
    } else {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: ['', []],
        spaces: this.formBuilder.array([])
      });
      const form = this.project.ownerAudits[this.tower][this.apto][this.project.ownerAudits[this.tower][this.apto].length - 1].form;
      this.spaceForm.patchValue(form.controls.spaces);
      spaces.forEach((space, index) => {
        this.addSpace2(space, form.controls.spaces.controls[index]);
      });
    }
  }


  createSpaceGroup2(spaceobj, features, data?) {
    if (data) {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([]),
      });

      space.patchValue(data);

      let feats: FormArray = space.get('features') as FormArray;
      features.forEach((feature, index) => {
        feats.push(
          this.createFeatureGroup2(
            feature,
            data.controls.features.controls[index]
          )
        );
      });
      return space;
    } else {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([]),
      });

      let feats: FormArray = space.get('features') as FormArray;
      features.forEach((feature) => {
        feats.push(this.createFeatureGroup2(feature));
      });
      return space;
    }
  }

  addSpace2(space, data?) {
    if (data) {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup2(space, features, data));
    } else {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup2(space, features));
    }
  }

  createFeatureGroup2(feature, data?) {
    if (data) {
      let form = this.formBuilder.group({
        name: [feature, []],
        observations: [data.controls.obs.value, []],
        aproved: [data.controls.aproved.value, []],
      });
      form.patchValue(data.value);
      return form;
    } else {
      return this.formBuilder.group({
        name: [feature, []],
        observations: ['', []],
        aproved: [false, []],
      });
    }
  }
  
}
