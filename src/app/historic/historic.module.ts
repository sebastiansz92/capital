import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoricRoutingModule } from './historic-routing.module';
import { HistoricComponent } from '../historic/historic/historic.component';
import { HistoricDetailsComponent } from './historic-details/historic-details.component';
import { HistoricListComponent } from './historic-list/historic-list.component';
import { MDBBootstrapModulesPro, ToastModule, MDBSpinningPreloader } from 'ng-uikit-pro-standard';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppHammerConfig } from '../app.module';
import { environment } from 'src/environments/environment';
import { AuditHistoriesComponent } from './audit-histories/audit-histories.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AptoHistoricComponent } from './apto-historic/apto-historic.component';

@NgModule({
  declarations: [HistoricComponent, HistoricDetailsComponent, HistoricListComponent, AuditHistoriesComponent, AptoHistoricComponent],
  imports: [
    CommonModule,
    HistoricRoutingModule,
    MDBBootstrapModulesPro.forRoot(),
    ToastModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule,
    
  ],
  providers: [MDBSpinningPreloader,]
})
export class HistoricModule { }
