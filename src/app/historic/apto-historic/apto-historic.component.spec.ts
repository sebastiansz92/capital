import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AptoHistoricComponent } from './apto-historic.component';

describe('AptoHistoricComponent', () => {
  let component: AptoHistoricComponent;
  let fixture: ComponentFixture<AptoHistoricComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AptoHistoricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AptoHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
