import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { ProjectsService } from 'src/app/projects/project.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-apto-historic',
  templateUrl: './apto-historic.component.html',
  styleUrls: ['./apto-historic.component.css']
})
export class AptoHistoricComponent implements OnInit {

  project;
  audits;
  apto;
  tower;
  owner = false;
  nomenclature;
  ready = false;


  constructor(private projectService: ProjectService, private route: ActivatedRoute, private router: Router,
    private projectsService: ProjectsService) { }

  ngOnInit() {
    //this.project = this.projectService.data.projects[this.route.snapshot.params.id];
    this.project = this.projectsService.project;
    this.nomenclature = this.projectsService.nomenclature;
    if(this.project == null || this.nomenclature == null) {
      this.router.navigate(['/historic']);
    }
    this.projectsService.getAuditsByNomenclature(this.nomenclature?.id)
      .subscribe(res => {
        this.audits = res.body['data'].pageContent;
        if (this.route.snapshot.params.owner != undefined) {
          this.owner = true;
          this.audits = this.audits.filter(audit => audit.auditor !== 'CONSTRUCTORA');
        } else {
          this.audits = this.audits.filter(audit => audit.auditor === 'CONSTRUCTORA');
        }
        this.ready = true;
      })
    this.tower = this.route.snapshot.params.tower;
    /* if (this.route.snapshot.params.owner != undefined) {
      this.owner = true;
      //this.audits = this.projectsService.getAudits(this.nomenclature.audits, this.nomenclature.projectTypeId, this.project, 'PROPIETARIO');
    } else {
      //this.audits = this.projectsService.getAudits(this.nomenclature.audits, this.nomenclature.projectTypeId, this.project);
    } */
  }

  goToAuditHistory(apto, i, j) {
    if(apto) {
      this.projectsService.setAudit(this.audits[j]);
      this.projectsService.setReformIndex(this.audits.length - (i + 1));
      if (this.route.snapshot.params.owner !== undefined) {
        this.router.navigate(['/historic', 'owner', 'true', 'project', this.route.snapshot.params.id, 'tower',
        this.tower, 'apto', apto, 'audit', i]);
      } else {
        this.router.navigate(['/historic', 'project', this.route.snapshot.params.id, 'tower', this.tower, 'apto', apto, 'audit', i]);
      }
    }
  }

  getDate(audit) {
    audit;
    if(audit) {
      let year = audit.year;
      let month = audit.month;
      let day = audit.day;
      return year + '-' + month + '-' + day;
    } else {
      return '---';
    }
  }


  getSpaces(project, typeid) {
    return project['projectTypes'].find(type => type.id == typeid).spaces;
  }

  exportTopdf(auditid, nomenclature, i) {
    this.projectsService.getAuditDetail(auditid).subscribe(res => {
      let dataToPdf;
    let table;
    let typeid = nomenclature.projectTypeId;
    let spaces = this.project.projectTypes.find(type => type.id === nomenclature.projectTypeId).spaces;
    let audit = res.body['data'];
    //let spaces = this.getSpaces(this.project, typeid)
    if (this.route.snapshot.params.owner !== undefined) {
      //dataToPdf = this.projectService.getOwnerAudit(this.route.snapshot.params.id, tower, apto, audit);
      table = this.mapToPdfTable2(spaces, audit, nomenclature, i);
    } else {
      //dataToPdf = this.projectService.getAudit(audit, spaces);
      table = this.mapToPdfTable(spaces, audit, nomenclature, i);
    }
    const data = { content: [
        { width: '*', text: '' },
        {
          width: 'auto',
          alignment: 'center',
          layout: {
            // code from lightHorizontalLines:
                        hLineWidth: function (i, node) {
                          return 0;
                        },
                        vLineWidth: function (i) {
                          return 0;
                        },
                        paddingLeft: function (i) {
                          return i === 0 ? 0 : 8;
                        },
                        paddingRight: function (i, node) {
                          return (i === node.table.widths.length - 1) ? 0 : 8;
                        },
            // code for zebra style:
                    fillColor: function (i, node) {
                      return (i % 2 === 0) ? '#edf8ff' : null;
                    }
                  },
          table: {
            alignment: 'center',
            widths: ['*', '*', '*', '*'],
            body: table
          },
        },
        { width: '*', text: '' }
      ],
      styles: {
        header: {
          bold: true,
          color: 'white',
          fillColor: '#359bcc',
          margin: [2, 2, 2, 2]
        },
        subheader: {
          bold: true,
          color: 'white',
          fillColor: '#359bcc',
          margin: [2, 2, 2, 2]
        },
        lastValue: {
          margin: [0, 3, 0, 20]
        },
        value: {
          margin: [0, 3, 0, 3]
        }
      }
    };

    this.generatePdf('download', data, nomenclature);
    });
    
  }

  generatePdf(action = 'open', data, nomenclature) {
    const pdfName = 'audit_' + this.project.name + '_' + '_' + nomenclature.number;
    const documentDefinition = data;
    switch (action) {
      case 'open':
        pdfMake.createPdf(documentDefinition).open();
        break;
      case 'print':
        pdfMake.createPdf(documentDefinition).print();
        break;
      case 'download':
        pdfMake.createPdf(documentDefinition).download(pdfName);
        break;
      default:
        pdfMake.createPdf(documentDefinition).open();
        break;
    }
  }

  mapToPdfTable(spa, audits, nomenclature, i) {
    let spacesArray = [];
    
    let spaces = this.projectsService.mapAuditsToSpaces(spa, audits, 'PENDIENTE');
    console.log(spaces);
    spaces.audits.forEach(space => {
      const header = [{text: space.name, colSpan: 4, style: 'header', alignment: 'center'}, {}, {}, {}];
      const subheaders = [ {text: 'Nombre', style: 'subheader', width: '*'}, {text: 'Obs Visita', style: 'subheader'},
    {text: 'Obs Pendiente', style: 'subheader'}, {text: 'Aprobado', style: 'subheader'}];
      spacesArray.push(header);
      spacesArray.push(subheaders);
      const features = space.features.forEach((feature, index) => {
        console.log(feature);
        //let audit = audits.find(aud => aud.feature.id == feature.id);
        /* if (index != space.features.length - 1) { */
        spacesArray.push(
            [
              {text: this.validateEmptyField(feature.reformName || feature.feature), style: 'value'},
              {text: this.validateEmptyField(feature?.visitNote || '.'), style: 'value'},
              {text: this.validateEmptyField(feature?.observation || '.'), style: 'value'},
              {text: this.validateEmptyField((feature?.approved? 'Sí' : 'No' ) || '.'), style: 'value'}
        ]);
      });
      spacesArray.push(features);
      spacesArray = spacesArray.filter(element => element !== undefined);
    });
    let reformsspaces = [];
    nomenclature.reforms.forEach((ref, index) => {
      reformsspaces.push({
        name: ref.name
      })
    })
    //let reforms = this.getReforms(i, nomenclature);
    let reformsids = nomenclature.reforms.map(elem => elem.id);
    let audsReforms = audits.filter(aud => aud.reform !== undefined);
    let reforms = audsReforms.map(elem => {
      let name = reformsspaces.find(ref => ref.name == elem.reform.name).name;
      return [
        {text: this.validateEmptyField(name), style: 'value'},
              {text: this.validateEmptyField(elem.visitNote), style: 'value'},
              {text: this.validateEmptyField(elem.observation), style: 'value'},
              {text: this.validateEmptyField(elem.approved? 'Sí' : 'No'), style: 'value'}
      ]
    })
    if(reforms?.length > 0) {
      spacesArray = spacesArray.concat(reforms);
    }
    console.log(spacesArray);
    return(spacesArray);
  }

  getReforms(i, nomenclature) {
    let pdfData = [];
    let array = [];
    this.projectsService.setReformIndex(this.audits.length - (i + 1));

    // añadir cabecera de la reforma en el pdf
    if(nomenclature.reforms.length > 0) {
      if (!this.owner) {
        pdfData.push([{text: 'Reformas', colSpan: 4, style: 'header', alignment: 'center'}, {}, {}, {}]);
        pdfData.push([ {text: 'Nombre', style: 'subheader', width: '*'}, {text: 'Obs Visita', style: 'subheader'},
        {text: 'Obs Pendiente', style: 'subheader'}, {text: 'Aprobado', style: 'subheader'}]);
      } else {
        pdfData.push([{text: 'Reformas', colSpan: 4, style: 'header', alignment: 'center'}, {}, {}, {}]);
        pdfData.push([ {text: 'Nombre', style: 'subheader', width: '*'}, {text: 'Observaciones', style: 'subheader', colSpan: 2}, {},
        {text: 'Aprobado', style: 'subheader'}]);
      }
    }

    nomenclature.reforms.forEach((ref, index) => {
      // Historial de proyectos, retornar todos los campos de la auditoria
      if (!this.owner) {
        array = nomenclature.reforms[index].audits.filter(x => x.auditor != 'PROPIETARIO');
        if ((array.length - this.projectsService.reformIndex - 1) >= 0) {
          let audi = array[array.length - this.projectsService.reformIndex - 1];
          pdfData.push(
            [
              {text: this.validateEmptyField(ref.name), style: 'value'},
              {text: this.validateEmptyField(audi.visitNote), style: 'value'},
              {text: this.validateEmptyField(audi.observation), style: 'value'},
              {text: this.validateEmptyField(audi.approved? 'Sí' : 'No'), style: 'value'}
        ]);
          }
      } else {
        // Historial de entregas, retornar solo los campos de entrega
        array = nomenclature.reforms[index].audits.filter(x => x.auditor == 'PROPIETARIO');
        if ((array.length - this.projectsService.reformIndex - 1) >= 0) {
          let audi = array[array.length - this.projectsService.reformIndex - 1];
          pdfData.push(
            [
              {text: this.validateEmptyField(ref.name), style: 'value'},
              {text: this.validateEmptyField(audi.visitNote), style: 'value', colSpan: 2}, {},
              {text: this.validateEmptyField(audi.approved? 'Sí' : 'No'), style: 'value'}
        ]);
          }
      }
    });

    return pdfData;
  }

  mapToPdfTable2(spaces, audits, nomenclature, i) {
    let spacesArray = [];
    spaces.forEach(space => {
      const header = [{text: space.name, colSpan: 4, style: 'header', alignment: 'center'}, {}, {}, {}];
      const subheaders = [ {text: 'Nombre', style: 'subheader', width: '*'},
      {text: 'Observaciones', style: 'subheader', colSpan: 2}, {},
      {text: 'Aprobado', style: 'subheader'}];
      spacesArray.push(header);
      spacesArray.push(subheaders);
      const features = space.features.forEach((feature, index) => {
        let audit = audits.find(aud => aud.feature.id == feature.id);
        /* if (index != space.features.length - 1) { */
        spacesArray.push(
            [
              {text: this.validateEmptyField(feature.name), style: 'value'},
              {text: this.validateEmptyField(audit.visitNote), style: 'value', colSpan: 2}, {},
              {text: this.validateEmptyField(audit.approved? 'Sí' : 'No'), style: 'value'}
        ]);
      });
      spacesArray.push(features);
      spacesArray = spacesArray.filter(element => element !== undefined);
    });
    //let reforms = this.getReforms(i, nomenclature);
    let reformsspaces = [];
    nomenclature.reforms.forEach((ref, index) => {
      reformsspaces.push({
        name: ref.name
      })
    })
    //let reforms = this.getReforms(i, nomenclature);
    let reformsids = nomenclature.reforms.map(elem => elem.id);
    let audsReforms = audits.filter(aud => aud.reform !== undefined);
    let reforms = audsReforms.map(elem => {
      let name = reformsspaces.find(ref => ref.name == elem.reform.name).name;
      return [
        {text: this.validateEmptyField(name), style: 'value'},
              {text: this.validateEmptyField(elem.visitNote), style: 'value'},
              {text: this.validateEmptyField(elem.observation), style: 'value'},
              {text: this.validateEmptyField(elem.approved? 'Sí' : 'No'), style: 'value'}
      ]
    })
    if(reforms?.length > 0) {
      spacesArray = spacesArray.concat(reforms);
    }
    return(spacesArray);
  }

  validateEmptyField(field) {
    return field !== '' ? field : '---';
  }

}
