import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { ProjectsService } from 'src/app/projects/project.service';
import { contract } from './contract';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: "app-historic-list",
  templateUrl: "./historic-list.component.html",
  styleUrls: ["./historic-list.component.css"],
})
export class HistoricListComponent implements OnInit {
  project;
  noms = [];
  date;
  audits;
  ready = false;
  towers;
  owner = false;
  day;
  month;
  year;
  keys = ['proprietaryName', 'proprietaryIdentification', 'proprietaryEmail', 'address', 'city', 'deliveryCity', 'parking', 'utilRoom', 'energy',
  'aqueduct', 'gas'];
  multimedia = 'http://68.183.97.247:8091/';
  signatures = [];
  CONTRACT = contract;
  paginadores: any;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private router: Router,
    private projectsService: ProjectsService
  ) {}

  ngOnInit() {
    this.owner = this.route.snapshot.params.owner;
    this.getDate2();
    this.projectsService.getProjectById(this.route.snapshot.params.id).subscribe(res => {
      this.project = res.body['data'];
      this.towers = this.project.towers;
      console.log(this.towers);
      /* if (this.owner == undefined) {
        this.towers = this.project.towers;
      } else {
        this.towers = this.project.towers.map(tower => {
          // solo los apartamentos aprobados o entregados
          return {...tower, nomenclaturesPage: {...tower.nomenclaturesPage, pageContent: tower.nomenclaturesPage.pageContent.filter(nom => nom.state == 'APROBADO' || nom.state == 'ENTREGADO')} };
        }).filter(tow => tow.nomenclaturesPage?.pageContent?.length > 0);
      } */
      this.ready = true;
      if(this.owner) {
        this.towers.forEach(tower => {
          this.projectsService.getNomenclaturesByTowerAndState(tower.id, 0, 10, 'APROBADO').subscribe(
            (res) => {
              tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
              tower.nomenclaturesCount = tower.nomenclaturesPage.pageContent.length;
              this.paginadores = this.towers.map(tower => ({page: 0, size: 10, tower: tower.id, pages: Array.from(Array(Math.ceil(tower.nomenclaturesCount / (10)))).map((x, i) => i )}));
            }
            )
          });
        } else {
          this.paginadores = this.towers.map(tower => ({page: 0, size: 10, tower: tower.id, pages: Array.from(Array(Math.ceil(tower.nomenclaturesCount / (10)))).map((x, i) => i )}));
        }
    });
  }

  setPaginador(paginador, index, tower) {
    paginador.page = index;
    if(!this.owner) {
      this.projectsService.getNomenclaturesByTower(tower.id, paginador.page, paginador.size).subscribe(
        (res) => {
          tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
        }
      )
    } else {
      this.projectsService.getNomenclaturesByTowerAndState(tower.id, paginador.page, paginador.size, 'APROBADO').subscribe(
        (res) => {
          tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
        }
      )
    }
  }

  next(paginador, tower) {
    if((paginador.page + 1) !== paginador.pages.length) {
      paginador.page = paginador.page + 1;
      if(!this.owner) {
        this.projectsService.getNomenclaturesByTower(tower.id, paginador.page, paginador.size).subscribe(
          (res) => {
            tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
          }
        )
      } else {
        this.projectsService.getNomenclaturesByTowerAndState(tower.id, paginador.page, paginador.size, 'APROBADO').subscribe(
          (res) => {
            tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
          }
        )
      }
    }
  }

  previous(paginador, tower) {
    if(paginador.page !== 0) {
      paginador.page = paginador.page - 1;
      if(!this.owner) {
        this.projectsService.getNomenclaturesByTower(tower.id, paginador.page, paginador.size).subscribe(
          (res) => {
            tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
          }
        )
      } else {
        this.projectsService.getNomenclaturesByTowerAndState(tower.id, paginador.page, paginador.size, 'APROBADO').subscribe(
          (res) => {
            tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
          }
        )
      }
    }
  }

  goToAuditHistory(tower, apto, nomenclature) {
    if (this.route.snapshot.params.owner !== undefined) {
      this.projectsService.setNomenclature(nomenclature);
      this.projectsService.setCurrentProject(this.project);
      this.router.navigate([
        "/historic",
        "owner",
        "true",
        "project",
        this.route.snapshot.params.id,
        "tower",
        tower,
        "apto",
        apto,
      ]);
    } else {
      this.projectsService.setNomenclature(nomenclature);
      this.projectsService.setCurrentProject(this.project);
      this.router.navigate([
        "/historic",
        "project",
        this.route.snapshot.params.id,
        "tower",
        tower,
        "apto",
        apto,
      ]);
    }
  }

  getDate(audit) {
    /* let audits;
    if (this.route.snapshot.params !== undefined) {
      audits = this.projectService.data.projects[this.route.snapshot.params.id]
        .ownerAudits[tower][nom];
    } else {
      audits = this.projectService.data.projects[this.route.snapshot.params.id]
        .audits[tower][nom];
    }
    return audits[audits.length - 1].date; */
    //let audit = audits[audits.length - 1];
    let year = audit.year;
    let month = audit.month;
    let day = audit.day;
    return year + '-' + month + '-' + day;
  }

  toDataURL = (src) => {
    var outputFormat = 'image/png'
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function() {
      var canvas = document.createElement('CANVAS') as HTMLCanvasElement;
      var ctx = canvas.getContext('2d');
      var dataURL;
      canvas.height = 100;
      canvas.width = window.innerWidth;
      ctx.drawImage(src, 0, 0);
      dataURL = canvas.toDataURL(outputFormat);
    };
    img.src = src;
    if (img.complete || img.complete === undefined) {
      img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
      img.src = src;
    }
  }

  getContract(tower, apto) {
    let url = this.multimedia + apto.contract.urlRelativeContract;
    window.open(url, '_blank');
    /* this.replaces(tower,apto);
    //this.toDataURL(this.getImage(apto.contract.signatures[0].resource_url));
    //this.imageToBase64(this.getImage(apto.contract.signatures[0].resource_url));
    const data = { content: [
      { width: '*', text: this.project, style: 'title', alignment: 'center' },
      { width: '*', text: tower, style: 'title', alignment: 'center' },
      { width: '*', text: apto.number, style: 'title', alignment: 'center' },
        { width: '*', text: '\n\r' },
        { width: '*', text: this.CONTRACT },
        { width: '*', text: '\n\r' },
        //this.getTable(apto.contract.signatures),
      ],
      styles: {
        sign: {
          color: 'blue',
          fontSize: 10
        },
        title: {
          bold: true,
          fillColor: '#359bcc',
          margin: [2, 2, 2, 2]
        },
        lastValue: {
          margin: [0, 3, 0, 20]
        },
        value: {
          margin: [0, 3, 0, 3]
        }, header: {
          bold: true,
          color: 'white',
          fillColor: '#359bcc',
          margin: [2, 2, 2, 2]
        },
        subheader: {
          bold: true,
          color: 'white',
          fillColor: '#359bcc',
          margin: [2, 2, 2, 2]
        }
      }
    };
    
    this.generatePdf("download", data); */
  }

  imageToBase64 = (URL) => {
    var img = new Image,
    canvas = document.createElement("canvas"),
    ctx = canvas.getContext("2d"),
    src = URL // insert image url here

    img.crossOrigin = "Anonymous";

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage( img, 0, 0 );
        localStorage.setItem( "savedImageData", canvas.toDataURL("image/png") );
    }
    img.src = src;
    // make sure the load event fires for cached images too
    if ( img.complete || img.complete === undefined ) {
        img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        img.src = src;
    }
}

  replaces(tower, apto) {
    this.keys.forEach(key => {
      this.CONTRACT = this.CONTRACT.replace('$'+key, apto.contract[key]);
    });
    this.CONTRACT = this.CONTRACT.replace('$project', this.project.name);
    this.CONTRACT = this.CONTRACT.replace('$tower', tower);
    this.CONTRACT = this.CONTRACT.replace('$nom', apto.number);

    this.CONTRACT = this.CONTRACT.replace('$day', this.day);
    this.CONTRACT = this.CONTRACT.replace('$month', this.month);
    this.CONTRACT = this.CONTRACT.replace('$year', this.year);
  }

  getDate2() {
    let date = new Date();
    this.day = date.getDate();
    this.month = this.findMonth(date.getMonth());
    this.year = date.getFullYear();
  }

  findMonth(month){
    switch (month) {
      case 0:
        return 'Enero';
      case 1:
        return 'Febrero';
      case 2:
        return 'Marzo';
      case 3:
        return 'Abril';
      case 4:
        return 'Mayo';
      case 5:
        return 'Junio';
      case 6:
        return 'Julio';
      case 7:
        return 'Agosto';
      case 8:
        return 'Septiembre';
      case 9:
        return 'Octubre';
      case 10:
        return 'Noviembre';
      case 11:
        return 'Diciembre';
    }
  }

  generatePdf(action = "open", data) {
    const documentDefinition = data;
    switch (action) {
      case "open":
        pdfMake.createPdf(documentDefinition).open();
        break;
      case "print":
        pdfMake.createPdf(documentDefinition).print();
        break;
      case "download":
        pdfMake.createPdf(documentDefinition).download();
        break;
      default:
        pdfMake.createPdf(documentDefinition).open();
        break;
    }
  }

  haveContracts(tower, apto?) {
    let contracts = this.project.contracts;
    if (apto == undefined) {
      if (contracts[tower]) {
        return true;
      }
    }
    if (contracts[tower]) {
      if (contracts[tower][apto]) {
        return true;
      }
    } else {
      return false;
    }
    return false;
  }

  getImage(image_url){
    let url = image_url.substr(10);
    return this.multimedia + image_url;
  }

  getTable(signs) {
    return {
      layout: 'noBorders', // optional
      table: {
        // headers are automatically repeated if the table spans over multiple pages
        // you can declare how many rows should be treated as headers
        headerRows: 1,
        widths: [ 'auto', 70, 'auto'],

        body: [
          [{text: 'Firma', style:'sign'}, ' ', {text: 'Firma', style:'sign'}],
          [ {stack: [{image: this.getImage(signs[0].resource_url), width: 200}]},{text: ' ', width: 20}, {stack: [{image: this.getImage(signs[1].resource_url), width: 200}]}],
          [ {text: 'propietario', alignment: 'center'}, ' ', {text: 'constructora', alignment: 'center'} ]
        ]
      }
    }
  }

  
}
