import { NgModule} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProjectsComponent } from './projects/projects.component';
import { NewProjectComponent } from './new-project/new-project.component';
import { ProyectDetailsComponent } from './audit/proyect-details/proyect-details.component';
import { SpacesComponent } from './audit/spaces/spaces.component';
import { AdminGuard } from './admin/admin.guard';
import { SignupComponent } from './signup/signup.component';
import { DeactivateGuard } from './shared/services/http/deactiveGuard';

const routes: Routes = [
{ path: '', component: LoginComponent },
{ path: 'login', component: LoginComponent },
{ path: 'signup', component: SignupComponent, canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN']}},
{ path: 'projects', component: ProjectsComponent , canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA', 'RESIDENTE_OBRA']}, canDeactivate: [DeactivateGuard]},
{ path: 'projects/cache', component: ProjectsComponent , canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA', 'RESIDENTE_OBRA']}},
{ path: 'new_project', component: NewProjectComponent , canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA']}, canDeactivate: [DeactivateGuard]},
{ path: 'new_project/:cacheid', component: NewProjectComponent , canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA']}, canDeactivate: [DeactivateGuard]},
{ path: 'project/:id', component: ProyectDetailsComponent, canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA', 'RESIDENTE_OBRA']}},
{ path: 'editProject/:id', component: NewProjectComponent, canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA']}},
{ path: 'project/:projectId/tower/:towerId/apto/:aptoIndex', component: SpacesComponent, canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA', 'RESIDENTE_OBRA']}},
{ path: 'historic', redirectTo: '/historic', pathMatch: 'full'},
{ path: 'reform', redirectTo: '/reform', pathMatch: 'full'},
{ path: 'delivery', redirectTo: '/delivery', pathMatch: 'full'},
{ path: 'offline-projects', redirectTo: '/offline-projects', pathMatch: 'full'}
];

@NgModule({
imports: [RouterModule.forRoot(routes, { useHash: false, relativeLinkResolution: 'legacy' })],
exports: [RouterModule]
})
export class AppRouting {}
export const routingComponents = [
];


