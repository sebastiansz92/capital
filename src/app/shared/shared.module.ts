import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './services/http/auth-interceptor.service';
import { FormsModule } from '@angular/forms';
import { OfflineModalComponent } from './offline-modal/offline-modal.component';
import { DesistidoModalComponent } from './desistido-modal/desistido-modal.component';
import { ButtonsModule, CheckboxModule, InputsModule, WavesModule } from 'ng-uikit-pro-standard';



@NgModule({
  declarations: [OfflineModalComponent, DesistidoModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    InputsModule, WavesModule, ButtonsModule, CheckboxModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  exports : [
    CommonModule
  ]
})
export class SharedModule { }
