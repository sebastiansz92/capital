import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MDBModalRef } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent implements OnInit {
  action: Subject<any> = new Subject();
  question: string;

  constructor(public modalRef: MDBModalRef) { }

  ngOnInit() {
  }

  onYesClick() {
    this.action.next(true);
    this.modalRef.hide();
  }

  onNoClick() {
    this.action.next(false);
    this.modalRef.hide();
  }

}
