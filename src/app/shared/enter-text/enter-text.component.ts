import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-enter-text',
  templateUrl: './enter-text.component.html',
  styleUrls: ['./enter-text.component.css']
})
export class EnterTextComponent implements OnInit {

  action: Subject<any> = new Subject();
  question: string;
  answer: string;

  constructor(public modalRef: MDBModalRef) { }

  ngOnInit() {
  }

  confirmar() {
    this.action.next({
      answer: this.answer
    });
    this.modalRef.hide();
  }

  cancelar() {
    this.action.next(false);
    this.modalRef.hide();
  }

}
