import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesistidoModalComponent } from './desistido-modal.component';

describe('DesistidoModalComponent', () => {
  let component: DesistidoModalComponent;
  let fixture: ComponentFixture<DesistidoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesistidoModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesistidoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
