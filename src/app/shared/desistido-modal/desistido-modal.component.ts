import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-desistido-modal',
  templateUrl: './desistido-modal.component.html',
  styleUrls: ['./desistido-modal.component.css']
})
export class DesistidoModalComponent implements OnInit {

  action: Subject<any> = new Subject();
  question: string;
  nomenclature: any;
  show: boolean = false;
  reason: string = '';
  newType;
  projectTypes: any;

  constructor(public modalRef: MDBModalRef) { }

  ngOnInit() {
    this.newType = this.nomenclature.projectTypeId
  }

  onYesClick() {
    this.action.next({reason: this.reason, newType: this.newType, sameType: !this.show});
    this.modalRef.hide();
  }

  onNoClick() {
    this.action.next(false);
    this.modalRef.hide();
  }

}
