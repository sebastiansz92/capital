import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  data = {
    projects : [
        {
            name: 'Proyecto 1',
            img_url: 'https://informativojuridico.com/wp-content/uploads/2015/04/proyecto-construcci%C3%B3n.jpg',
            towersCount: 1,
            towers: ['Torre1'],
            noms: [{tower: 'Torre1', nom: '101', status: 'Pendiente', type: 'Apartamento'},
            {tower: 'Torre1', nom: '102', status: 'Reprogramado', type: 'Apartamento'},
            {tower: 'Torre1', nom: '103', status: 'Aprobado', type: 'Apartamento'}],
            aptos: 3,
            types: [
                {
                    name: 'Apartamento',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Comedor',
                            features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos']
                        }
                    ]
                },
                {
                    name: 'Aparta-estudio',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        }
                    ]
                },
                {
                    name: 'Casa',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Comedor',
                            features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        },
                        {
                            name: 'Patio',
                            features: ['Jardin', 'Juegos']
                        },
                        {
                            name: 'Sala',
                            features: ['Ventanas', 'Muebles', 'Enchufes', 'Luces']
                        },
                        {
                            name: 'Baño',
                            features: ['Inodoro', 'Ducha', 'Tina', 'Espejos', 'Cajones']
                        }
                    ]
                }
            ],
            audits: [],
            ownerAudits: [],
            contracts: []
        },
        {
            name: 'Proyecto 2',
            img_url: 'https://www.portafolio.co/files/article_multimedia/uploads/2018/09/02/5b8c8f78a67e9.jpeg',
            towersCount: 1,
            towers: ['Torre1'],
            noms: [{tower: 'Torre1', nom: '101', status: 'Pendiente', type: 'Apartamento'},
            {tower: 'Torre1', nom: '102', status: 'Pendiente', type: 'Apartamento'}],
            aptos: 2,
            types: [
                {
                    name: 'Apartamento',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Comedor',
                            features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        }
                    ]
                },
                {
                    name: 'Aparta-estudio',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        }
                    ]
                },
                {
                    name: 'Casa',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Comedor',
                            features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        },
                        {
                            name: 'Patio',
                            features: ['Jardin', 'Juegos']
                        },
                        {
                            name: 'Sala',
                            features: ['Ventanas', 'Muebles', 'Enchufes', 'Luces']
                        },
                        {
                            name: 'Baño',
                            features: ['Inodoro', 'Ducha', 'Tina', 'Espejos', 'Cajones']
                        }
                    ]
                }
            ],
            audits: [],
            ownerAudits: [],
            contracts: []
        },
        {
            name: 'Proyecto 3',
            img_url: 'https://www.portafolio.co/files/article_multimedia/uploads/2018/09/02/5b8c8f78a67e9.jpeg',
            towersCount: 1,
            towers: ['Torre1'],
            noms: [{tower: 'Torre1', nom: '101', status: 'Pendiente', type: 'Apartamento'},
            {tower: 'Torre1', nom: '102', status: 'Pendiente', type: 'Apartamento'},
            {tower: 'Torre1', nom: '103', status: 'Pendiente', type: 'Apartamento'},
            {tower: 'Torre1', nom: '104', status: 'Pendiente', type: 'Apartamento'}],
            aptos: 4,
            types: [
                {
                    name: 'Apartamento',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Comedor',
                            features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        }
                    ]
                },
                {
                    name: 'Aparta-estudio',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        }
                    ]
                },
                {
                    name: 'Casa',
                    spaces: [
                        {
                            name: 'Alcoba',
                            features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Comedor',
                            features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
                        },
                        {
                            name: 'Cocina',
                            features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
                        },
                        {
                            name: 'Patio',
                            features: ['Jardin', 'Juegos']
                        },
                        {
                            name: 'Sala',
                            features: ['Ventanas', 'Muebles', 'Enchufes', 'Luces']
                        },
                        {
                            name: 'Baño',
                            features: ['Inodoro', 'Ducha', 'Tina', 'Espejos', 'Cajones']
                        }
                    ]
                }
            ],
            audits: [],
            ownerAudits: [],
            contracts: []
        }
    ]
};

newProject = {
  city: '',
  imageBase64: '',
  actualTower: '',
  name: '',
  towersCount: 1,
  numAptos: 0,
  towers: [],
  noms: [],
  types: [
    /* {
        name: 'Apartamento',
        spaces: [
            {
                name: 'Alcoba',
                features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
            },
            {
                name: 'Comedor',
                features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
            },
            {
                name: 'Cocina',
                features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
            }
        ]
    },
    {
        name: 'Aparta-estudio',
        spaces: [
            {
                name: 'Alcoba',
                features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
            },
            {
                name: 'Cocina',
                features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
            }
        ]
    },
    {
        name: 'Casa',
        spaces: [
            {
                name: 'Alcoba',
                features: ['Closet', 'Ventanas', 'Enchufes', 'luces']
            },
            {
                name: 'Comedor',
                features: ['Barra', 'Ventanas', 'Enchufes', 'luces']
            },
            {
                name: 'Cocina',
                features: ['Estufa', 'Barra', 'Lavaplatos', 'Cajones']
            },
            {
                name: 'Patio',
                features: ['Jardin', 'Juegos']
            },
            {
                name: 'Sala',
                features: ['Ventanas', 'Muebles', 'Enchufes', 'Luces']
            },
            {
                name: 'Baño',
                features: ['Inodoro', 'Ducha', 'Tina', 'Espejos', 'Cajones']
            }
        ]
      } */
    ]
};

addType(name: string, index?) {
    if (this.newProject.types.filter(type => type.name == name).length < 1) {
        this.newProject.types.push({ name, spaces: []});
    }
}

deleteType (type) {
    this.newProject.types = this.newProject.types.filter(typ => typ.name !== type.name);
}

setInitialState() {
    this.newProject.numAptos = 0;
    this.newProject.name = '';
    this.newProject.towers = [];
    this.newProject.actualTower = '';
    this.newProject.types = [];
}

getData() {
  return this.data;
}



getNewProject() {
  return this.newProject;
}

setProject(project) {
    this.newProject = project;
}

addSpace(index, newSpace, projectIndex?) {
    this.newProject.types[index].spaces.push(newSpace);
    if (projectIndex != null) {
        this.data.projects[projectIndex].types[index].spaces.push(newSpace);
    }
}

addProject(project) {
  this.data.projects.push(project);
}

modifyProject(data) {
  this.newProject = {...this.newProject, ...data};
}

editStatus(projectIndex, apto, status) {
    const aptoIndex = this.data.projects[projectIndex].noms.indexOf(apto);
    this.data.projects[projectIndex].noms[aptoIndex].status = status;
}

editProject(index, data) {
    this.data.projects[index] = {...this.data.projects[index], ...data};
}

getProject(index) {
    return this.data.projects[index];
}

changeType(i, type) {
    this.newProject.noms[i]['type'] = type;
}

deleteProject(index) {
    this.data.projects.splice(index, 1);
}

addAudit(index, data) {
    if(this.data.projects[index].audits[data.tower]) {
        if (this.data.projects[index].audits[data.tower][data.apto]) {
            this.data.projects[index].audits[data.tower][data.apto].push(data);
        } else {
            this.data.projects[index].audits[data.tower][data.apto] = [];
            this.data.projects[index].audits[data.tower][data.apto].push(data);
        }
    } else {
        this.data.projects[index].audits[data.tower] = [];
        this.data.projects[index].audits[data.tower][data.apto] = [];
        this.data.projects[index].audits[data.tower][data.apto].push(data);
    }
}

addOwnerAudit(index, data) {
    if(this.data.projects[index].ownerAudits[data.tower]) {
        if (this.data.projects[index].ownerAudits[data.tower][data.apto]) {
            this.data.projects[index].ownerAudits[data.tower][data.apto].push(data);
        } else {
            this.data.projects[index].ownerAudits[data.tower][data.apto] = [];
            this.data.projects[index].ownerAudits[data.tower][data.apto].push(data);
        }
    } else {
        this.data.projects[index].ownerAudits[data.tower] = [];
        this.data.projects[index].ownerAudits[data.tower][data.apto] = [];
        this.data.projects[index].ownerAudits[data.tower][data.apto].push(data);
    }
}

getAudit(audit, type) {

}

/* getAudit(projectid, tower, apto, audit?) {
    const data = this.data.projects[projectid].audits[tower];
    if (data) {
        if (audit) {
            if( audit !== 'last') {
                const audindex = this.checkautid(data[apto], audit);
                const formsData = this.mapFormDataToJson(data[apto][audindex]);
                return formsData;
            } else {
                const audindex = this.checkautid(data[apto], data[apto].length - 1);
                //console.log(data[apto].map(aud => this.mapFormDataToJson(aud)));
                // const formsData = data[apto][audindex].map(aud => this.mapFormDataToJson(aud));
                return this.mapFormDataToJson(data[apto][audindex]);
            }
        } else {
            const audindex = this.checkautid(data[apto], audit);
            const formsData = this.mapFormDataToJson(data[apto][audindex])
            return formsData;
        }
    } else {
        return undefined;
    }
} */

checkautid(array, auditIndex) {
    if(auditIndex > 0) {
        if (array[auditIndex].desistido) {
            console.log('entró por desistido');
            return this.checkautid(array, auditIndex - 1);
        } else {
            return auditIndex;
        }
    } else {
        return auditIndex;
    }
}

getContract(projectid, tower, apto) {
    const data = this.data.projects[projectid].contracts[tower];
    if (data) {
        if (data[apto]) {
            return data[apto][data[apto].length - 1];
        } else {
            return undefined
        }
    } else {
        return undefined;
    }
}

addContract(index, tower, apto, contract) {
    if (this.data.projects[index].contracts[tower]) {
        if (this.data.projects[index].contracts[tower][apto]) {
            this.data.projects[index].contracts[tower][apto].push(contract);
        } else {
            this.data.projects[index].contracts[tower][apto] = [];
            this.data.projects[index].contracts[tower][apto].push(contract);
        }
    } else {
        this.data.projects[index].contracts[tower] = [];
        this.data.projects[index].contracts[tower][apto] = [];
        this.data.projects[index].contracts[tower][apto].push(contract);
    }
}

/* getOwnerAudit(projectid, tower, apto, ownerAudit?) {
    const data = this.data.projects[projectid].ownerAudits[tower]
    if (data) {
        if (ownerAudit) {
            if ( ownerAudit !== 'last') {
                const audindex = this.checkautid(data[apto], ownerAudit);
                const formsData = this.mapFormDataToJson(data[apto][audindex])
                return formsData;
            } else {
                const audindex = this.checkautid(data[apto], data[apto].length - 1);
                const formsData = this.mapFormDataToJson(data[apto][audindex])
                return formsData;
            }
        } else {
            const formsData = this.mapFormDataToJson(data[apto][0]);
            return formsData;
        }
    } else {
        return undefined;
    }
} */

/* mapFormDataToJson(audit) {
    const obsGenerales = audit.form.value.obsGenerales;
    const spaces = audit.form.value.spaces.map(space => {
        const spacename = space.name;
        const features = space.features.map(feature => ({...feature, aproved: feature.aproved ? 'Sí' : 'No'}));
        return { space: spacename, features};
    });
    return {obsGenerales,
        spaces
    };
} */
/* mapFormDataToJson(audits, spaces) {
    spaces = audit.map()
} */

mapFormDataToJsonOwner(audit) {
    const obsGenerales = audit.form.value.obsGenerales;
    const spaces = audit.form.value.spaces.map(space => {
        const spacename = space.name;
        const features = space.features.map(feature => ({...feature, aproved: feature.aproved ? 'Sí' : 'No'}));
        return { space: spacename, features};
    });
    return {obsGenerales,
        spaces
    };
}

changeState(index, tower, apto) {
    this.data.projects[index].noms.find(nom => nom.tower == tower && nom.nom == apto)['status'] = 'Entregado';
}

getReforms(projectId, tower, apto) {
    return this.data.projects[projectId].noms.find(nom => nom.tower === tower && nom.nom === apto);
}

addReforms(projectid, tower, apto, data) {
    let apartment = this.data.projects[projectid].noms.find(nom => nom.tower === tower && nom.nom === apto);
    if(apartment['reforms']) {
        data.forEach(reform => {
            apartment['reforms'].push(reform);
        });
    } else {
        apartment['reforms'] = data;
    }
}

getSpaces(projectId, type, tower, apto) {
    let spaces = this.data.projects[projectId].types.find(x => x.name == type).spaces;
    if (this.data.projects[projectId].noms.find(x => x.nom == apto.nom && x.tower == tower)['reforms']) {
        const keys: string [] = Object.values(this.data.projects[projectId]
            .noms.find(x => x.nom == apto.nom && x.tower == tower)['reforms'].map(x => x.description));
        const newSpace = { name: 'Reformas', features : keys}
        let space = spaces.find(x => x.name == 'Reformas');
        if ( space ) {
            space['features'] = keys;
        } else {
            spaces.push(newSpace);
        }
    }
    return spaces;
}

changeAptoType(index, nom, tower, type) {
    this.data.projects[index].noms.find(x => x.nom == nom && x.tower == tower)['type'] = type;
    let project = this.data.projects[index];
    if (project.audits[tower]) {
        if(project.audits[tower][nom]) {
            if(project.audits[tower][nom].length > 0) {
                project.audits[tower][nom].push({desistido: type});
            }
        }
    }
    if (project.ownerAudits[tower]) {
        if(project.ownerAudits[tower][nom]) {
            if(project.ownerAudits[tower][nom].length > 0) {
                project.ownerAudits[tower][nom].push({desistido: type});
            }
        }
    }
}

  constructor() { }
}
