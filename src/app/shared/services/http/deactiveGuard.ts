import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { NewProjectComponent } from 'src/app/new-project/new-project.component';

@Injectable()
export class DeactivateGuard implements CanDeactivate<NewProjectComponent> {

    canDeact = true

  canDeactivate() {
    return this.canDeact
  }
}