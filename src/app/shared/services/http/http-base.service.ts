import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { finalize } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class HttpBaseService {
  constructor(private http: HttpClient) {}

  /* Cambio para usar en json server, sin puerto y sin root_path */
  /* private BASE_URL = `${environment.url_server}:${environment.port_server}/${environment.root_path}/`; */
  private BASE_URL = `http://68.183.97.247:8090/capital/`;//PRODUCCIÓN
  // private BASE_URL = `http://200.122.212.101:8090/capital/`

  protected post(
    relativeUrl: string,
    body: any,
    headers: HttpHeaders = new HttpHeaders()
  ) {
    return this.http
      .post(`${this.BASE_URL}${relativeUrl}`, body, {
        headers,
        observe: "response",
      })
      .pipe(
        finalize(() => {
          // do something when ends
        })
      );
  }

  get(
    relativeUrl: string,
    headers: HttpHeaders = new HttpHeaders(),
    baseUrl = this.BASE_URL
  ) {
    return this.http
      .get(`${baseUrl}${relativeUrl}`, {
        headers,
        observe: "response",
      })
      .pipe(
        finalize(() => {
          // do something when ends
        })
      );
  }

  protected put(
    relativeUrl: string,
    body: any,
    headers: HttpHeaders = new HttpHeaders()
  ) {
    return this.http
      .put(`${this.BASE_URL}${relativeUrl}`, body, {
        headers,
        observe: "response",
      })
      .pipe(
        finalize(() => {
          // do something when ends
        })
      );
  }

  protected delete(
    relativeUrl: string,
    headers: HttpHeaders = new HttpHeaders()
  ) {
    return this.http
      .delete(`${this.BASE_URL}${relativeUrl}`, {
        headers,
        observe: "response",
      })
      .pipe(
        finalize(() => {
          // do something when ends
        })
      );
  }
}
