import { Component, OnInit } from "@angular/core";
import { MDBModalRef } from "ng-uikit-pro-standard";
import { Subject } from "rxjs";

@Component({
  selector: "app-offline-modal",
  templateUrl: "./offline-modal.component.html",
  styleUrls: ["./offline-modal.component.css"],
})
export class OfflineModalComponent implements OnInit {
  question: string;
  title: string;
  description: string;
  action: Subject<any> = new Subject();

  constructor(public modalRef: MDBModalRef) {}

  ngOnInit(): void {}

  accept() {
    this.action.next(true);
    this.modalRef.hide();
  }
}
