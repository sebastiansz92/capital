import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private router: Router) {}
  
  canActivate(
    next:ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
      const expectedRoles = next.data.expectedRoles as Array<String>;
      let role = localStorage.getItem('role');
      if (localStorage.getItem('isLoggedIn') == 'true' && role) {
        if(expectedRoles.indexOf(role) >= 0) {
          console.log
          return true;
        } else {
          this.router.parseUrl("/projects");
        }
      } else {
        return this.router.parseUrl("/login");
      }
    }
}
