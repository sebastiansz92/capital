import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ProjectService } from '../../shared/services/project.service';

@Component({
  selector: 'app-nomenclature',
  templateUrl: './nomenclature.component.html',
  styleUrls: ['./nomenclature.component.css']
})
export class NomenclatureComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private projectservice: ProjectService) { }

  @Input() nomenclatureForm: FormGroup;
  @Input() numAptos;
  @Input() noms;
  createdAptos = 0;

  ngOnInit() {
    this.nomenclatureForm.setValidators([this.validateAptos()]);
    if(this.noms && this.noms.length > 0) {
      this.noms.forEach(nom => {
        this.addRange(nom);
      })
    } else {
      this.addRange();
    }
  }

  get ranges() : FormArray {
    if(this.nomenclatureForm != undefined) {
      return this.nomenclatureForm.get('ranges') as FormArray;
    }
  }

  addRange(nom?) { 
    if(nom) {
      this.ranges.push(this.createRangeform(nom));
    } else {
      this.ranges.push(this.createRangeform());
    }
  }

  createRangeform(range?): FormGroup {
    if(range) {
      let form =  this.formBuilder.group({
        delpiso : [range.minPiso, [Validators.required]],
        alpiso: [range.maxPiso, [Validators.required]],
        numApartamentos: [range.count, [Validators.required]],
        nom1: [range.min, [Validators.required]],
        nom2: [range.max, [Validators.required]]
      }, {validator: Validators.compose([this.validateNoms(), this.validateNomsCount()])});
      this.generateNom(true);
      return form;
    } else {
      return this.formBuilder.group({
        delpiso : ['', [Validators.required]],
        alpiso: ['', [Validators.required]],
        numApartamentos: ['', [Validators.required]],
        nom1: ['', [Validators.required]],
        nom2: ['', [Validators.required]]
      }, {validator: Validators.compose([this.validateNoms(), this.validateNomsCount()])});
    }
  }

  deleteRange(i) {
    if (this.ranges.length > 1) {
      this.ranges.removeAt(i);
    } else {
      alert('Debe tener almenos un rango de nomenclaturas');
    }
  }

  /* Validator */
  validateNoms() {
    return(form: FormGroup) : {[key: string] : any} => {
      let nom1 = form.get('nom1').value;
      let nom2 = form.get('nom2').value;
      if(nom1 && nom2) {
        if(nom1 > nom2) {
          return {'noms': true}
        }
      }
    };
  }

  /* Validator */
  validateAptos() {
    return (form: FormGroup): {[key: string]: any} => {
      const sumAptos = this.ranges.controls.reduce((sum, current) => {
        const aptos = current.get('numApartamentos').value;
        const n1 = current.get('nom1').value;
        const n2 = current.get('nom2').value;
        const delpiso = current.get('delpiso').value;
        const alpiso = current.get('alpiso').value;
        const pisos = alpiso - delpiso + 1;
        const noms = (aptos) ? aptos : (n2 && n1) ? n2 - n1 + 1 : undefined;
        return (pisos && noms) ? sum + (pisos * noms) : sum;
      }, 0);
      this.createdAptos = sumAptos;
      if(sumAptos > this.numAptos) {
        return {aptos: true}
      }
    }
  }

  validateNomsCount() {
    return(form: FormGroup) : {[key: string] : any} => {
      let nom1 = form.get('nom1').value;
      let nom2 = form.get('nom2').value;
      let aptos = form.get('numApartamentos').value;
      let count = nom2 - nom1 + 1;
      if(nom1 && nom2 && aptos) {
        if(count != aptos) {
          return {'nomsCount': true}
        }
      }
    };
  }

  generateNom(bool?) {
    let noms = [];
    if (this.nomenclatureForm.valid) {
      this.ranges.controls.forEach(control => {
        const n1 = control.get('nom1').value;
        const n2 = control.get('nom2').value;
        const delpiso = control.get('delpiso').value;
        const alpiso = control.get('alpiso').value;
        this.generatenoms(n1, n2, delpiso, alpiso).forEach(element => noms.push(element));
      });
      this.projectservice.modifyProject({noms: noms});
      if(!bool) {alert ('Los rangos de nomenclaturas han sido guardados correctamente');}
    } else {
      alert('Todos los rangos de nomenclaturas deben estar completamente diligenciados.')
    }
  }

  generatenoms(n1, n2, delpiso, alpiso) {
    let towerName = this.projectservice.getNewProject().actualTower;
    let noms = [];
    for(let piso = delpiso; piso <= alpiso; piso++) {
      for (let i = n1; i <= n2; i++ ) {
        noms.push({tower: towerName, nom: this.addZero(piso.toString(), i.toString()), status: 'Pendiente'});
      }
    }
    return noms;
  }

  addZero(floor: string, num: string) {
    if(num.length < 2) {
      return floor + '0' + num;
    } else {
      return floor + num;
    }
  }

}
