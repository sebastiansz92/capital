import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { data } from '../../data';
import { FeatureCreationComponent } from '../feature-creation/feature-creation.component';
import { MDBModalRef, MDBModalService, TabsetComponent } from 'ng-uikit-pro-standard';
import { TypeCreationModalComponent } from '../type-creation-modal/type-creation-modal.component';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';
import { FormGroup } from '@angular/forms';
import { ConfirmationModalComponent } from 'src/app/shared/confirmation-modal/confirmation-modal.component';
import { EnterTextComponent } from 'src/app/shared/enter-text/enter-text.component';

@Component({
  selector: 'app-types',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.css']
})
export class TypesComponent implements OnInit {
  modalRef: MDBModalRef;
  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent
  types;
  activeTabIndex = 0;
  @Input() show;
  projectId;
  @Input() loadedTypes;
  @Input() typesForm : FormGroup;
  project = null;
  @Input() editing: boolean = false;
  loadingtype = false;

  constructor(private modalService: MDBModalService, private projectService: ProjectService,
    private route: ActivatedRoute, private projectsService: ProjectsService) { }

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.projectId = this.route.snapshot.params.id;
    if (this.projectId) {
      //this.types = this.projectService.getProject(this.projectId).types;
      this.project = this.projectsService.project;
      this.types = this.getLoadedTypes(this.project.projectTypes);
    } else if (this.loadedTypes) {
      this.types = this.getLoadedTypes(this.loadedTypes);
    } else {
      this.types = [...this.projectService.getNewProject().types];
    }
  }

  ngAfterContentInit() {
    if (this.projectId || this.loadedTypes) {
      if(this.typesForm != undefined) {
        this.typesForm.get('haveTypes').setValue(true);
        this.typesForm.get('typesChecked').setValue(true);
      }
    }
  }

  getLoadedTypes(projectTypes) {
    if(projectTypes == undefined) {
      return this.projectService.getNewProject().types;
    }
    let types = [...projectTypes];
    /* this.projectService.getNewProject().types.forEach(type => {
      types.push(type);
    }); */
    return types;
  }

  openModal() {
    this.modalRef = this.modalService.show(FeatureCreationComponent, {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'feature-modal',
        containerClass: '',
        animated: true
    });

    this.modalRef.content.action.subscribe( (result: any) => {
      if (this.project != null){
        this.projectService.addSpace(this.activeTabIndex - this.project.projectTypes.length, result);
        this.types = this.getLoadedTypes(this.project.projectTypes);
      } else {
        this.projectService.addSpace(this.activeTabIndex, result);
        this.types = [...this.projectService.getNewProject().types];
      }
      let typesChecked = this.types.reduce((checked, actual) => checked && actual.spaces.length > 0);
      if (typesChecked) {
        this.typesForm.get('typesChecked').setValue(true);
      }
    });
    this.modalRef.hide();
  }

  openTypeCreationModal() {
    let modal = this.modalService.show(TypeCreationModalComponent, {
      backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'feature-modal',
        containerClass: '',
        animated: true
    });
    modal.content.action.subscribe(type  => {
      if(type != null){
        (this.projectId != undefined) ? this.projectService.addType(type, this.projectId) : this.projectService.addType(type);
        if (this.project != null){
          this.types = this.getLoadedTypes(this.project.projectTypes);
        } else {
          this.types = [...this.projectService.getNewProject().types];
        }
      }
      if (this.types.length > 0) {
        this.typesForm.get('haveTypes').setValue(true);
        this.typesForm.get('typesChecked').setValue(false);
      }
    });
    modal.hide();
  }

  onGetActiveTab(event) {
    if (event) {
      this.activeTabIndex = event.activeTabIndex;
    }
  }

  deleteFeature(type, space, feature){
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Está seguro que desea eliminar la caracteristica ` + (feature.name || feature)
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        if (feature.id) {
          this.projectsService.deleteFeature(feature.id).subscribe(res => {
            space.features = space.features.filter(feat => feat.id !== feature.id)
          }, error => {
            alert (error.error.data);
          })
        } else {
          space.features = space.features.filter(feat => feat !== feature);
        }
      }
    })
  }

  deleteSpace(type, space) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Está seguro que desea eliminar el espacio - ` + (space.name)
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        if (space.id) {
          this.projectsService.deleteSpace(space.id).subscribe(res => {
            type.spaces = type.spaces.filter(spa => spa.id !== space.id)
          }, error => {
            alert (error.error.data);
          })
        } else {
          type.spaces = type.spaces.filter(spa => spa !== space);
        }
      }
    })
  }

  deleteType(type) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Está seguro que desea eliminar el tipo - ` + (type.name)
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        if (type.id) {
          this.projectsService.deleteType(type.id).subscribe(res => {
            this.types = this.types.filter(typ => typ.id !== type.id)
          }, error => {
            alert (error.error.data);
          })
        } else {
          this.projectService.deleteType(type);
          this.types = this.types.filter(typ => typ !== type);
          console.log(this.types);
        }
      }
    });
  }

  changeFeatureName(type, space, feature){
    this.modalRef = this.modalService.show(EnterTextComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Cuál es el nuevo nombre de la característica?`
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        if (feature.id) {
          this.projectsService.updateFeature(feature.id, result.answer).subscribe(res => {
            if(feature.name) {
              feature.name = result.answer;
            } else {
              feature = result.answer
            }
          }, error => {
            alert (error.error.data);
          })
        } else {
          if(feature.name) {
            feature.name = result.answer;
          } else {
            space.features = space.features.filter(elem => elem !== feature);
            space.features.push(result.answer);
            //feature = result.answer
          }
        }
      }
    })
  }

  changeSpaceName(space) {
    this.modalRef = this.modalService.show(EnterTextComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Cuál es el nuevo nombre del espacio?`
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        if (space.id) {
          this.projectsService.updateSpace(space.id, result.answer).subscribe(res =>{
            space.name = result.answer;
          }, error => {
            alert (error.error.data);
          })
        } else {
          space.name = result.answer;
        }
      }
    })
  }

  addFeatures(space){
    this.modalRef = this.modalService.show(FeatureCreationComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        space: space.name,
        spaceId: space.id
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        let feats = result.features;
        let spaceFeatures = space.features.map(spaceFeature => spaceFeature.name);
        feats = feats.filter(feat => !spaceFeatures.includes(feat));
        if(space.id) {
          feats.forEach(feat => {
            this.projectsService.createFeature(space.id, feat).subscribe((res) => {
              space.features.push({name: feat, id: this.getId(res.body['data'])});
            }, error => {
              alert (error.error.data);
            })
        });
        } else {
          feats.forEach(feat => {
            space.features.push(feat);
        });
        }
      }
    })
  }

  createSpace(type) {
    this.modalRef = this.modalService.show(FeatureCreationComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true
  });

  this.modalRef.content.action.subscribe( (result: any) => {
    let space = result;
    result.features = result.features.map(feat => ({name: feat}));
    if(type.id) {
      this.projectsService.createSpace(type.id, space).subscribe(res => {
        space.id = this.getId(res.body['data']);
        this.projectsService.getProjectById(this.projectId).subscribe(res => {
          let newType = res.body['data'].projectTypes.find(typ => typ.id === type.id);
          type.spaces = newType.spaces;
          let tabindex = this.activeTabIndex;
        })
      }, error => {
        alert(error.error.data);
      });
    } else {
      type.spaces.push(space);
    }
  });
  this.modalRef.hide();
  }

  changeTypeName(type){
    this.modalRef = this.modalService.show(EnterTextComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Cuál es el nombre del nuevo tipo?`
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        if(type.id) {
          this.projectsService.updateType(type.id, result.answer)
        .subscribe(res => {
          type.name = result.answer;
        }, error => {
          alert (error.error.data);
        });
        } else {
          type.name = result.answer;
        }
      }
    })
  }

  createType() {
    this.modalRef = this.modalService.show(EnterTextComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Cuál es el nombre del nuevo tipo?`
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        let type = {...result, name: result.answer};
        if(this.project?.id) {
          this.projectsService.createType(this.projectId, {name: result.answer, spaces: []})
        .subscribe(res => {
          type.id = this.getId(res.body['data']);
          type.spaces = [];
          this.types.push(type);
        }, error => {
          alert (error.error.data);
        });
        } else {
          type.spaces = [];
          this.types = [...this.types, type];
        }
      }
    })
  }

  getId(message: string) {
    let index = message.lastIndexOf('/');
    return message.slice(index + 1);
  }

  duplicateType(type) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Está seguro que desea duplicar este tipo?`
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        if (type.id) {
          this.projectsService.duplicateType(type.id).subscribe(res => {
            this.projectsService.getProjectById(this.projectId).subscribe(res2 => {
              this.types = res2.body['data'].projectTypes;
              setTimeout(() => {
                this.staticTabs.setActiveTab(1);
              }, 400);
            })
          })
        } else {
          console.log('tipo inexistente aún');
        }
      }
    })
  }


}
