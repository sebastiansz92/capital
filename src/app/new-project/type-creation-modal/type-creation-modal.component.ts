import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/shared/services/project.service';

@Component({
  selector: 'app-type-creation-modal',
  templateUrl: './type-creation-modal.component.html',
  styleUrls: ['./type-creation-modal.component.css']
})
export class TypeCreationModalComponent implements OnInit {

  action: Subject<any> = new Subject();
  type: '';
  typeForm: FormGroup;

  constructor(public modalRef: MDBModalRef, private projectService: ProjectService
    , private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createTypeForm();
  }

  createTypeForm() {
    this.typeForm = this.formBuilder.group({
      name: ['', [Validators.required]]
    });
  }

  createType() {
    if (this.typeForm.valid) {
      this.action.next(this.typeForm.get('name').value);
      this.modalRef.hide();
    } else {
      alert('Debe llenar todos los campos');
    }
  }

  closeModal() {
    this.modalRef.hide();
  }

}
