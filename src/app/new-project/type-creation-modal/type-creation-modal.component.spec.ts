import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TypeCreationModalComponent } from './type-creation-modal.component';

describe('TypeCreationModalComponent', () => {
  let component: TypeCreationModalComponent;
  let fixture: ComponentFixture<TypeCreationModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeCreationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeCreationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
