import { Component, OnInit, SimpleChanges, Input, ViewChild } from '@angular/core';
import { ProjectService } from '../../shared/services/project.service';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CheckboxComponent } from 'ng-uikit-pro-standard';
import {ProjectsService} from '../../projects/project.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  @ViewChild(CheckboxComponent) checkbox: CheckboxComponent;

  tableHeaders = ['Nombre Torre', 'Tipo Apto', 'Nomenclatura'];
  tableContent = [];
  options = [];
  @Input() noms;
  @Input() editing;
  @Input() projectIndex;
  @Input() typesForm: FormGroup;
  cacheId;
  @Input() set setCacheid(val: any) {
    this.cacheId = val;
  }
  globalType;
  toggleAll;
  cache;
  

  constructor(private projectService: ProjectService, private formBuilder: FormBuilder, private router: Router,
              private projectsService: ProjectsService, private route: ActivatedRoute ) { }

  ngOnInit() {
    if(this.route.snapshot.routeConfig.path.includes('cache')) this.cache = true;
    console.log(this.cache)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.tableContent = this.projectService.getNewProject().noms;
    this.options = this.projectService.getNewProject().types.map(type => type.name);
    if(this.route.snapshot.params.id != undefined) {
      let proj = this.projectsService.project;
      proj.projectTypes.forEach(typ => {
        this.options.push(typ.name);
      });
    }
  }

  createTypesForm() {
    this.typesForm = this.formBuilder.group({
      types: this.formBuilder.array([])
    });
    this.noms.forEach(x => {
      this.types.push(this.createTypeForm());
    });
  }

  get types() : FormArray {
    if(this.typesForm != undefined) {
      return this.typesForm.get('types') as FormArray;
    }
  }

  createTypeForm() {
    return this.formBuilder.group({
      type: ['', [Validators.required]],
      checked: [false, []]
    });
  }

  refreshData() {
    this.options = this.projectService.getNewProject().types.map(type => type.name);
    if(this.route.snapshot.params.id != undefined) {
      let proj = this.projectsService.project;
      proj.projectTypes.forEach(typ => {
        this.options.push(typ.name);
      });
    }
    this.tableContent = this.projectService.getNewProject().noms;
  }

  createProject() {
    let objectsWithValue = this.typesForm.get('types')['controls'].every(control => control.get('type').value !== '');
    if (this.typesForm.valid && objectsWithValue) {
      this.typesForm.get('types')['controls'].forEach((control, index) => {
          this.projectService.changeType(index, this.options[control.get('type').value]);
      });
      if (this.editing) {
        const project = this.projectService.getNewProject();
        const towersArray = project.towers;
        towersArray.push(project.actualTower);
        const newProject = {...project, towersCount: 1, aptos: project.numAptos, towers: towersArray, audits: [], ownerAudits: [], contracts:[]};
        const realtowers = newProject.towers.map( x => ({
          name: x,
          nomenclatures: newProject.noms.filter(nom => nom.tower === x).map(apto => ({number: Number(apto.nom), state: 'PENDIENTE', projectType: apto.type})),
          nomenclaturesCount: newProject.aptos.toString()
        }));
        console.log(realtowers);

        const realtypes = newProject.types.map(x => {
          return {...x, spaces: x.spaces.map(space => {
            return {...space, features: space.features.map(feature => ({name: feature}))};
          })};
        });
        const realProject = {
          id: Number(this.route.snapshot.params.id),
          name: this.projectService.getNewProject().name,
          towers: realtowers,
          projectTypes: realtypes,
          city: this.projectsService.project.city
          // tslint:disable-next-line: max-line-length
        };
        this.projectsService.addTower(realProject).subscribe(res => {
          this.router.navigate(['/projects']);
        }, error => {
          alert(error.error.data || error.error.message);;
          this.router.navigate(['/projects']);
        });


        /* let project = this.projectService.getProject(this.projectIndex);
        const towersArray = project.towers;
        const actualTower = this.projectService.getNewProject().actualTower;
        towersArray.push(actualTower);
        const towersCount = project.towersCount + 1;
        const aptos = this.projectService.getNewProject().numAptos + project.aptos;
        const noms = project.noms.concat(this.projectService.getNewProject().noms);
        this.projectService.editProject(this.projectIndex, {...project, towersCount, aptos, noms}); */
      } else {
        this.projectService.modifyProject({
          img_url: 'https://www.portafolio.co/files/article_multimedia/uploads/2018/09/02/5b8c8f78a67e9.jpeg'
        });
        const project = this.projectService.getNewProject();
        const towersArray = project.towers;
        console.log(project);
        towersArray.push(project.actualTower);
        const newProject = {...project, towersCount: 1, aptos: project.numAptos || project.noms?.length, towers: towersArray, audits: [], ownerAudits: [], contracts:[]};
        console.log(newProject);
        let realtowers = newProject.towers.map( x => ({
          name: x,
          nomenclatures: newProject.noms.filter(nom => nom.tower === x).map(apto => ({number: Number(apto.nom), state: 'PENDIENTE', projectType: apto.type})),
          nomenclaturesCount: newProject.aptos.toString()
        }));
        if(realtowers.length > 1) {
          realtowers.shift();
        }

        const realtypes = newProject.types.map(x => {
          return {...x, spaces: x.spaces.map(space => {
            return {...space, features: space.features.map(feature => ({name: feature}))};
          })};
        });
        const realProject = {
          name: newProject.name,
          towers: realtowers,
          city: newProject.city,
          projectTypes: realtypes,
          // tslint:disable-next-line: max-line-length
          imageBase64: newProject.imageBase64
        };
        this.projectsService.createProject(realProject).subscribe(res => {
          if(this.cacheId) {
            this.projectsService.deleteAdvances(this.cacheId).subscribe(res => {
              console.log(res);
            });
          }
          this.router.navigate(['/projects']);
          this.projectService.addProject(newProject);
        }, error => {
          alert(error.error.data || error.error.message);
          this.router.navigate(['/projects'])
        });
        this.projectService.addProject(newProject);
      }
    } else {
      alert('Debe seleccionar un tipo a cada apartamento');
    }
    }

    changeType(i, event) {
      this.projectService.changeType(i, this.options[event.target.value]);
    }

    applyType() {
      for( let control of this.typesForm.get('types')['controls']) {
        if(control.get('checked').value) {
          control.get('type').setValue(this.globalType);
        }
      }
    }

    toggleAllChecks() {
      let allToggled = true;
      
      for ( let control of this.typesForm.get('types')['controls']) {
        allToggled = allToggled && control.get('checked').value;
      }
      if(!allToggled) {
        this.typesForm.get('types')['controls'].forEach((control, index) => {
          control.get('checked').setValue(true);
        });
        this.typesForm.get('checked').setValue(!allToggled);
      } else {
        this.typesForm.get('types')['controls'].forEach((control, index) => {
          control.get('checked').setValue(false);
        });
        this.typesForm.get('checked').setValue(!allToggled);
      }
    }

    checkToggles() {
      let allToggled = true;
      for ( let control of this.typesForm.get('types')['controls']) {
        allToggled = allToggled && control.get('checked').value;
      }
      this.typesForm.get('checked').setValue(allToggled);
      //this.checkbox.checked = allToggled;
    }

}
