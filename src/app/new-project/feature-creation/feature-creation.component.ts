import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-feature-creation',
  templateUrl: './feature-creation.component.html',
  styleUrls: ['./feature-creation.component.css']
})
export class FeatureCreationComponent implements OnInit {
  spaceForm: FormGroup;
  action: Subject<any> = new Subject();
  spaceId: any;
  space: string;
  typeId: any;
  
constructor(public modalRef: MDBModalRef, public fb: FormBuilder) { 
  this.spaceForm = fb.group({
    spaceName: ['', [Validators.required]],
    feature: ['', [Validators.minLength(3), Validators.maxLength(100)]],
  });
}

get spaceName() { return this.spaceForm.get('spaceName'); }
get feature() { return this.spaceForm.get('feature'); }

features: any[] = [];

ngOnInit() {
  if(this.space) {
    this.spaceForm.get('spaceName').setValue(this.space);
    this.spaceForm.get('spaceName').disable();
  }
}

onYesClick() {
  const space = {
    name: this.spaceName.value,
    features: this.features
  };
  this.action.next(space);
  this.modalRef.hide();
}

onNoClick() {
    this.modalRef.hide();
}

handleChip() {
  const index = this.features.indexOf(this.feature.value);
  if (index < 0 && this.feature.value.length >= 3 && this.spaceForm.get('feature').valid) {
    this.features.push(this.feature.value);
  }
  this.feature.setValue('');
}

deleteChip(feature) {
  const index = this.features.indexOf(feature);
  this.features.splice(index, 1);
}

}
