import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FeatureCreationComponent } from './feature-creation.component';

describe('FeatureCreationComponent', () => {
  let component: FeatureCreationComponent;
  let fixture: ComponentFixture<FeatureCreationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
