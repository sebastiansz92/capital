import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../shared/services/project.service';
import { FormBuilder, FormGroup, Validators, FormArray, AbstractControl } from '@angular/forms';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { ProjectsService } from '../projects/project.service';
import { ConfirmationModalComponent } from '../shared/confirmation-modal/confirmation-modal.component';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { DeactivateGuard } from '../shared/services/http/deactiveGuard';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {

  constructor(private route: ActivatedRoute, private projectService: ProjectService,
    private formBuilder: FormBuilder, private projectsService:ProjectsService,
    private modalService: MDBModalService, private router: Router,
    private deactiveGuard: DeactivateGuard) { }


  modalRef: MDBModalRef;
  projectForm: FormGroup;
  nomenclatureForm: FormGroup;
  noms = [];
  numAptos = 0;
  project;
  editing = false;
  projectId;
  typesForm: FormGroup;
  summaryForm: FormGroup;
  loaded = false;
  secondStepForm: FormGroup;
  cacheId;
  mapNoms;
  private routeSub: any;

  ngOnInit() {
    this.deactiveGuard.canDeact = false;
    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.openChangeRouteModal(event);
      }
    });
    this.projectService.setProject({});
    this.projectsService.setCurrentProject({});
    if(sessionStorage) {
      this.cacheId = sessionStorage.getItem('capital_cacheid');
    }
    let cache = this.route.snapshot.params.cacheid;
    if(cache) {
      this.cacheId = cache;
    }
    this.projectId = this.route.snapshot.params.id;
    
    if( this.projectId != undefined && (this.cacheId == null || this.cacheId == undefined)) {
      this.project = this.projectsService.project;
      if(this.project == null) {
        this.projectsService.getProjectById(this.projectId).subscribe(res => {
          this.project = res.body['data'];
          this.projectsService.setCurrentProject(this.project);
          this.createProjectForm(this.project);
          this.createNomenclatureForm();
          this.createTypesForm();
          this.createSecondStepForm();
          this.projectService.newProject.types = [];
          this.loaded = true;
        });
      } else {
        this.createProjectForm(this.project);
        this.createNomenclatureForm();
        this.createTypesForm();
        this.createSecondStepForm();
        this.projectService.newProject.types = [];
        this.loaded = true;
      }
    } else if(this.cacheId > 0) {
        this.projectsService.getProjectCache(this.cacheId).subscribe(res => {
        this.project = res.body['data'];
        this.project.types = this.mapProjectTypesToTypes(this.project.projectTypes);
        this.project.actualTower = this.project.towers[this.project.towers.length -1].name;
        this.project.noms = this.mapTowersToNoms(this.project.towers);
        this.projectService.setProject(this.project);
        this.projectsService.setCurrentProject(this.project);
        this.createProjectForm(this.project);
        this.createNomenclatureForm();
        this.createTypesForm();
        this.createSecondStepForm();
        this.mapNoms = this.mapNomenclatures(this.project.towers[this.project.towers.length - 1].nomenclatures);
        this.loaded = true;
      });
    } else {
        this.createProjectForm(this.project);
        this.createNomenclatureForm();
        this.createTypesForm();
        this.createSecondStepForm();
        this.projectService.newProject.types = [];
        this.projectService.newProject.towers = [];
        this.loaded = true;
    }
    
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  mapProjectTypesToTypes(projectTypes) {
    if(projectTypes) {
      let types = projectTypes.map(projectType => {
        return {...projectType, spaces: projectType.spaces.map(space => {
          return {...space, features: space.features.map(feature => (feature.name))}
        })}
      });
      return types;
    } else {
      return []
    }
  }

  mapTowersToNoms(towers) {
    let towerName = towers[towers.length -1].name;
    if(towers[towers.length -1].nomenclatures) {
      this.project.numAptos = towers[towers.length -1].nomenclaturesCount;
      return towers[towers.length -1].nomenclatures.map(nom => ({tower: towerName, nom: nom.number, status: nom.state || 'PENDIENTE', type: nom.projectTypeName || 'unos'}))
    }
  }

  onStepChange(event) {
    this.noms = this.projectService.getNewProject().noms || [];
    this.numAptos = this.projectService.getNewProject().numAptos;
    this.createTypesForm();
  }

  getDisability() {
    let types = this.projectService.newProject.types;
    if( types.length > 0) {
      return false
    } else {
      return true
    }
  }

  createProjectForm(project) {
    if (project) {
      this.editing = (!this.cacheId) ;
      this.projectForm = this.formBuilder.group({
        name: [{value: project.name, disabled: false}, [Validators.required]],
        towerName: [project.towers ? project.towers[0].name : '', [Validators.required,Validators.minLength(3) ]],
        aptos: [project.towers? project.towers[0].nomenclaturesCount : '' , [Validators.required]],
        image: [project.image_url, []],
        city: [project.city || '', [Validators.required]]
      });
    } else {
      this.projectForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        towerName: ['', [Validators.required, Validators.minLength(3)]],
        aptos: ['', [Validators.required]],
        image: ['', [Validators.required]],
        city: ['', [Validators.required]]
      });
    }
  }

  mapNomenclatures(nomenclatures) {
    if(nomenclatures) {
      let noms = nomenclatures.map(nom => nom.number);
      let pisos = this.getPisos(noms);
      let aptosPorPiso = this.findAptosPorPiso(pisos, noms);
      let aptosIguales = this.findPisosIguales(aptosPorPiso);
      return aptosIguales;
    } else {
      return []
    }
  }

  findPisosIguales(aptosPorPiso) {
    let bigArray = aptosPorPiso.map(aps => aps.apartamentos);
    let stringArray = bigArray.map(JSON.stringify);
    let uniqueStringArray = new Set(stringArray);
    let uniqueArray = Array.from(uniqueStringArray).map((x) => JSON.parse(x.toString()));
    let lastSets = [];
    let apartmentsSets = uniqueArray;
    return apartmentsSets.map(set => {
      let pisosiguales = aptosPorPiso.filter(piso => this.arraysequal(set ,piso.apartamentos)).map(x => x.piso);
      return {minPiso: Math.min.apply(Math, pisosiguales), maxPiso: Math.max.apply(Math, pisosiguales), min: Math.min.apply(Math, set), max: Math.max.apply(Math, set), count: set.length}
    })
  }
  
  findAptosPorPiso(pisos, noms) {
    return pisos.map(x => {
      return {piso: x, apartamentos: noms.filter(nom => (Math.floor(nom/100) === x)).map(val => val % (x*100)).sort((a,b) => {
        if(a < b){
          return -1
        } else if(a > b){
          return 1
        } else {
          return 0
        }
      })}
    }).sort((c, d) => {
      if(c.piso < d.piso){
        return -1
      } else if(c.piso > d.piso){
        return 1
      } else {
        return 0
      }
    })
  }
  getPisos(numbers) {
    return numbers.map(num => Math.floor(num/100)).filter((piso, index) => {
      return numbers.map(num => Math.floor(num/100)).indexOf(piso) === index; 
    });
  }

  arraysequal (arr1, arr2) {

    // Check if the arrays are the same length
    if (arr1.length !== arr2.length) return false;
  
    // Check if all items exist and are in the same order
    for (var i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) return false;
    }
  
    return true;
  
  };

  createNomenclatureForm () {
    this.nomenclatureForm = this.formBuilder.group( {
      ranges: this.formBuilder.array([]),
      formValid: [false, []]
    }, {validator: this.validateAptos()});
  }

  preloadNomenclatureForm(obj) {
    //obj.towers?.nomenclatures.map()
  }

  get ranges() : FormArray {
    if(this.nomenclatureForm != undefined) {
      return this.nomenclatureForm.get('ranges') as FormArray;
    }
  }

  validateAptos() {
    return (form: FormGroup): {[key: string]: any} => {
      let sum = 0;
      if(this.ranges) {
        for( let control of this.ranges.controls) {
          sum = sum + control.get('numApartamentos').value;
        }
      }
      if(sum > this.numAptos) {
        return {aptos: true}
      }
    }
  }

  validateTowerName() {
    return (control: AbstractControl): {[key: string]: any} => {
      const name = control.value;
      if(this.project.towers.indexOf(name) >= 0) {
        return {towerName: true};
      }
    };
  }

  createTypesForm() {
    this.typesForm = this.formBuilder.group({
      types: this.formBuilder.array([]),
      checked: [false, []]
    });
    this.noms.forEach(x => {
      this.types.push(this.createTypeForm());
    });
  }

  get types() : FormArray {
    if(this.typesForm != undefined) {
      return this.typesForm.get('types') as FormArray;
    }
  }

  saveStep(index) {
    let newProject = this.projectService.getNewProject();
    const realtowers = [
      {
        name: newProject.actualTower,
        nomenclaturesCount: this.projectForm.get('aptos').value,
        nomenclatures: newProject.noms? newProject.noms.map(nom => ({number: nom.nom, state: nom.status.toUpperCase(), projectType: 'unos'})) : []
      }
    ];
    /* newProject.towers.map( x => ({
      name: x,
      nomenclatures: newProject.noms.filter(nom => nom.tower === x).map(apto => ({number: Number(apto.nom), state: 'PENDIENTE', projectType: apto.type})),
      nomenclaturesCount: newProject.numAptos.toString()
    })); */
    if(this.cacheId === null || this.cacheId === undefined) {
      this.projectsService.saveProjectCache({...this.projectService.getNewProject(),
        progress: (index + 1) * 25,
        projectTypes: newProject.types || [],
        towers: realtowers}).subscribe(res => {
        sessionStorage.setItem('capital_cacheid', res.body['data'].cacheId);
        this.cacheId = res.body['data'].cacheId;
      });
    } else {
      this.projectsService.updateProjectCache(this.cacheId, {...this.projectService.getNewProject(),
        progress: (index + 1) * 25,
        projectTypes: this.projectService.getNewProject().types,
        towers: realtowers})
      .subscribe(res => {
        console.log(res);
      });
    }
  }

  createTypeForm() {
    return this.formBuilder.group({
      type: ['', [Validators.required]],
      checked: [false, []]
    });
  }

  createSecondStepForm() {
    this.secondStepForm = this.formBuilder.group({
      haveTypes: [false, [Validators.requiredTrue]],
      typesChecked: [false, [Validators.requiredTrue]]
    });
  }

  openChangeRouteModal(ev: any) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: '¿Está seguro que desea salir? Asegurese de haber guardado los cambios'
      }
  });
    this.modalRef.content.action.subscribe( (result: any) => {
      if (result) {
        this.deactiveGuard.canDeact = true;
        this.routeSub.unsubscribe();
        this.router.navigateByUrl(ev.url)
      } else {
        console.log('oe nothing happens', ev);
      }
    });
    this.modalRef.hide();
  }

}
