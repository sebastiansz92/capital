import { Component, OnInit, Input } from '@angular/core';
import { ProjectService } from '../../shared/services/project.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-project-first-step',
  templateUrl: './project-first-step.component.html',
  styleUrls: ['./project-first-step.component.css']
})
export class ProjectFirstStepComponent implements OnInit {

  constructor(private projectService: ProjectService, private formBuilder: FormBuilder) { }

  @Input() projectForm: FormGroup;
  imageLoaded = false;
  imageSource;
  @Input() imageUrl;
  multimedia = 'http://68.183.97.247:8091/';
  @Input() imageB64;

  ngOnInit() {
    if(this.imageUrl != null && this.imageUrl != undefined) {
      this.imageSource = this.getProjectImage(this.imageUrl);
      this.imageLoaded = true;
      this.projectForm.get('image').setValue(this.imageSource);
    }
    if(this.imageB64 != null && this.imageB64 != undefined) {
      this.imageSource = this.imageB64;
      this.imageLoaded = true;
      this.projectForm.get('image').setValue('image');
    }
  }

  createProjectForm() {
    this.projectForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      towerName: ['', [Validators.required]],
      aptos: ['', [Validators.required]],
    });
  }

  changeName(event) {
    const data = {
      name: event.target.value
    };
    this.projectService.modifyProject(data);
  }

  changeTowers(event) {
    const data = {
      actualTower: event.target.value
    };
    this.projectService.modifyProject(data);
  }

  changeAptos(event) {
    const data = {
      numAptos: parseInt(event.target.value, 10)
    };
    this.projectService.modifyProject(data);
  }

  changeCity(event) {
    const data = {
      city: event.target.value
    };
    this.projectService.modifyProject(data);
  }

  getImageSource() {
    return this.imageSource;
  }

  imageIsLoaded() {
    return this.imageLoaded;
  }

  loadImage(event) {
    const file = event.target.files[0];
    this.getBase64(file);
  }


  getBase64 = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    /* reader.onload = function (x) {
        console.log(x);
        return reader.result;
        //cb(reader.result)
    }; */
    reader.onloadend = () => {
      this.imageSource = reader.result;
      this.imageLoaded = true;
      this.projectService.getNewProject()['imageBase64'] = this.imageSource;
      this.projectForm.get('image').setValue(this.imageSource);
    }
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
    return reader.result;
}

clearImage() {
    this.imageLoaded = false;
    this.imageSource = null;
    this.projectForm.get('image').setValue('');
}

getProjectImage(image_url){
  let url = image_url.substr(10);
  return this.multimedia + image_url;
}

}
