import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProjectFirstStepComponent } from './project-first-step.component';

describe('ProjectFirstStepComponent', () => {
  let component: ProjectFirstStepComponent;
  let fixture: ComponentFixture<ProjectFirstStepComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectFirstStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectFirstStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
