import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminGuard } from "../admin/admin.guard";
import { DeactivateGuard } from "../shared/services/http/deactiveGuard";
import { OfflineProjectsComponent } from "./offline-projects/offline-projects.component";
import { SyncDataComponent } from "./sync-data/sync-data.component";

const routes: Routes = [
  {
    path: "offline-projects",
    component: OfflineProjectsComponent,
    canActivate: [AdminGuard],
    data: { expectedRoles: ["ADMIN", "RESIDENTE_ENTREGA", "RESIDENTE_OBRA"] },
    canDeactivate: [DeactivateGuard],
  },
  {
    path: "sync-data",
    component: SyncDataComponent,
    canActivate: [AdminGuard],
    data: { expectedRoles: ["ADMIN", "RESIDENTE_ENTREGA", "RESIDENTE_OBRA"] },
    canDeactivate: [DeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfflineRoutingModule {}
