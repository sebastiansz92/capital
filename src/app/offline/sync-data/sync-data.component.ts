import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ProjectsService } from "src/app/projects/project.service";

@Component({
  selector: "app-sync-data",
  templateUrl: "./sync-data.component.html",
  styleUrls: ["./sync-data.component.css"],
})
export class SyncDataComponent implements OnInit {
  modoOffline = false;
  showLoaderSync = false;
  showNoSyncProject = false;
  projects = [];
  ownerAudits = [];
  listProjects = [];
  getLastDateUpdate;
  projectName;
  elements: any = [
    { id: 1, first: "Mark", last: "Otto", handle: "@mdo" },
    { id: 2, first: "Jacob", last: "Thornton", handle: "@fat" },
    { id: 3, first: "Larry", last: "the Bird", handle: "@twitter" },
  ];

  headElements = ["ID", "First", "Last", "Handle"];

  constructor(
    private projectsService: ProjectsService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.loadProjectsToSync();
    this.loadProjectsSelected();
  }

  loadProjectsToSync() {
    if (JSON.parse(localStorage.getItem("project"))) {
      this.showNoSyncProject = false;
      this.projectName = JSON.parse(localStorage.getItem("project")).name;
      this.getLastDateUpdate = JSON.parse(
        localStorage.getItem("fechaActualizacion")
      ).split("T")[0];
      this.projects = JSON.parse(localStorage.getItem("audits"));
      this.ownerAudits = JSON.parse(localStorage.getItem("ownerAudits"));
    } else {
      this.showNoSyncProject = true;
    }
    
  }

  loadProjectsSelected() {
    this.projectsService.getProjects().subscribe((res) => {
      let projs = res.body["data"];
      this.listProjects = projs.map((proj) => ({
        label: proj.name,
        value: proj.projectId,
      }));
    });
  }

  loadProjectLocal(event) {
    this.showLoaderSync = true;
    this.showNoSyncProject = false;
    let date = new Date();
    localStorage.setItem("fechaActualizacion", JSON.stringify(date));
    this.projectsService
      .getProjectByIdWithoutPaging(event.value)
      .subscribe((res) => {
        localStorage.setItem("project", JSON.stringify(res.body["data"]));
        this.showLoaderSync = false;
        this.getLastDateUpdate = JSON.parse(
          localStorage.getItem("fechaActualizacion")
        ).split("T")[0];
        this.projectName = JSON.parse(localStorage.getItem("project")).name;
      });
  }

  syncData() {
    this.spinner.show();
    this.projects.forEach((request, index) => {
      if (request.auditsByReforms !== undefined) {
        let withReforms = {
          generalObservation: request.generalObservation,
          auditsByReforms: request.auditsByFeatures,
          nomenclatureState: request.nomenclatureState,
          auditor: "CONSTRUCTORA",
        };
        this.projectsService
          .addAudit(request.id, {
            ...withReforms,
            auditsByReforms: request.auditsByReforms,
          })
          .subscribe(
            (res) => {
              this.spinner.hide();
              console.log("Actualizo bien");
            },
            (error) => {
              alert(error.error.data || error.error.message);
            }
          );
      } else {
        let withOutReforms = {
          generalObservation: request.generalObservation,
          auditsByFeatures: request.auditsByFeatures,
          nomenclatureState: request.nomenclatureState,
          auditor: "CONSTRUCTORA",
        };
        this.projectsService.addAudit(request.id, withOutReforms).subscribe(
          (res) => {
            this.spinner.hide();
            console.log("Actualizo bien");
          },
          (error) => {
            alert(error.error.data || error.error.message);
          }
        );
      }
    });
    localStorage.removeItem("audits");
    this.router.navigate(["/offline-projects"]);
  }

  syncDataEntrega() {
    this.spinner.show();
    this.ownerAudits.forEach((request) => {
      let data = {
        auditsByFeatures: request.auditsByFeatures,
        auditsByReforms: request.auditsByReforms,
        contract: request.contract,
      };
      this.projectsService
        .addOwnerAudit(request.nomenclatureId, data)
        .subscribe(
          (res) => {
            this.spinner.hide();
            console.log(res);
          },
          (error) => {
            if (error.status == 400) {
              alert(error.error.message);
            } else {
              alert(error.error.data);
            }
          }
        );
    });
    localStorage.removeItem("ownerAudits");
    this.router.navigate(["/offline-projects"]);
  }
}
