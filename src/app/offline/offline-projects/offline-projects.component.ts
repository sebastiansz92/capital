import { Component, Input, OnInit } from "@angular/core";
import { MDBModalRef, MDBModalService } from "ng-uikit-pro-standard";
import { ProjectsService } from "src/app/projects/project.service";
import { ConfirmationModalComponent } from "src/app/shared/confirmation-modal/confirmation-modal.component";
import { ProjectService } from "src/app/shared/services/project.service";
import { ConnectionService } from "ng-connection-service";
import { Router } from "@angular/router";
@Component({
  selector: "app-offline-projects",
  templateUrl: "./offline-projects.component.html",
  styleUrls: ["./offline-projects.component.css"],
})
export class OfflineProjectsComponent implements OnInit {
  modalRef: MDBModalRef;
  projectdata;
  projectdataCache;
  loaded = false;
  cacheloaded = false;
  isConnected = true;
  modoOffline = false;
  multimedia = "http://68.183.97.247:8091/";
  @Input() cache = false;
  loadingMessage = "Cargando";

  constructor(
    private projectService: ProjectService,
    private modalService: MDBModalService,
    private projectsSservice: ProjectsService,
    private connectionService: ConnectionService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.loadProjects();
    this.validateStatusOnline();
  }

  validateStatusOnline() {
    this.connectionService.monitor().subscribe((isConnected) => {
      if (isConnected) {
        this.modoOffline = false;
      } else {
        this.modoOffline = true;
      }
    });
    let status = localStorage.getItem("status");
    if (status === "online") {
      this.modoOffline = false;
    } else {
      this.modoOffline = true;
    }
  }

  loadProjects() {
    this.projectdata = JSON.parse(localStorage.getItem("project"));
    if (this.projectdata) {
      this.loaded = true;
    } else {
      this.router.navigate(['sync-data']);
    }
  }

  resetProject() {
    this.projectService.setInitialState();
    localStorage.removeItem("capital_cacheid");
  }

  openConfirmationModal(projectName, projectId) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: "feature-modal",
      containerClass: "",
      animated: true,
      data: {
        question:
          "¿Está seguro que desea eliminar este proyecto (" +
          projectName +
          ")?",
      },
    });
    this.modalRef.content.action.subscribe((result: any) => {
      if (result) {
        this.projectsSservice.deleteteProject(projectId).subscribe(
          (res) => {
            this.projectdata = this.projectdata.filter(
              (proj) => proj.projectId != projectId
            );
            console.log(
              `El proyecto ${projectName} ha sido eliminado correctamente`
            );
          },
          (error) => {
            alert(error.error.data || error.error.message);
          }
        );
      }
    });
    this.modalRef.hide();
  }

  getProjectImage(image_url) {
    let url = "";
    if (image_url) {
      image_url.substr(10);
    }
    return this.multimedia + image_url;
  }
}
