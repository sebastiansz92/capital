import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { OfflineRoutingModule } from "./offline-routing.module";
import { OfflineProjectsComponent } from "./offline-projects/offline-projects.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "src/environments/environment";
import { SharedModule } from "../shared/shared.module";
import { MDBBootstrapModulesPro, WavesModule } from "ng-uikit-pro-standard";
import { SyncDataComponent } from './sync-data/sync-data.component';
import { NgxSpinnerModule } from "ngx-spinner";
@NgModule({
  declarations: [OfflineProjectsComponent, SyncDataComponent],
  imports: [
    MDBBootstrapModulesPro.forRoot(),
    CommonModule,
    OfflineRoutingModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production,
    }),
    SharedModule,
    WavesModule,
    NgxSpinnerModule
  ],
})
export class OfflineModule {}
