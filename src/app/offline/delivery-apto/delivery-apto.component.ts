import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';

@Component({
  selector: 'app-delivery-apto',
  templateUrl: './delivery-apto.component.html',
  styleUrls: ['./delivery-apto.component.css']
})
export class DeliveryAptoComponent implements OnInit {

  projects;
  idProject;

  constructor(private projectsService: ProjectsService, private router: Router) { }

  ngOnInit(): void {
    this.loadProjectsApproved();
  }

  loadProjectsApproved() {
    this.projects = JSON.parse(localStorage.getItem("project"));  
    this.idProject = this.projects.id;
  }

  goToDelivery(tower, apto, nomenclature) {
    this.projectsService.setNomenclature(nomenclature);
    this.projectsService.setCurrentProject(this.projects);
    this.router.navigate(['delivery', 'owner', 'project', this.idProject, 'tower', tower, 'apto', apto]);
  }

  checkAprovedAptos() {
    return this.projects.towers.filter(tower => this.checkAprobedAptosByTower(tower)).length > 0;
  }

  checkAprobedAptosByTower(tower){
    return tower.nomenclaturesPage.pageContent.filter(element => element.state == 'APROBADO').length > 0;
  }

}
