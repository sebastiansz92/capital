import { Component, OnInit, Input } from '@angular/core';
import { MDBModalService, MDBModalRef } from 'ng-uikit-pro-standard';
import { VisitModalComponent } from '../visit-modal/visit-modal.component';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ConfirmationModalComponent } from 'src/app/shared/confirmation-modal/confirmation-modal.component';
import { Router } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';

@Component({
  selector: 'app-tower-details',
  templateUrl: './tower-details.component.html',
  styleUrls: ['./tower-details.component.css']
})
export class TowerDetailsComponent implements OnInit {
  modalRef: MDBModalRef;
  @Input() project;
  @Input() projectIndex;
  noms = [];
  optionsSelect = [];
  paginadores : any;

  constructor(private modalService: MDBModalService, private ps: ProjectService, private router: Router, private projectsService: ProjectsService) { }

  ngOnInit() {
    /* this.noms = this.project.towers.map(tower => {
      let aux = this.project.noms.filter(nom => nom.tower == tower);
      return {tower, noms: aux};
    }); */
    this.project.projectTypes.forEach(x => {
      this.optionsSelect.push({value: x.name, label: x.name});
    })
    this.paginadores = this.project.towers.map(tower => ({page: 0, size: 10, tower: tower.id, pages: Array.from(Array(Math.ceil(tower.nomenclaturesCount / (10)))).map((x, i) => i )}));
  }
  setPaginador(paginador, index, tower) {
    paginador.page = index;
    this.projectsService.getNomenclaturesByTower(tower.id, paginador.page, paginador.size).subscribe(
      (res) => {
        tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
      }
    )
  }

  next(paginador, tower) {
    if((paginador.page + 1) !== paginador.pages.length) {
      paginador.page = paginador.page + 1;
      this.projectsService.getNomenclaturesByTower(tower.id, paginador.page, paginador.size).subscribe(
      (res) => {
        tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
      }
    );
    }
  }

  previous(paginador, tower) {
    if(paginador.page !== 0) {
      paginador.page = paginador.page - 1;
    this.projectsService.getNomenclaturesByTower(tower.id, paginador.page, paginador.size).subscribe(
      (res) => {
        tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
      }
    )
    }
  }

  openVisitModal(towerIndex, aptoIndex, state) {
    if(state != 'APROBADO' && state != 'ENTREGADO'){
      this.modalRef = this.modalService.show(VisitModalComponent, {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'feature-modal',
        containerClass: '',
        animated: true,
        data: {
          projectIndex: this.projectIndex,
          towerIndex,
          aptoIndex
        }
    });
    } else {
      this.router.navigate(['/project', parseInt(this.projectIndex, 10), 'tower', towerIndex, 'apto', aptoIndex]);
    }
  }

  changeType(event, nom, tower) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal',
      containerClass: '',
      animated: true,
      data: {
        question: `Está seguro que desea cambiar el tipo de apartamento a ${event.value}`
      }
    });

    this.modalRef.content.action.subscribe(result => {
      if(result) {
        this.ps.changeAptoType(this.projectIndex, nom, tower, event.value);
      }
    })
  }

  getClass(state) {
    return 'Pendiente';
  }

}
