import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { switchMap } from "rxjs/operators";
import { ProjectService } from "src/app/shared/services/project.service";
import { ProjectsService } from "../../projects/project.service";

@Component({
  selector: "app-proyect-details",
  templateUrl: "./proyect-details.component.html",
  styleUrls: ["./proyect-details.component.css"],
})
export class ProyectDetailsComponent implements OnInit {
  project;
  projectLoaded = false;
  projectId;
  hasInternet = "online";
  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private router: Router,
    private projectsService: ProjectsService
  ) {}

  ngOnInit() {
    // REUTILIZAR EL COMPONENTE DE LOS DETALLES DEL PROYECTO Y VALIDAR SI TIENE INTERNET O NO.
    this.hasInternet = localStorage.getItem("status");
    if (this.hasInternet === "online" || this.hasInternet === null) {
      this.projectId = this.route.snapshot.params.id;
      this.projectsService.getProjectById(this.projectId).subscribe(
        (res) => {
          this.projectLoaded = true;
          this.project = res.body["data"];
          this.projectsService.setCurrentProject(res.body["data"]);
        },
        (error) => {
          this.router.navigate(["/projects"]);
        }
      );
      this.loadProjectWithOutPagin();
    } else {
      this.projectId = this.route.snapshot.params.id;
      this.project = JSON.parse(localStorage.getItem("project"));
      this.projectsService.setCurrentProject(this.project);
      this.projectLoaded = true;
    }

    /* this.project = this.projectService.getProject(this.projectId);
    if (this.project === undefined) {
      this.router.navigate(['/projects']);
    } */
  }

  loadProjectWithOutPagin() {
    let date = new Date();
    localStorage.setItem("fechaActualizacion", JSON.stringify(date));
    this.projectsService
      .getProjectByIdWithoutPaging(this.projectId)
      .subscribe((res) => {
        localStorage.setItem("project", JSON.stringify(res.body["data"]));
      });
  }

  checkRol() {
    if (localStorage.getItem("role") != "RESIDENTE_OBRA") {
      return true;
    } else {
      return false;
    }
  }
}
