import { Component, OnInit } from "@angular/core";
import { ProjectService } from "src/app/shared/services/project.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  FormArray,
  FormControl,
  AbstractControl,
  Validators,
} from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { MDBModalService, MDBModalRef } from "ng-uikit-pro-standard";
import { CarouselModalComponent } from "../carousel-modal/carousel-modal.component";
import * as _ from "lodash";
import { ProjectsService } from "src/app/projects/project.service";
import { ReformsService } from "src/app/reform/reforms.service";
import { DesistidoModalComponent } from "src/app/shared/desistido-modal/desistido-modal.component";

@Component({
  selector: "app-spaces",
  templateUrl: "./spaces.component.html",
  styleUrls: ["./spaces.component.css"],
})
export class SpacesComponent implements OnInit {
  project;
  tower;
  type;
  apto;
  spaceForm: FormGroup;
  pictures = [];
  modalRef: MDBModalRef;
  ready = false;
  aptoSpaces;
  projectId;
  towerIndex;
  aptoIndex;
  projectType;
  nomenclatureId;
  featuresCount = 0;
  audSpaces;
  imageSource;
  attachs = [];
  multimedia = "http://68.183.97.247:8091/";
  hasReforms = false;
  reformas;
  reformloaded = false;
  formAproved = false;
  reformAttachs = [];
  generalObservation = "";
  hasInternet = "online";
  dataOffline;
  nomenclatureObj;
  reformsAudits;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private senitizer: DomSanitizer,
    private modalService: MDBModalService,
    private router: Router,
    private projectsService: ProjectsService,
    private reformsService: ReformsService
  ) {}

  ngOnInit() {
    this.validateOfflineAudits();
    this.hasInternet = localStorage.getItem("status");
    this.projectId = this.route.snapshot.params.projectId;
    this.towerIndex = this.route.snapshot.params.towerId;
    this.aptoIndex = this.route.snapshot.params.aptoIndex;
    if (this.projectsService.project === null) {
      this.router.navigate(["/project", this.projectId]);
    }
    this.project = this.projectsService.project;
    this.projectType = this.project.towers[
      this.towerIndex
    ].nomenclaturesPage.pageContent[this.aptoIndex].projectTypeId;
    this.nomenclatureId = this.project.towers[
      this.towerIndex
    ].nomenclaturesPage.pageContent[this.aptoIndex].id;
    this.aptoSpaces = this.project["projectTypes"].find(
      (type) => type.id == this.projectType
    ).spaces;
    this.aptoSpaces.forEach((space) => {
      space.features.forEach((feat) => {
        this.featuresCount = this.featuresCount + 1;
        this.attachs.push({ featureId: feat.id, attachments: [] });
      });
    });
    this.attachs.push({ attachments: [] });
    this.nomenclatureObj = this.project.towers[this.towerIndex].nomenclaturesPage
      .pageContent[this.aptoIndex];
    this.reformas = this.nomenclatureObj.reforms;
    if (this.reformas && this.reformas.length > 0) {
      this.reformas.forEach((ref) => {
        this.reformAttachs.push({ reformId: ref.id, attachments: [] });
      });
    }
    //consultar auditorias
    if (this.hasInternet === "online") {
      this.projectsService
        .getAuditsByNomenclature(
          this.project.towers[this.towerIndex].nomenclaturesPage.pageContent[
            this.aptoIndex
          ].id
        )
        .subscribe((res) => {
          let auditId = res.body["data"].pageContent[0]?.id;
          this.generalObservation =
            res.body["data"].pageContent[0]?.generalObservation;
          if (auditId) {
            this.projectsService.getAuditDetail(auditId).subscribe((resp) => {
              let audits = resp.body["data"];
              
              this.getAudits(audits, this.nomenclatureObj, "online");
              this.ready = true;
            });
          } else {
            this.createSpaceForm(this.aptoSpaces, this.reformas);
            this.ready = true;
          }
        });
    } else {
      this.dataOffline = JSON.parse(localStorage.getItem("project"));
      let auditId = this.dataOffline.towers[this.towerIndex].nomenclaturesPage
        .pageContent[this.aptoIndex].lastAudit?.id;
      if (auditId) {
        let auditsOffline = JSON.parse(localStorage.getItem("audits"));
        auditsOffline = auditsOffline.filter(audit => audit.id === this.nomenclatureObj.id);
        this.generalObservation = auditsOffline[0]?.generalObservation;
        let audits = auditsOffline[0]?.auditsByFeatures;
        if ( audits?.length > 0 ) {
          this.getAudits(audits, this.nomenclatureObj, "offline");
        } else {
          audits = this.dataOffline.towers[this.towerIndex].nomenclaturesPage
          .pageContent[this.aptoIndex].lastAudit.detailsForLastAudit;
          this.generalObservation = this.dataOffline.towers[
            this.towerIndex
          ].nomenclaturesPage.pageContent[
            this.aptoIndex
          ].lastAudit.generalObservation;
          this.getAudits(audits, this.nomenclatureObj, "offline");
        }
        this.ready = true;
      } else {
        this.createSpaceForm(this.aptoSpaces, this.reformas);
        this.ready = true;
      }
    }
    /* if (this.project.towers[this.towerIndex].nomenclaturesPage.pageContent[this.aptoIndex].audits?.length > 0) {
      let audits = this.projectsService.getLastNElements(this.project.towers[this.towerIndex].nomenclaturesPage.pageContent[this.aptoIndex].audits, this.featuresCount+1);
      this.audSpaces = this.projectsService.mapAuditsToSpaces(this.aptoSpaces, audits);
      this.createSpaceForm(this.audSpaces, this.reformas, true);
    } else {
      this.createSpaceForm(this.aptoSpaces, this.reformas);
    }
    if (nomenclature.state == 'APROBADO' || nomenclature.state == 'ENTREGADO') {
      this.spaceForm.disable();
      this.formAproved = true;
    }
    this.ready = true; */
  }

  validateOfflineAudits() {
    if (localStorage.getItem("audits") == null) {
      localStorage.setItem("audits", JSON.stringify([]));
    } else {
      localStorage.setItem("audits", localStorage.getItem("audits"));
    }
  }

  getData() {
    if(this.audSpaces?.audits?.length > 0) {
      return this.audSpaces.audits;
    } else {
      return this.aptoSpaces;
    }
  }

  getAudits(audits, nomenclature, status) {
    if (audits?.length > 0) {
      let noreformAudits = audits.filter((x) => !x.reformName);
      this.reformsAudits = audits.filter((x) => x.reformName);
      this.audSpaces = this.projectsService.mapAuditsToSpaces(
        this.aptoSpaces,
        noreformAudits,
        status
      );
      this.createSpaceForm(this.audSpaces, this.reformas, true );
    } else {
      let noreformAudits = audits.filter((x) => !x.reformName);
      this.reformsAudits = audits.filter((x) => x.reformName);
      this.audSpaces = this.projectsService.mapAuditsToSpaces(
        this.aptoSpaces,
        noreformAudits,
        status
        );
      this.createSpaceForm(this.aptoSpaces, this.reformas);
    }
    if (nomenclature.state == "APROBADO" || nomenclature.state == "ENTREGADO") {
      this.spaceForm.disable();
      this.formAproved = true;
    }
  }

  getTypeName() {
    if (this.project != undefined) {
      return this.project.projectTypes.find(
        (type) => type.id == this.projectType
      ).name;
    } else {
      return "";
    }
  }

  createSpaceForm(data, reforms, copy?) {
    if (!copy) {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: ["", [Validators.maxLength(255)]],
        spaces: this.formBuilder.array([]),
        toggle: [false, []],
        reforms: this.formBuilder.array([]),
      });

      data.forEach((space, index) => {
        this.addSpace(space);
      });

      if (reforms?.length > 0) {
        reforms.forEach((reform) => {
          this.addReform(reform);
        });
        this.reformloaded = true;
      }
    } else {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: [
          this.generalObservation || "",
          [Validators.maxLength(255)],
        ],
        spaces: this.formBuilder.array([]),
        toggle: [false, []],
        reforms: this.formBuilder.array([]),
      });

      data.audits.forEach((space, index) => {
        this.addSpace(space, true);
      });
      if (reforms.length > 0) {
        reforms.forEach((reform) => {
          this.addReform(reform, true);
        });
        this.reformloaded = true;
      }
    }
  }

  get spaces(): FormArray {
    if (this.spaceForm != undefined) {
      return this.spaceForm.get("spaces") as FormArray;
    }
  }

  get reforms(): FormArray {
    if (this.spaceForm != undefined) {
      return this.spaceForm.get("reforms") as FormArray;
    }
  }

  getfeatures(index): FormArray {
    return this.spaceForm.get("spaces").get(index.toString()) as FormArray;
  }

  createFeatureGroup(feature, copy?) {
    if (copy && this.project.status !== 'PENDIENTE') {
      let form = this.formBuilder.group({
        name: [feature.feature, []],
        obsVisita: [
          feature.visitNote? this.checkNotNull(feature.visitNote) : '',
          [Validators.maxLength(255)],
        ],
        obsPendientes: [feature.observation, [Validators.maxLength(255)]],
        aproved: [feature.approved || false, [Validators.requiredTrue]],
        id: [feature.featureId || feature.id, []],
      });
      return form;
    } else {
      return this.formBuilder.group({
        name: [feature.name || feature.feature || feature, []],
        obsVisita: ["", [Validators.maxLength(255)]],
        obsPendientes: ["", [Validators.maxLength(255)]],
        aproved: [false, [Validators.requiredTrue]],
        id: [feature.featureId || feature.id, []],
      });
    }
  }

  createSpaceGroup(spaceobj, features, copy?) {
    if (copy) {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([]),
        toggle: [false, []],
      });

      let feats: FormArray = space.get("features") as FormArray;
      features.forEach((feature, index) => {
        feats.push(this.createFeatureGroup({...feature, spaceName: spaceobj.name}, true));
      });
      return space;
    } else {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([]),
        toggle: [false, []],
      });

      let feats: FormArray = space.get("features") as FormArray;
      features.forEach((feature) => {
        feats.push(this.createFeatureGroup(feature));
      });
      return space;
    }
  }

  addSpace(space, copy?) {
    if (copy) {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup(space, features, copy));
    } else {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup(space, features));
    }
  }

  addReform(reform, copy?) {
    if (reform.audit) {
      let ref = reform.audit;
      let form = this.formBuilder.group({
        name: [reform.name, []],
        obsVisita: [
          this.checkNotNull(ref.visitNote),
          [Validators.maxLength(255)],
        ],
        obsPendientes: [
          this.checkNotNull(ref.observation),
          [Validators.maxLength(255)],
        ],
        aproved: [ref.approved, [Validators.requiredTrue]],
        id: [reform.id, []],
      });
      this.reforms.push(form);
    } else {
      let reformaInfo = this.reformsAudits.find(ref => ref.reformName === reform.name);
      let form = this.formBuilder.group({
        name: [reformaInfo?.reformName || '', []],
        obsVisita: [reformaInfo?.visitNote || '', [Validators.maxLength(255)]],
        obsPendientes: [reformaInfo?.observation || '', [Validators.maxLength(255)]],
        aproved: [reformaInfo?.approved || false, [Validators.requiredTrue]],
        id: [reform.id, []],
      });
      this.reforms.push(form);
    }
  }

  addFeature(i, name) {
    this.getfeatures(i).push(this.createFeatureGroup(name));
  }

  addPicture(event, space, feature, id) {
    let url = URL.createObjectURL(event.target.files[0]);
    url = <string>this.senitizer.bypassSecurityTrustUrl(url);
    this.pictures.push({ url, space, feature });

    const file = event.target.files[0];
    this.getBase64(file, id);
  }

  addReformPicture(event, id) {
    let url = URL.createObjectURL(event.target.files[0]);
    url = <string>this.senitizer.bypassSecurityTrustUrl(url);
    this.pictures.push({ url, reformId: id });
    const file = event.target.files[0];
    this.getReformBase64(file, id);
  }

  openCarouselModal(space, feature) {
    /* const data = this.pictures.filter(x => x.space == space && x.feature == feature);
  this.modalRef = this.modalService.show(CarouselModalComponent, {
    backdrop: true,
    keyboard: true,
    focus: true,
    show: false,
    ignoreBackdropClick: false,
    class: 'feature-modal',
    containerClass: '',
    animated: true,
    data: {
      pictures: data
    }
}); */
  }

  getNumPics(space, feature) {
    return this.pictures.filter((x) => x.space == space && x.feature == feature)
      .length;
  }

  getRefNumPics(id) {
    return this.pictures.filter((x) => x.reformId == id).length;
  }

  aprove() {
    if (this.spaceForm.valid) {
      if (this.hasInternet === "online") {
        if (this.reformas?.length > 0) {
          this.projectsService
            .addAudit(this.nomenclatureId, {
              ...this.getFormData("APROBADO"),
              auditsByReforms: this.getReformsData(),
            })
            .subscribe(
              (res) => {
                this.router.navigate(["/project", this.projectId]);
              },
              (error) => {
                alert(error.error.data || error.error.message);
                this.router.navigate(["/project", this.projectId]);
              }
            );
        } else {
          this.projectsService
            .addAudit(this.nomenclatureId, this.getFormData("APROBADO"))
            .subscribe(
              (res) => {
                this.router.navigate(["/project", this.projectId]);
              },
              (error) => {
                alert(error.error.data || error.error.message);
                this.router.navigate(["/project", this.projectId]);
              }
            );
        }
      } else {
        let auditsOffline = JSON.parse(localStorage.getItem("audits"));
        if (this.reformas?.length > 0) {
          if (auditsOffline.length !== 0) {
            auditsOffline.forEach((element, index) => {
              if (element.id === this.nomenclatureId) {
                auditsOffline[index] = {
                  id: this.nomenclatureId,
                  ...this.getFormData("APROBADO"),
                  auditsByReforms: this.getReformsData(),
                };
              } else if (
                !auditsOffline.some((audit) => audit.id === this.nomenclatureId)
              ) {
                auditsOffline.push({
                  id: this.nomenclatureId,
                  ...this.getFormData("APROBADO"),
                  auditsByReforms: this.getReformsData(),
                });
              }
            });
          } else {
            auditsOffline.push({
              id: this.nomenclatureId,
              ...this.getFormData("APROBADO"),
              auditsByReforms: this.getReformsData(),
            });
          }
          localStorage.setItem("audits", JSON.stringify(auditsOffline));
        } else {
          if (auditsOffline.length !== 0) {
            auditsOffline.forEach((element, index) => {
              if (element.id === this.nomenclatureId) {
                auditsOffline[index] = {
                  id: this.nomenclatureId,
                  ...this.getFormData("APROBADO"),
                };
              } else if (
                !auditsOffline.some((audit) => audit.id === this.nomenclatureId)
              ) {
                auditsOffline.push({
                  id: this.nomenclatureId,
                  ...this.getFormData("APROBADO"),
                });
              }
            });
          } else {
            auditsOffline.push({
              id: this.nomenclatureId,
              ...this.getFormData("APROBADO"),
            });
          }
          localStorage.setItem("audits", JSON.stringify(auditsOffline));
          this.router.navigate(["/project", this.projectId]);
        }
      }
    } else {
      alert("Para continuar debe aprobar todas las caracteristicas");
    }
  }

  reSchedule() {
    if (this.hasInternet === "online") {
      if (this.reformas?.length > 0) {
        this.projectsService
          .addAudit(this.nomenclatureId, {
            ...this.getFormData("REPROGRAMADO"),
            auditsByReforms: this.getReformsData(),
          })
          .subscribe(
            (res) => {
              this.router.navigate(["/project", this.projectId]);
            },
            (error) => {
              alert(error.error.data || error.error.message);
              this.router.navigate(["/project", this.projectId]);
            }
          );
      } else {
        this.projectsService
          .addAudit(this.nomenclatureId, this.getFormData("REPROGRAMADO"))
          .subscribe(
            (res) => {
              this.router.navigate(["/project", this.projectId]);
            },
            (error) => {
              alert(error.error.data || error.error.message);
              this.router.navigate(["/project", this.projectId]);
            }
          );
      }
    } else {
      let auditsOffline = JSON.parse(localStorage.getItem("audits"));
      if (this.reformas?.length > 0) {
        if (auditsOffline.length !== 0) {
          auditsOffline.forEach((element, index) => {
            if (element.id === this.nomenclatureId) {
              auditsOffline[index] = {
                id: this.nomenclatureId,
                ...this.getFormData("REPROGRAMADO"),
                auditsByReforms: this.getReformsData(),
              };
            } else if (
              !auditsOffline.some((audit) => audit.id === this.nomenclatureId)
            ) {
              auditsOffline.push({
                id: this.nomenclatureId,
                ...this.getFormData("REPROGRAMADO"),
                auditsByReforms: this.getReformsData(),
              });
            }
          });
        } else {
          auditsOffline.push({
            id: this.nomenclatureId,
            ...this.getFormData("REPROGRAMADO"),
            auditsByReforms: this.getReformsData(),
          });
        }
        localStorage.setItem("audits", JSON.stringify(auditsOffline));
      } else {
        if (auditsOffline.length !== 0) {
          auditsOffline.forEach((element, index) => {
            if (element.id === this.nomenclatureId) {
              auditsOffline[index] = {
                id: this.nomenclatureId,
                ...this.getFormData("REPROGRAMADO"),
              };
            } else if (
              !auditsOffline.some((audit) => audit.id === this.nomenclatureId)
            ) {
              auditsOffline.push({
                id: this.nomenclatureId,
                ...this.getFormData("REPROGRAMADO"),
              });
            }
          });
        } else {
          auditsOffline.push({
            id: this.nomenclatureId,
            ...this.getFormData("REPROGRAMADO"),
          });
        }
        localStorage.setItem("audits", JSON.stringify(auditsOffline));
        this.router.navigate(["/project", this.projectId]);
      }
    }
  }

  copyFormControl(control: AbstractControl) {
    if (control instanceof FormControl) {
      return new FormControl(control.value);
    } else if (control instanceof FormGroup) {
      const copy = new FormGroup({});
      Object.keys(control.getRawValue()).forEach((key) => {
        copy.addControl(key, this.copyFormControl(control.controls[key]));
      });
      return copy;
    } else if (control instanceof FormArray) {
      const copy = new FormArray([]);
      control.controls.forEach((control) => {
        copy.push(this.copyFormControl(control));
      });
      return copy;
    }
  }

  getFormData(state) {
    let auditsByFeatures = [];
    let type = this.project.projectTypes.find(
      (typ) => typ.id == this.projectType
    );
    this.spaces.controls.forEach((sp: any, sindex) => {
      sp.controls.features.value.forEach((ft:any, findex) => {
        auditsByFeatures.push({
          featureId: ft.id,
          visitNote: this.checkEmptyness(
              ft.obsVisita
          ),
          observation: this.checkEmptyness(
            ft.obsPendientes
          ),
          approved: ft.aproved,
          attachments: this.attachs.find((x) => x.featureId == ft.id)
            ? this.attachs.find((x) => x.featureId == ft.id).attachments
            : [],
        });
      })
    })


    /* type.spaces.forEach((space, sindex) => {
      space.features.forEach((feature, findex) => {
        auditsByFeatures.push({
          featureId: feature.id,
          visitNote: this.checkEmptyness(
            this.spaceForm
              .get("spaces")
              .get(sindex.toString(10))
              .get("features")
              .get(findex.toString(10))
              .get("obsVisita").value
          ),
          observation: this.checkEmptyness(
            this.spaceForm
              .get("spaces")
              .get(sindex.toString(10))
              .get("features")
              .get(findex.toString(10))
              .get("obsPendientes").value
          ),
          approved: this.spaceForm
            .get("spaces")
            .get(sindex.toString(10))
            .get("features")
            .get(findex.toString(10))
            .get("aproved").value,
          attachments: this.attachs.find((x) => x.featureId == feature.id)
            ? this.attachs.find((x) => x.featureId == feature.id).attachments
            : [],
        });
      });
    }); */

    let json = {
      //lo comentado es para la observación y adjuntos generales
      generalObservation: this.spaceForm.get("obsGenerales").value,
      auditsByFeatures,
      nomenclatureState: state,
      //attachments: this.attachs.find(x => x.featureId == undefined)? this.attachs.find(x => x.featureId == undefined).attachments : [],
      auditor: "CONSTRUCTORA",
    };
    return json;
  }

  getReformsData() {
    let auditsByReforms = [];
    this.reformas.forEach((reform, index) => {
      auditsByReforms.push({
        reformId: reform.id,
        visitNote: this.checkEmptyness(
          this.spaceForm.get("reforms").get(index.toString(10)).get("obsVisita")
            .value
        ),
        observation: this.checkEmptyness(
          this.spaceForm
            .get("reforms")
            .get(index.toString(10))
            .get("obsPendientes").value
        ),
        approved: this.spaceForm
          .get("reforms")
          .get(index.toString(10))
          .get("aproved").value,
        attachments: this.reformAttachs.find((x) => x.reformId == reform.id)
          ? this.reformAttachs.find((x) => x.reformId == reform.id).attachments
          : [],
      });
    });

    return auditsByReforms;
  }

  checkEmptyness(value) {
    if (value == undefined || value.trim() == "") {
      return ".";
    } else {
      return value;
    }
  }

  checkNotNull(value) {
    if (value == ".") {
      return "";
    } else {
      return value;
    }
  }

  getBase64 = (file, id) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.imageSource = reader.result;
      if (id != 0) {
        this.attachs
          .find((x) => x.featureId == id)
          .attachments.push(this.imageSource);
      } else {
        this.attachs
          .find((x) => x.featureId == undefined)
          .attachments.push(this.imageSource);
      }
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
    return reader.result;
  };

  getReformBase64 = (file, id) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.imageSource = reader.result;
      this.reformAttachs
        .find((x) => x.reformId == id)
        .attachments.push(this.imageSource);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
    return reader.result;
  };

  getProjectImage(image_url) {
    let url = image_url.substr(10);
    return this.multimedia + image_url;
  }

  toggleAllChecks(controlSpace, reforms) {
    let allToggled = true;
    let word = reforms ? "reforms" : "features";
    for (let control of controlSpace.get(word)["controls"]) {
      allToggled = allToggled && control.get("aproved").value;
    }
    if (!allToggled) {
      controlSpace.get(word)["controls"].forEach((control, index) => {
        control.get("aproved").setValue(true);
      });
      controlSpace.get("toggle").setValue(!allToggled);
    } else {
      controlSpace.get(word)["controls"].forEach((control, index) => {
        control.get("aproved").setValue(false);
      });
      controlSpace.get("toggle").setValue(!allToggled);
    }
  }

  handleChange(event, space) {
    if (!event.target.checked) {
      space.get("toggle").setValue(false);
    }
  }

  desistir() {
    let nom = this.project.towers[this.towerIndex].nomenclaturesPage.pageContent[this.aptoIndex];
    this.modalRef = this.modalService.show(DesistidoModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'feature-modal-rounded',
      containerClass: '',
      animated: true,
      data: {
        question: `Cúal es el motivo del desistimiento?`,
        nomenclature: this.project.towers[this.towerIndex].nomenclaturesPage.pageContent[this.aptoIndex],
        projectTypes: this.project.projectTypes
      }
    })

    this.modalRef.content.action.subscribe(result => {
      if(result && !result.sameType) {
        this.projectsService.desistNomenclature({
          cause: result.reason,
          nomenclatures: [nom.id],
          destProjectType: parseInt(result.newType, 10)
        }).subscribe(resp => {
          console.log(resp)
          window.history.back();
        });
      } else if(result && result.sameType) {
        this.projectsService.desistNomenclature({
          cause: result.reason,
          nomenclatures: [nom.id],
          destProjectType: nom.projectTypeId
        }).subscribe(resp => {
          console.log(resp)
          window.history.back();
        });
      } else {
        console.log('no change')
      }
    })
  }
}
