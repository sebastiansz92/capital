import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-visit-modal',
  templateUrl: './visit-modal.component.html',
  styleUrls: ['./visit-modal.component.css']
})
export class VisitModalComponent implements OnInit {
  towerIndex;
  projectIndex;
  aptoIndex;

  constructor(public modalRef: MDBModalRef, private router: Router) { }

  ngOnInit() {
  }

  onYesClick() {
    this.router.navigate(['/project', parseInt(this.projectIndex, 10), 'tower', this.towerIndex, 'apto', this.aptoIndex]);
    this.modalRef.hide();
  }

  onNoClick() {
    this.router.navigate(['/project', parseInt(this.projectIndex, 10), 'tower', this.towerIndex, 'apto', this.aptoIndex]);
    this.modalRef.hide();
  }

}
