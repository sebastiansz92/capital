import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from '../app/shared/shared.module';
import { LoginComponent } from './login/login.component';
import { AppRouting } from './routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule, StepperModule, WavesModule, SelectModule, PreloadersModule, ProgressbarModule } from 'ng-uikit-pro-standard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProjectsComponent } from './projects/projects.component';
import { NewProjectComponent } from './new-project/new-project.component';
import { ProjectFirstStepComponent } from './new-project/project-first-step/project-first-step.component';
import { TypesComponent } from './new-project/types/types.component';
import { FeatureCreationComponent } from './new-project/feature-creation/feature-creation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NomenclatureComponent } from './new-project/nomenclature/nomenclature.component';
import { SummaryComponent } from './new-project/summary/summary.component';
import { TypeCreationModalComponent } from './new-project/type-creation-modal/type-creation-modal.component';
import { ProyectDetailsComponent } from './audit/proyect-details/proyect-details.component';
import { TowerDetailsComponent } from './audit/tower-details/tower-details.component';
import { VisitModalComponent } from './audit/visit-modal/visit-modal.component';
import { CarouselModalComponent } from './audit/carousel-modal/carousel-modal.component';
import { SpacesComponent } from './audit/spaces/spaces.component';
import { ConfirmationModalComponent } from './shared/confirmation-modal/confirmation-modal.component';
import { SideBarComponent } from './shared/side-bar/side-bar.component';

/* HammerJS and swipe functionality */
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { MainComponent } from './main/main.component';
import { HistoryProjectSelectionComponent } from './historic/history-project-selection/history-project-selection.component';
declare var Hammer: any;
import { HistoricRoutingModule} from '../app/historic/historic-routing.module';
import { HistoricModule } from './historic/historic.module';
import { ReformModule } from './reform/reform.module';
import { ReformRoutingModule } from './reform/reform-routing.module';
import { ProjectSelectionComponent } from './delivery/project-selection/project-selection.component';
import { DeliveryModule } from './delivery/delivery.module';
import { DeliveryRoutingModule } from './delivery/delivery-routing.module';
import { SignupComponent } from './signup/signup.component';
import { EnterTextComponent } from './shared/enter-text/enter-text.component';
import { DeactivateGuard } from './shared/services/http/deactiveGuard';
import { OfflineModule } from './offline/offline.module';
import { OfflineRoutingModule } from './offline/offline-routing.module';
import { ConnectionServiceModule } from 'ng-connection-service';

@Injectable()
export class AppHammerConfig extends HammerGestureConfig {
  overrides = <any> {
    'pan': { direction: Hammer.DIRECTION_All },
    'swipe': { direction: Hammer.DIRECTION_VERTICAL },
  };

  buildHammer(element: HTMLElement) {
    const mc = new Hammer(element, {
      touchAction: 'auto',
          inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput,
          recognizers: [
            [Hammer.Swipe, {
              direction: Hammer.DIRECTION_HORIZONTAL
            }]
          ]
    });
    return mc;
  }
}
/* End HammerJS and swipe functionality */

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProjectsComponent,
    NewProjectComponent,
    ProjectFirstStepComponent,
    TypesComponent,
    FeatureCreationComponent,
    NomenclatureComponent,
    SummaryComponent,
    TypeCreationModalComponent,
    ProyectDetailsComponent,
    TowerDetailsComponent,
    VisitModalComponent,
    SpacesComponent,
    CarouselModalComponent,
    ConfirmationModalComponent,
    SideBarComponent,
    MainComponent,
    HistoryProjectSelectionComponent,
    ProjectSelectionComponent,
    SignupComponent,
    EnterTextComponent
  ],
  imports: [
    MDBBootstrapModulesPro.forRoot(),
    ToastModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRouting,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    StepperModule,
    WavesModule,
    FormsModule,
    ReactiveFormsModule,
    HistoricRoutingModule,
    HistoricModule,
    ReformModule,
    ReformRoutingModule,
    DeliveryModule,
    DeliveryRoutingModule,
    OfflineModule,
    OfflineRoutingModule,
    SelectModule,
    WavesModule,
    PreloadersModule,
    ProgressbarModule,
    ConnectionServiceModule
  ],
  providers: [MDBSpinningPreloader,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: AppHammerConfig
    },
  DeactivateGuard],
  bootstrap: [AppComponent],
  entryComponents: [
    FeatureCreationComponent,
    TypeCreationModalComponent,
    VisitModalComponent,
    CarouselModalComponent,
    ConfirmationModalComponent,
    EnterTextComponent
  ],
  exports : [
    MDBBootstrapModulesPro,
    ToastModule
  ]
})
export class AppModule { }
