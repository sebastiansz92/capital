import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryRoutingModule } from './delivery-routing.module';
import { MDBBootstrapModulesPro, ToastModule, MDBSpinningPreloader, PreloadersModule, WavesModule, AccordionModule, CollapseModule } from 'ng-uikit-pro-standard';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppHammerConfig } from '../app.module';
import { environment } from 'src/environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeliveryComponent } from './delivery/delivery.component';
import { AptoSelectionComponent } from './apto-selection/apto-selection.component';
import { DeliveryFormComponent } from './delivery-form/delivery-form.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { OwnerAuditComponent } from './owner-audit/owner-audit.component';
import { DeliveryAptoComponent } from '../offline/delivery-apto/delivery-apto.component';


@NgModule({
  declarations: [DeliveryComponent, AptoSelectionComponent, DeliveryFormComponent, OwnerAuditComponent, DeliveryAptoComponent],
  imports: [
    CommonModule,
    DeliveryRoutingModule,
    MDBBootstrapModulesPro.forRoot(),
    ToastModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule,
    SignaturePadModule ,
    PreloadersModule,
    WavesModule,
    CollapseModule, AccordionModule
  ],
  exports: [DeliveryAptoComponent]
})
export class DeliveryModule { }
