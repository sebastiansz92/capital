import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliveryComponent } from './delivery/delivery.component';
import { ProjectSelectionComponent } from './project-selection/project-selection.component';
import { AptoSelectionComponent } from './apto-selection/apto-selection.component';
import { DeliveryFormComponent } from './delivery-form/delivery-form.component';
import { OwnerAuditComponent } from './owner-audit/owner-audit.component';
import { AdminGuard } from '../admin/admin.guard';


const routes: Routes = [
  {
    path: '', component: DeliveryComponent,
    children: [{path: '', component: ProjectSelectionComponent}],
    canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA', 'RESIDENTE_OBRA']}
  },
  {
    path: 'delivery', component: DeliveryComponent,
    children: [
      {path: '', component: ProjectSelectionComponent},
      {path: 'project_selection', component: ProjectSelectionComponent},
      {path: 'owner/project/:id/tower/:tower/apto/:apto', component: OwnerAuditComponent},
      {path: 'project/:id', component: AptoSelectionComponent},
      {path: 'project/:id/tower/:tower/apto/:apto', component: DeliveryFormComponent},
    ],
    canActivate: [AdminGuard], data : {expectedRoles: ['ADMIN', 'RESIDENTE_ENTREGA', 'RESIDENTE_OBRA']}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryRoutingModule { }
