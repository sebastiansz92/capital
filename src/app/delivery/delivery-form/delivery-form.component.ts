import { Component, OnInit, ViewChild, SimpleChanges } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { contract } from "./contract";
import { ActivatedRoute, Router } from "@angular/router";
import { ProjectService } from "src/app/shared/services/project.service";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { ConnectionService } from "src/app/shared/services/connectionServices";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { ProjectsService } from "src/app/projects/project.service";
import { WATERMARK } from "./acta";
import { MDBModalRef, MDBModalService } from "ng-uikit-pro-standard";
import { ConfirmationModalComponent } from "src/app/shared/confirmation-modal/confirmation-modal.component";
import { SignaturePad } from "angular2-signaturepad";

@Component({
  selector: "app-delivery-form",
  templateUrl: "./delivery-form.component.html",
  styleUrls: ["./delivery-form.component.css"],
})
export class DeliveryFormComponent implements OnInit {
  deliveryForm: FormGroup;
  firmAgent;
  firmCustomer;
  CONTRACT = contract;
  project;
  tower;
  nom;
  sign1;
  sign2;
  signaturePadOptions: Object = {
    // passed through to szimek/signature_pad constructor
    minWidth: 2,
    dotSize: 2,
  };
  @ViewChild("sign_canvas") signaturePad1: SignaturePad;
  @ViewChild("sign_canvas2") signaturePad2: SignaturePad;
  canvas;
  day;
  month;
  year;
  jsonData = null;
  btnDisabled = false;
  watermark = WATERMARK;
  projectimage;
  modalRef: MDBModalRef;
  nomenclature;
  hasInternet = "online";

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private router: Router,
    private connectionService: ConnectionService,
    private http: HttpClient,
    private projectsService: ProjectsService,
    private modalService: MDBModalService
  ) {}

  ngOnInit() {
    this.validateOfflineOwnerAudit();
    this.jsonData = this.projectsService.jsonData;
    if (this.jsonData == null || this.jsonData == undefined) {
      this.router.navigate(["/projects"]);
    }
    let projectid = this.route.snapshot.params.id;
    this.tower = this.route.snapshot.params.tower;
    this.nom = this.route.snapshot.params.apto;
    this.project = this.projectsService.project;
    /* this.getImage('https://cdn.dribbble.com/users/1212223/screenshots/3088645/moka.png', (obj) => {
      this.projectimage = obj;
      console.log(obj);
    }); */
    console.log(this.project);
    this.createDeliveryForm();
    this.getDate();
  }

  ngAfterViewInit(): void {
    this.resizeSignaturePad();
  }

  getDate() {
    let date = new Date();
    this.day = date.getDate();
    this.month = this.findMonth(date.getMonth());
    this.year = date.getFullYear();
  }

  findMonth(month) {
    switch (month) {
      case 0:
        return "Enero";
      case 1:
        return "Febrero";
      case 2:
        return "Marzo";
      case 3:
        return "Abril";
      case 4:
        return "Mayo";
      case 5:
        return "Junio";
      case 6:
        return "Julio";
      case 7:
        return "Agosto";
      case 8:
        return "Septiembre";
      case 9:
        return "Octubre";
      case 10:
        return "Noviembre";
      case 11:
        return "Diciembre";
    }
  }

  getImage(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open("GET", "https://cors-anywhere.herokuapp.com/" + url);
    xhr.responseType = "blob";
    xhr.send();
  }

  resizeSignaturePad() {
    let container = document.getElementById("signcontainer");
    let width = container.offsetWidth;
    let height = container.offsetHeight;
    let image1 = this.signaturePad1.toDataURL();
    let image2 = this.signaturePad2.toDataURL();
    this.signaturePad1.set("canvasWidth", width);
    this.signaturePad2.set("canvasWidth", width);
    this.signaturePad1.set("canvasHeight", height);
    this.signaturePad2.set("canvasHeight", height);
    this.signaturePad1.fromDataURL(image1);
    this.signaturePad2.fromDataURL(image2);
  }

  createDeliveryForm() {
    this.deliveryForm = this.formBuilder.group({
      name: ["", [Validators.required]],
      id: ["", [Validators.required]],
      parking: ["", [Validators.required]],
      usefulRoom: ["", [Validators.required]],
      address: ["", [Validators.required]],
      municipality: ["", [Validators.required]],
      energy: ["", [Validators.required]],
      aqueduct: ["", [Validators.required]],
      gas: ["", [Validators.required]],
      firmMunicipality: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
    });
  }

  replaces() {
    Object.keys(this.deliveryForm.value).forEach((key) => {
      this.CONTRACT = this.CONTRACT.replace(
        "$" + key,
        this.deliveryForm.get(key).value
      );
    });
    this.CONTRACT = this.CONTRACT.replace("$project", this.project.name);
    this.CONTRACT = this.CONTRACT.replace("$tower", this.tower);
    this.CONTRACT = this.CONTRACT.replace("$nom", this.nom);

    this.CONTRACT = this.CONTRACT.replace("$day", this.day);
    this.CONTRACT = this.CONTRACT.replace("$month", this.month);
    this.CONTRACT = this.CONTRACT.replace("$year", this.year);
  }

  clear1() {
    this.signaturePad1.clear();
  }

  clear2() {
    this.signaturePad2.clear();
  }

  getAuditTable(data) {
    return {
      width: "auto",
      alignment: "center",
      layout: {
        // code from lightHorizontalLines:
        hLineWidth: function (i, node) {
          return 0;
        },
        vLineWidth: function (i) {
          return 0;
        },
        paddingLeft: function (i) {
          return i === 0 ? 0 : 8;
        },
        paddingRight: function (i, node) {
          return i === node.table.widths.length - 1 ? 0 : 8;
        },
        // code for zebra style:
        fillColor: function (i, node) {
          return i % 2 === 0 ? "#edf8ff" : null;
        },
      },
      table: {
        alignment: "center",
        widths: ["*", "*", "*"],
        body: this.mapToPdfTable(data),
      },
    };
  }

  mapToPdfTable(json) {
    let audit = json.auditsByFeatures;
    let spacesArray = [];
    let torre = this.route.snapshot.params.tower;
    let apto = this.route.snapshot.params.apto;
    let nom = this.project.towers
      .find((tower) => tower.name == torre)
      .nomenclaturesPage.pageContent.find(
        (apt) => apt.number == parseInt(apto, 10)
      );
    this.nomenclature = nom;
    console.log(this.nomenclature);
    this.project.projectTypes
      .find((type) => type.id == nom.projectTypeId)
      .spaces.forEach((space) => {
        const header = [
          {
            text: space.name,
            colSpan: 3,
            style: "header",
            alignment: "center",
          },
          {},
          {},
        ];
        const subheaders = [
          { text: "Nombre", style: "subheader", width: "*" },
          { text: "Obsservaciones", style: "subheader" },
          { text: "Aprobado", style: "subheader" },
        ];
        spacesArray.push(header);
        spacesArray.push(subheaders);
        const features = space.features.forEach((feature, index) => {
          spacesArray.push([
            { text: this.validateEmptyField(feature.name), style: "value" },
            {
              text: this.validateEmptyField(
                audit.find((el) => el.featureId == feature.id).visitNote
              ),
              style: "value",
            },
            {
              text: this.validateEmptyField(
                audit.find((el) => el.featureId == feature.id).approved
                  ? "Sí"
                  : "No"
              ),
              style: "value",
            },
          ]);
        });

        spacesArray.push(features);
        spacesArray = spacesArray.filter((element) => element !== undefined);
      });

    // Si tiene reformas
    let reforms = json.auditsByReforms.map((aud) => ({
      ...aud,
      reform: nom.reforms.find((reform) => reform.id == aud.reformId).name,
    }));
    if (reforms.length > 0) {
      spacesArray.push([
        { text: "Reformas", colSpan: 3, style: "header", alignment: "center" },
        {},
        {},
      ]);
      spacesArray.push([
        { text: "Nombre", style: "subheader", width: "*" },
        { text: "Obsservaciones", style: "subheader" },
        { text: "Aprobado", style: "subheader" },
      ]);
    }
    reforms.forEach((reform) => {
      spacesArray.push([
        { text: this.validateEmptyField(reform.reform), style: "value" },
        { text: this.validateEmptyField(reform.visitNote), style: "value" },
        {
          text: this.validateEmptyField(reform.approved ? "Sí" : "No"),
          style: "value",
        },
      ]);
    });
    return spacesArray;
  }

  validateEmptyField(field) {
    return field !== "" ? field : "---";
  }

  finish() {
    this.btnDisabled = true;
    if (
      this.deliveryForm.valid &&
      this.sign1 != undefined &&
      this.sign2 != undefined
    ) {
      //Obtener el base64 del pdf
      this.replaces();
      const data = {
        background: [
          {
            image: this.watermark,
            width: 792,
          },
        ],
        pageMargins: [40, 150, 40, 40],
        content: [
          /* {
          image: this.projectimage,
          fit: [90, 90],
          absolutePosition: {x: 440, y: 20}
        }, */
          {
            width: "*",
            text: this.project.name,
            style: "title",
            alignment: "center",
          },
          {
            width: "*",
            text: `${this.tower} - ${this.nom}`,
            style: "title",
            alignment: "center",
          },
          { width: "*", text: "\n\r" },
          { width: "*", text: this.CONTRACT, alignment: "justify" },
          { width: "*", text: "\n\r" },
          this.getTable(),
          { width: "*", text: "\n\r" },
          { width: "*", text: "\n\r" },
          { width: "*", text: "\n\r" },
          this.getAuditTable(this.projectsService.jsonData),
        ],
        styles: {
          sign: {
            color: "blue",
            fontSize: 10,
          },
          title: {
            bold: true,
            fillColor: "#359bcc",
            margin: [2, 2, 2, 2],
          },
          lastValue: {
            margin: [0, 3, 0, 20],
          },
          value: {
            margin: [0, 3, 0, 3],
          },
          header: {
            bold: true,
            color: "white",
            fillColor: "#359bcc",
            margin: [2, 2, 2, 2],
          },
          subheader: {
            bold: true,
            color: "white",
            fillColor: "#359bcc",
            margin: [2, 2, 2, 2],
          },
        },
      };
      const pdf = pdfMake.createPdf(data);
      //Obtener los campos del formulario del contrato
      let contract = {
        proprietaryName: this.deliveryForm.get("name").value,
        proprietaryLastname: "LastName",
        proprietaryIdentification: this.deliveryForm.get("id").value,
        proprietaryEmail: this.deliveryForm.get("email").value,
        address: this.deliveryForm.get("address").value,
        city: this.deliveryForm.get("municipality").value,
        deliveryCity: this.deliveryForm.get("firmMunicipality").value,
        parking: this.deliveryForm.get("parking").value,
        utilRoom: this.deliveryForm.get("usefulRoom").value,
        aqueduct: this.deliveryForm.get("aqueduct").value,
        gas: this.deliveryForm.get("gas").value,
        energy: this.deliveryForm.get("energy").value,
        signatures: [
          {
            imageBase64: this.sign1,
          },
          {
            imageBase64: this.sign2,
          },
        ],
      };
      //Llamar al backend para validar campos y guardar contrato
      let base64 = pdf.getBase64((pdf) => {
        if (pdf != undefined) {
          this.addOwnerAudit(data, contract, pdf);
        } else {
          alert("No es posible decodificar el PDF a base64");
        }
      });
    } else {
      alert(
        "Para continuar el formulario debe estár correctamente diligenciado"
      );
    }
  }

  validateOfflineOwnerAudit() {
    if (localStorage.getItem("ownerAudits") == null) {
      localStorage.setItem("ownerAudits", JSON.stringify([]));
    } else {
      localStorage.setItem("ownerAudits", localStorage.getItem("ownerAudits"));
    }
  }

  addOwnerAudit(data, contract, pdfCodificado) {
    this.hasInternet = localStorage.getItem("status");
    if (this.hasInternet === "online" || this.hasInternet === null) {
      this.projectsService
        .addOwnerAudit(this.nomenclature.id, {
          ...this.projectsService.jsonData,
          contract: { ...contract, pdfBase64: pdfCodificado },
        })
        .subscribe(
          (res) => {
            this.generatePdf("download", data, contract);
            this.router.navigate(["/projects"]);
            this.btnDisabled = false;
          },
          (error) => {
            if (error.status == 400) {
              alert(error.error.message);
            } else {
              alert(error.error.data);
            }
            this.btnDisabled = false;
          }
        );
    } else {
      let ownerAuditsOffline = JSON.parse(localStorage.getItem("ownerAudits"));
      ownerAuditsOffline.push({
        nomenclatureId: this.nomenclature.id,
        ...this.projectsService.jsonData,
        contract: { ...contract, pdfBase64: pdfCodificado },
      });
      localStorage.setItem("ownerAudits", JSON.stringify(ownerAuditsOffline));
      this.generatePdf("download", data, contract);
      this.router.navigate(["/offline-projects"]);
    }
  }

  getTable() {
    return {
      layout: "noBorders", // optional
      table: {
        // headers are automatically repeated if the table spans over multiple pages
        // you can declare how many rows should be treated as headers
        headerRows: 1,
        widths: ["auto", 70, "auto"],

        body: [
          [{ text: "Recibido por:" }, " ", { text: "Entregado por:" }],
          [
            { text: "Firma", style: "sign" },
            " ",
            { text: "Firma", style: "sign" },
          ],
          [
            { stack: [{ image: this.sign1, width: 200 }] },
            { text: " ", width: 20 },
            { stack: [{ image: this.sign2, width: 200 }] },
          ],
          [
            " ",
            " ",
            {
              text: "Constructora Capital Medellín S.A.S",
              alignment: "center",
            },
          ],
        ],
      },
    };
  }

  generatePdf(action = "open", data, contract) {
    const documentDefinition = data;
    const pdfName =
      this.project.name +
      "_" +
      this.tower +
      "_" +
      this.nom +
      "_" +
      this.year +
      "_" +
      this.month +
      "_" +
      this.day;
    switch (action) {
      case "open":
        pdfMake.createPdf(documentDefinition).open();
        break;
      case "print":
        pdfMake.createPdf(documentDefinition).print();
        break;
      case "download":
        pdfMake.createPdf(documentDefinition).download(pdfName);
        break;
      default:
        pdfMake.createPdf(documentDefinition).open();
        break;
    }
  }

  drawComplete() {
    this.sign1 = this.signaturePad1.toDataURL();
  }

  drawComplete2() {
    this.sign2 = this.signaturePad2.toDataURL();
  }

  drawStart() {}

  getBase64 = async (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => console.log(reader.result);
    reader.onerror = function (error) {};
    return reader.result;
  };

  sendMesage(deita) {
    const url =
      "https://us-central1-capital-constructora.cloudfunctions.net/sendMail";
    const params: HttpParams = new HttpParams();
    const headers = new HttpHeaders()
      .set("content-type", "application/json")
      .set("Access-Control-Allow-Origin", "*");
    const options = { headers };

    const data = {
      dest: this.deliveryForm.get("email").value,
      file: deita,
    };
    return this.http
      .post(url, data, options)
      .toPromise()
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  openConfirmationModal() {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: "feature-modal",
      containerClass: "",
      animated: true,
      data: {
        question: "¿Está seguro que desea continuar?",
      },
    });
    this.modalRef.content.action.subscribe((result: any) => {
      if (result) {
        this.finish();
      }
    });
    this.modalRef.hide();
  }
}
