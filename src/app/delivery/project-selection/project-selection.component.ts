import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { Router } from '@angular/router';
import { ProjectsService } from 'src/app/projects/project.service';

@Component({
  selector: 'app-project-selection',
  templateUrl: './project-selection.component.html',
  styleUrls: ['./project-selection.component.css']
})
export class ProjectSelectionComponent implements OnInit {

  projects = [];
  status: string;

  constructor(private projectService: ProjectService, private router: Router, private projectsService: ProjectsService) { }

  ngOnInit() {
    //this.projects = this.projectService.data.projects.map((project, index) => ({label: project.name, value: index}));
    this.status = localStorage.getItem("status");
    this.projectsService.getProjects().subscribe(res => {
      let projs = res.body['data'];
      this.projects = projs.map(proj => ({label: proj.name, value: proj.projectId}));
    });
  }

  goToAptoSelection(event) {
    this.router.navigate(['/delivery', 'project', event.value] );
  }



}
