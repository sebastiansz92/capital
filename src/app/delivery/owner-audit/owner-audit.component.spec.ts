import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OwnerAuditComponent } from './owner-audit.component';

describe('OwnerAuditComponent', () => {
  let component: OwnerAuditComponent;
  let fixture: ComponentFixture<OwnerAuditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
