import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, AbstractControl, FormControl, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ProjectsService } from 'src/app/projects/project.service';
import { ReformsService } from 'src/app/reform/reforms.service';

@Component({
  selector: 'app-owner-audit',
  templateUrl: './owner-audit.component.html',
  styleUrls: ['./owner-audit.component.css']
})
export class OwnerAuditComponent implements OnInit {
  project;
  tower;
  type;
  apto;
  spaceForm: FormGroup;
  pictures = [];
  ready = false;

  featuresCount = 0;
  aptoSpaces;
  projectType;
  nomenclatureId;
  projectId;
  towerIndex;
  aptoIndex;
  audSpaces;
  imageSource;
  attachs = [];
  multimedia = 'http://68.183.97.247:8091/';
  hasReforms = false;
  reformas;
  reformloaded = false;
  generalObservation = '';
  hasInternet = "online";

  constructor(private projectService: ProjectService, private route: ActivatedRoute, private formBuilder: FormBuilder,
              private senitizer: DomSanitizer, private router: Router, private projectsService: ProjectsService,
              private reformsService: ReformsService) { }

  ngOnInit() {
    this.hasInternet = localStorage.getItem("status");
    this.projectId = this.route.snapshot.params.id;
    this.towerIndex = this.route.snapshot.params.tower;
    this.aptoIndex = this.route.snapshot.params.apto;
    if (this.projectsService.project === null) {
      this.router.navigate(['/project', this.projectId]);
    }
    this.project = this.projectsService.project;
    this.projectType = this.project?.towers.find(tower => tower.name === this.towerIndex).nomenclaturesPage.pageContent.find(nom => nom.number.toString() === this.aptoIndex).projectTypeId;
    this.nomenclatureId = this.project?.towers.find(tower => tower.name === this.towerIndex).nomenclaturesPage.pageContent.find(nom => nom.number.toString() === this.aptoIndex).id;
    this.aptoSpaces = this.project['projectTypes'].find(type => type.id == this.projectType).spaces;
    this.aptoSpaces.forEach(space => {
      space.features.forEach(feat => {
        this.featuresCount = this.featuresCount + 1;
        this.attachs.push({featureId: feat.id, attachments: []});
      });
    });
    this.attachs.push({attachments: []});
    let nomenclature = this.project?.towers.find(tower => tower.name === this.towerIndex).nomenclaturesPage.pageContent.find(nom => nom.number.toString() === this.aptoIndex);
    console.log(nomenclature);
    this.reformas = nomenclature.reforms;
    //consultar auditorias
    if (this.hasInternet === "online" || this.hasInternet === null) { 
      this.projectsService.getAuditsByNomenclature(this.nomenclatureId)
      .subscribe(res => {
        let auditId = res.body['data'].pageContent.filter(auditoria => auditoria.auditor !== 'CONSTRUCTORA')[0]?.id;
        this.generalObservation = res.body['data'].pageContent.filter(auditoria => auditoria.auditor !== 'CONSTRUCTORA')[0]?.generalObservation;
        if(auditId) {
          this.projectsService.getAuditDetail(auditId)
          .subscribe(resp => {
            let audits = resp.body['data'];
            console.log(audits);
            if (audits?.length > 0) {
              console.log(audits, this.reformas);
              let noreformAudits = audits.filter(x => !x.reform);
              let reformAudits = audits.filter(x => x.reform);
              if(reformAudits) {
                this.reformas = this.reformas.map(reforma => ({...reforma, audit: reformAudits.find(refAudit => refAudit.reform.id === reforma.id)}));
              }
              this.audSpaces = this.projectsService.mapAuditsToSpaces(this.aptoSpaces, noreformAudits, "online");
              this.createSpaceForm(this.audSpaces, this.reformas);
            } else {
              this.createSpaceForm(this.aptoSpaces, this.reformas);
            }
            this.ready = true;
          })
        } else {
          this.createSpaceForm(this.aptoSpaces, this.reformas);
          this.ready = true;
        }
      })
    } else {
      let auditsByNomenclatura = this.project.towers[0].nomenclaturesPage.pageContent.find( project => project.id === this.nomenclatureId);
      console.log(auditsByNomenclatura);
      let auditId = auditsByNomenclatura.lastAudit.auditor !== 'CONSTRUCTORA' ? auditsByNomenclatura.lastAudit.id : null;
      this.generalObservation = auditsByNomenclatura.lastAudit.auditor !== 'CONSTRUCTORA' ? auditsByNomenclatura.lastAudit.generalObservation : null;
      if(auditId) {
        this.projectsService.getAuditDetail(auditId)
        .subscribe(resp => {
          let audits = resp.body['data'];
          console.log(audits);
          if (audits?.length > 0) {
            console.log(audits, this.reformas);
            let noreformAudits = audits.filter(x => !x.reform);
            let reformAudits = audits.filter(x => x.reform);
            if(reformAudits) {
              this.reformas = this.reformas.map(reforma => ({...reforma, audit: reformAudits.find(refAudit => refAudit.reform.id === reforma.id)}));
            }
            console.log(noreformAudits);
            this.audSpaces = this.projectsService.mapAuditsToSpaces(this.aptoSpaces, noreformAudits, "online");
            this.createSpaceForm(this.audSpaces, this.reformas, true);
          } else {
            this.createSpaceForm(this.aptoSpaces, this.reformas);
          }
          this.ready = true;
        })
      } else {
        this.createSpaceForm(this.aptoSpaces, this.reformas);
        this.ready = true;
      }
    }
    
  }

  createSpaceForm(data, reforms, copy?) {
    if(!copy) {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: ['', [Validators.maxLength(255)]],
        spaces: this.formBuilder.array([]),
        toggle: [false, []],
        reforms: this.formBuilder.array([])
      });
  
      data.forEach((space, index) => {
        this.addSpace(space);
      });

      if (reforms.length > 0) {
        reforms.forEach(reform => {
          this.addReform(reform);
        })
        this.reformloaded = true;
      }

    } else {
      this.spaceForm = this.formBuilder.group({
        obsGenerales: [this.generalObservation || '', [Validators.maxLength(255)]],
        spaces: this.formBuilder.array([]),
        toggle: [false, []],
        reforms: this.formBuilder.array([])
      });
      
      data.audits.forEach((space, index) => {
        this.addSpace(space, true);
      });
      if (reforms.length > 0) {
        reforms.forEach(reform => {
          this.addReform(reform, true);
        })
        this.reformloaded = true;
      }
    }
  }

  addReform(reform, copy?) {
    if(reform.audits?.filter(x => x.auditor == 'PROPIETARIO').length > 0) {
      let ref = reform.audits.filter(x => x.auditor == 'PROPIETARIO')[reform.audits.filter(x => x.auditor == 'PROPIETARIO').length -1];
      let form = this.formBuilder.group({
        name: [reform.name, []],
        obs: [this.checkNotNull(ref.visitNote), [Validators.maxLength(255)]],
        obsPendientes: [this.checkNotNull(ref.observation), [Validators.maxLength(255)]],
        aproved: [ref.approved, [Validators.requiredTrue]],
        id: [reform.id, []]
      });
      this.reforms.push(form);
    }else {
      //let ref = reform.audits[reform.audits.length -1];
      let form = this.formBuilder.group({
        name: ['', []],
        obs: ['', [Validators.maxLength(255)]],
        obsPendientes: ['', [Validators.maxLength(255)]],
        aproved: [false, [Validators.requiredTrue]],
        id: [reform.id, []]
      });
      this.reforms.push(form);
    }
  }

  get reforms(): FormArray {
    if(this.spaceForm != undefined) {
      return this.spaceForm.get('reforms') as FormArray;
    }
  }

  get spaces(): FormArray {
    if (this.spaceForm != undefined) {
      return this.spaceForm.get('spaces') as FormArray;
    }
  }

  getfeatures(index): FormArray {
    return this.spaceForm.get('spaces').get(index.toString()) as FormArray;
  }

  createFeatureGroup(feature, copy?) {
    if (copy) {
      let form = this.formBuilder.group({
        name: [feature.name, []],
        obs: [this.checkNotNull(feature.aud.visitNote), [Validators.maxLength(255)]],
        aproved: [feature.aud.approved, [Validators.requiredTrue]],
        id: [feature.aud.featureId, []]
      });
      return form;
    } else {
      return this.formBuilder.group({
        name: [feature, []],
        obs: ['', [Validators.maxLength(255)]],
        aproved: [false, [Validators.requiredTrue]],
        id: [feature.id, []]
      });
    }
  }

  createSpaceGroup(spaceobj, features, data?) {
    if(data) {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([]),
        toggle: [false, []]
      });

      space.patchValue(data);
  
      let feats: FormArray = space.get('features') as FormArray;
      features.forEach((feature, index) => {
        feats.push(this.createFeatureGroup(feature, true));
      });
      return space;
    } else {
      let space = this.formBuilder.group({
        name: [spaceobj.name, []],
        features: this.formBuilder.array([]),
        toggle: [false, []]
      });
  
      let feats: FormArray = space.get('features') as FormArray;
      features.forEach(feature => {
        feats.push(this.createFeatureGroup(feature));
      });
      return space;
    }
  }

  addSpace(space, data?) {
    if (data) {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup(space, features, data));
    } else {
      const features = space.features;
      this.spaces.push(this.createSpaceGroup(space, features));
    }
  }

  addFeature(i, name) {
    this.getfeatures(i).push(this.createFeatureGroup(name));
  }

copyFormControl(control: AbstractControl) {
  if (control instanceof FormControl) {
      return new FormControl(control.value);
  } else if (control instanceof FormGroup) {
      const copy = new FormGroup({});
      Object.keys(control.getRawValue()).forEach(key => {
          copy.addControl(key, this.copyFormControl(control.controls[key]));
      });
      return copy;
  } else if (control instanceof FormArray) {
      const copy = new FormArray([]);
      control.controls.forEach(control => {
          copy.push(this.copyFormControl(control));
      });
      return copy;
  }
}

reject() {
  this.projectsService.addOwnerAudit(this.nomenclatureId, this.getFormData('REPROGRAMADO')).subscribe(res => {
    this.router.navigate(['/project', this.projectId]);
  }, error => {
    alert(error.error.data || error.error.message);
    this.router.navigate(['/project', this.projectId]);
  });
  this.router.navigate(['/projects']);
}

aprove() {

  if(this.spaceForm.valid) {
    let json = this.getFormData('REPROGRAMADO');
    this.projectsService.jsonData = json;
    this.router.navigate(['/delivery', 'project', this.route.snapshot.params.id, 'tower', this.towerIndex, 'apto', this.aptoIndex]);
  } else {
    alert('Para continuar todas las caracteristicas deben ser aprobadas');
  }
}



////////////////////


checkEmptyness(value) {
  if(value == undefined || value.trim() == '') {
    return '.';
  } else {
    return value;
  }
}

checkNotNull(value) {
  if(value == '.') {
    return '';
  } else {
    return value;
  }
}

getFormData(state){
  let auditsByFeatures = [];
  let type = this.project.projectTypes.find(typ => (typ.id == this.projectType));
  type.spaces.forEach((space, sindex) => {
    space.features.forEach((feature, findex) => {
      auditsByFeatures.push({
        featureId: feature.id,
        visitNote: this.checkEmptyness(this.spaceForm.get('spaces').get(sindex.toString(10)).get('features').get(findex.toString(10)).get('obs').value),
        approved: this.spaceForm.get('spaces').get(sindex.toString(10)).get('features').get(findex.toString(10)).get('aproved').value,
        observation: ''
      });
    });
  });

  let json = {
    auditsByFeatures, 
    nomenclatureId: this.nomenclatureId,
    auditsByReforms: this.getReformsData('REPROGRAMADO').auditsByReforms
  }
  return json;
}

getReformsData(state) {
  let auditsByReforms = [];
  this.reformas.forEach((reform, index) => {
    auditsByReforms.push({
      reformId: reform.id,
      visitNote: this.checkEmptyness(this.spaceForm.get('reforms').get(index.toString(10)).get('obs').value),
      observation: this.checkEmptyness(this.spaceForm.get('reforms').get(index.toString(10)).get('obsPendientes').value),
      approved: this.spaceForm.get('reforms').get(index.toString(10)).get('aproved').value
    });
  });
  let json = {
    nomenclatureState: state,
    auditsByReforms
  }
  return json;
}

// toggle all checks

toggleAllChecks(controlSpace, reforms) {
  let allToggled = true;
  let word = reforms? 'reforms': 'features'
  for ( let control of controlSpace.get(word)['controls']) {
    allToggled = allToggled && control.get('aproved').value;
  }
  if(!allToggled) {
    controlSpace.get(word)['controls'].forEach((control, index) => {
      control.get('aproved').setValue(true);
    });
    controlSpace.get('toggle').setValue(!allToggled);
  } else {
    controlSpace.get(word)['controls'].forEach((control, index) => {
      control.get('aproved').setValue(false);
    });
    controlSpace.get('toggle').setValue(!allToggled);
  }
}

handleChange(event, space) {
  if(!event.target.checked) {
    space.get('toggle').setValue(false);
  }
}

}
