import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AptoSelectionComponent } from './apto-selection.component';

describe('AptoSelectionComponent', () => {
  let component: AptoSelectionComponent;
  let fixture: ComponentFixture<AptoSelectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AptoSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AptoSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
