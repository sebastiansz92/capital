import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { ProjectsService } from 'src/app/projects/project.service';

@Component({
  selector: 'app-apto-selection',
  templateUrl: './apto-selection.component.html',
  styleUrls: ['./apto-selection.component.css']
})
export class AptoSelectionComponent implements OnInit {

  project;
  noms = [];
  date;
  audits;
  ready = false;
  paginadores: any;

  constructor(private projectService: ProjectService, private route: ActivatedRoute, private router: Router,
    private projectsService: ProjectsService) { }

  ngOnInit() {
    this.projectsService.getProjectById(this.route.snapshot.params.id).subscribe(res => {
      this.project = res.body['data'];
      this.project.towers.forEach(tower => {
        this.projectsService.getNomenclaturesByTowerAndState(tower.id, 0, 10, 'APROBADO').subscribe(
          (res) => {
            tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
            tower.nomenclaturesCount = tower.nomenclaturesPage.pageContent.length;
            this.paginadores = this.project.towers.map(tower => ({page: 0, size: 10, tower: tower.id, pages: Array.from(Array(Math.ceil(tower.nomenclaturesCount / (10)))).map((x, i) => i )}));
          }
          )
          this.ready = true;
        });
    });
  }

  setPaginador(paginador, index, tower) {
    paginador.page = index;
      this.projectsService.getNomenclaturesByTowerAndState(tower.id, paginador.page, paginador.size, 'APROBADO').subscribe(
        (res) => {
          tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
        }
      )
  }

  next(paginador, tower) {
    if((paginador.page + 1) !== paginador.pages.length) {
      paginador.page = paginador.page + 1;
        this.projectsService.getNomenclaturesByTowerAndState(tower.id, paginador.page, paginador.size, 'APROBADO').subscribe(
          (res) => {
            tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
          }
        )
    }
  }

  previous(paginador, tower) {
    if(paginador.page !== 0) {
      paginador.page = paginador.page - 1;
        this.projectsService.getNomenclaturesByTowerAndState(tower.id, paginador.page, paginador.size, 'APROBADO').subscribe(
          (res) => {
            tower.nomenclaturesPage.pageContent = res.body['data'].pageContent;
          }
        )
    }
  }

  goToDelivery(tower, apto, nomenclature) {
    this.projectsService.setNomenclature(nomenclature);
    this.projectsService.setCurrentProject(this.project);
    this.router.navigate(['delivery', 'owner', 'project', this.route.snapshot.params.id, 'tower', tower, 'apto', apto]);
  }

  getDate(tower, nom) {
    let audits = this.projectService.data.projects[this.route.snapshot.params.id].audits[tower][nom];
    return audits[audits.length - 1].date;
  }

  checkAprovedAptos() {
    return this.project.towers.filter(tower => this.checkAprobedAptosByTower(tower)).length > 0;
  }

  checkAprobedAptosByTower(tower){
    return tower.nomenclaturesPage.pageContent.filter(element => element.state == 'APROBADO').length > 0;
  }

}
