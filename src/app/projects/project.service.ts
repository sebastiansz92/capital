import { Injectable } from '@angular/core';
import { HttpBaseService } from '../shared/services/http/http-base.service';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService extends HttpBaseService {

  private currentProject = new Subject<any>();
  public currentProjectSubscribe$ = this.currentProject.asObservable();

  project = null;
  nomenclature = null;
  audit = null;
  spaces = null;
  reformAudits = null;
  reformIndex = null;
  jsonData = null;
  owner;

  constructor(http: HttpClient) {
    super(http);
  }

  createProject(project) {
    return this.post('project', project);
  }

  getAuditsByNomenclature(nomenclature) {
    return this.get(`audit?nomenclature=${nomenclature}`);
  }

  getAuditDetails(auditId) {

  }

  deleteReform(nomenclature, filename) {
    return this.delete(`nomenclature/${nomenclature}/reform/${filename}`);
  }

  getAuditDetail(audit) {
    return this.get(`audit/${audit}`);
  }

  getProjects() {
    return this.get('project');
  }

  getProjectById(id) {
    return this.get(`project/${id}`);
  }

  getProjectByIdWithoutPaging(id) {
    return this.get(`project/${id}?nomenclatures_paging=disabled`);
  }

  addAudit(id, data) {
    return this.post(`audit/nomenclature?nomenclature=${id}`, data);
  }

  addOwnerAudit(id, data) {
    return this.post(`audit/delivery?nomenclature=${id}`, data);
  }

  getNomenclaturesByTower(tower, page, size) {
    return this.get(`nomenclature?tower=${tower}&page=${page}&size=${size}`);
  }

  getNomenclaturesByTowerAndState(tower, page, size, state) {
    return this.get(`nomenclature?tower=${tower}&page=${page}&size=${size}&nom_state=${state}`);
  }

  desistNomenclature(data) {
    return this.put(`nomenclature`, data);
  }

  addTower(tower) {
    return this.post(`project/tower`, tower);
  }

  deleteteProject(id) {
    return this.delete(`project/${id}`);
  }

  deleteAdvances(id) {
    return this.delete(`project/cache/${id}`);
  }

  saveProjectCache(body) {
    return this.post(`project/cache`, body);
  }

  updateProjectCache(cacheid, body) {
    return this.put(`project/cache/${cacheid}`, body);
  }

  getProjectCache(id) {
    return this.get(`project/cache/${id}`);
  }

  getProjectsCaches() {
    return this.get(`project/cache`);
  }

  deleteFeature(id) {
    return this.delete(`feature/${id}`);
  }

  deleteSpace(id) {
    return this.delete(`space/${id}`);
  }

  deleteType(id) {
    return this.delete(`ptype/${id}`);
  }

  updateFeature(id, name) {
    return this.put(`feature/${id}`, { name: name });
  }

  updateSpace(spaceId, name) {
    return this.put(`space/${spaceId}`, { name, features: [] });
  }

  updateType(typeId, name) {
    return this.put(`ptype/${typeId}`, { name, spaces: [] });
  }

  duplicateType(typeId) {
    return this.put(`ptype/copyFrom/${typeId}`, null);
  }

  createFeature(space, name) {
    return this.post(`feature?spaceId=${space}`, { name });
  }

  createSpace(typeId, space) {
    return this.post(`space?projectTypeId=${typeId}`, space);
  }

  createType(projectId, type) {
    return this.post(`ptype?projectId=${projectId}`, type);
  }

  public setCurrentProject(project) {
    this.project = project;
    this.currentProject.next(project);
  }

  public setNomenclature(nomenclature) {
    this.nomenclature = nomenclature;
  }

  public setReformIndex(index) {
    this.reformIndex = index;
  }

  public setAudit(audit) {
    this.audit = audit;
  }

  getLastNElements(arr, n, type?) {
    let newArr;
    if (type) {
      newArr = arr.filter(aud => aud.auditor == type);
    } else {
      newArr = arr.filter(aud => aud.auditor != 'PROPIETARIO');
    }
    if (newArr == null) {
      return void 0;
    }
    return newArr.slice(Math.max(newArr.length - n, 0));
  }

  mapAuditsToSpaces2(spaces, audits, status) {
    let result = { audits: [], generales: '' };
    spaces.forEach(space => {
      let feats = space.features.map(feat => {
        let featAudit;
        if (status === "online") {
          featAudit = audits.find(audit => audit.feature == feat?.name && space.name == audit.space);
        } else {
          featAudit = audits.find(audit => {
            if (audit?.featureId) {
              return audit?.featureId == feat?.id 
            } else {
              return audit.feature?.id == feat?.id
            }
          });
        }
        return { ...feat, aud: featAudit };
      });
      result.audits.push({ ...space, features: feats });
    });
    let generales = audits.find(x => x.feature?.id == undefined);
    if (generales) {
      result.generales = generales.visitNote;
    }
    return result;
  }

  mapAuditsToSpaces(spaces, audits, status) {

    console.log(audits, spaces);
    let result = { audits: [], generales: '' };
    let auds = audits.reduce((accu, actual) => {
      if(actual.space && actual.space !== '') { // es una caracteristica sino es una reforma
          return {...accu, [actual.space]: {
            features: accu[actual.space]?.features?  [...accu[actual.space]?.features, {...actual, featureId: spaces.find(space => space.name === actual.space)?.features.find(feat => feat.name === actual.feature).id}] : [{...actual, featureId: spaces.find(space => space.name === actual.space)?.features.find(feat => feat.name === actual.feature).id}]
          }
        }
      } else {
        return {...accu, reforms: {
          features: accu['reforms']?.features? [...accu['reforms'].features, actual] : [actual]
        }}
      }
    }, {})
    /* spaces.forEach(space => {
      let feats = space.features.map(feat => {
        let featAudit;
        if (status === "online") {
          featAudit = audits.find(audit => audit.space == space?.name && audit?.feature == feat.name);
        } else {
          featAudit = audits.find(audit => { 
            if (audit?.featureId) {
              return audit?.featureId == feat?.id 
            } else {
              return audit.feature?.id == feat?.id
            }
          });
        }
        return { ...feat, aud: featAudit };
      });
      result.audits.push({ ...space, features: feats });
    }); */
    let response: any = {audits : Object.entries(auds).map(entrie => ({name: entrie[0], features: entrie[1]['features']}))};
    let generales = audits.find(x => x.feature?.id == undefined);
    let notSpaces = spaces.filter(space => !response.audits.map(aud => aud.name).includes(space.name));
    spaces.forEach(space => {
      let audspace = response.audits.find(aud => aud.name === space.name);
      if(audspace) {
        let noFeatures = space.features.filter(feature => !audspace.features.map(feat => feat.feature).includes(feature.name));
        noFeatures.forEach(nofeat => {
          audspace.features.push({feature: nofeat.name, featureId: nofeat.id, space: space.name});
        })
      } else {
        let newSpace = {
          name: space.name,
          features: space.features.map(fe => ({
            feature: fe.name,
            featureId: fe.id,
            space: space.name
          }))
        }
      }
    })
    if (generales) {
      result.generales = generales.visitNote;
      response['generales'] = generales.visitNote;
    }
    return response;
  }

  splitInN(array, n) {
    let [...arr] = array;
    var res = [];
    while (arr.length) {
      res.push(arr.splice(0, n));
    }
    return res;
  }

  getAudits(audits, type, project, auditor?) {
    let newAudits;
    if (auditor) {
      newAudits = audits.filter(aud => aud.auditor == auditor);
    } else {
      newAudits = audits.filter(aud => aud.auditor != 'PROPIETARIO');
    }

    let featuresCount = 0;
    let spaces = project['projectTypes'].find(pro => pro.id == type).spaces;
    this.spaces = spaces;
    spaces.forEach(space => {
      space.features.forEach(feat => {
        featuresCount = featuresCount + 1;
      });
    });
    if (auditor == 'PROPIETARIO') {
      return this.splitInN(newAudits, featuresCount);
    } else {
      return this.splitInN(newAudits, featuresCount + 1);
    }
  }

  isOwner() {
    return this.owner;
  }



}
