import { Component, Input, OnInit } from "@angular/core";
import { ProjectService } from "../shared/services/project.service";
import { MDBModalRef, MDBModalService } from "ng-uikit-pro-standard";
import { ConfirmationModalComponent } from "../shared/confirmation-modal/confirmation-modal.component";
import { ProjectsService } from "./project.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-projects",
  templateUrl: "./projects.component.html",
  styleUrls: ["./projects.component.css"],
})
export class ProjectsComponent implements OnInit {
  modalRef: MDBModalRef;
  projectdata;
  projectdataCache;
  loaded = false;
  cacheloaded = false;
  multimedia = "http://68.183.97.247:8091/";
  @Input() cache = false;
  loadingMessage = "Cargando";

  constructor(
    private projectService: ProjectService,
    private modalService: MDBModalService,
    private projectsSservice: ProjectsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    if (this.route.snapshot.routeConfig.path.includes("cache"))
      this.cache = true;

    this.projectsSservice.getProjects().subscribe((res) => {
      this.projectdata = res.body["data"];
      this.loaded = true;
    });
    this.projectsSservice.getProjectsCaches().subscribe(
      (res) => {
        this.projectdataCache = res.body["data"];
        this.cacheloaded = true;
      },
      (error) => {
        this.loadingMessage = "No se han encontrado proyectos en progreso";
      }
    );
  }

  resetProject() {
    this.projectService.setInitialState();
    sessionStorage.removeItem("capital_cacheid");
  }

  getProjects() {}

  openConfirmationModal(projectName, projectId) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: "feature-modal",
      containerClass: "",
      animated: true,
      data: {
        question:
          "¿Está seguro que desea eliminar este proyecto (" +
          projectName +
          ")?",
      },
    });
    this.modalRef.content.action.subscribe((result: any) => {
      if (result) {
        this.projectsSservice.deleteteProject(projectId).subscribe(
          (res) => {
            this.projectdata = this.projectdata.filter(
              (proj) => proj.projectId != projectId
            );
            console.log(
              `El proyecto ${projectName} ha sido eliminado correctamente`
            );
          },
          (error) => {
            alert(error.error.data || error.error.message);
          }
        );
      }
    });
    this.modalRef.hide();
  }

  openConfirmationModalAdvances(projectName, projectId) {
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: "feature-modal",
      containerClass: "",
      animated: true,
      data: {
        question:
          "¿Está seguro que desea eliminar este proyecto en progreso (" +
          projectName +
          ")?",
      },
    });
    this.modalRef.content.action.subscribe((result: any) => {
      if (result) {
        this.projectsSservice.deleteAdvances(projectId).subscribe(
          (res) => {
            this.projectdataCache = this.projectdataCache.filter(
              (proj) => proj.cacheId != projectId
            );
            console.log(
              `El proyecto ${projectName} ha sido eliminado correctamente`
            );
          },
          (error) => {
            alert(error.error.data || error.error.message);
          }
        );
      }
    });
    this.modalRef.hide();
  }

  getProjectImage(image_url) {
    let url = "";
    if (image_url) {
      image_url.substr(10);
    }
    return this.multimedia + image_url;
  }

  checkRol() {
    if (localStorage.getItem("role") != "RESIDENTE_OBRA") {
      return true;
    } else {
      return false;
    }
  }

  deleteAdvances(id) {
    this.projectsSservice.deleteAdvances(id);
  }
}
